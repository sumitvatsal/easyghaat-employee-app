package com.zeusadssolutions.easyghaat_empapp.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.adapter.HandOver_Adapter;
import com.zeusadssolutions.easyghaat_empapp.adapter.MyEarning_Adapter;
import com.zeusadssolutions.easyghaat_empapp.adapter.MyTask_Adapter;
import com.zeusadssolutions.easyghaat_empapp.model.EarningData;
import com.zeusadssolutions.easyghaat_empapp.model.EmpInfo;
import com.zeusadssolutions.easyghaat_empapp.model.MyTaskData;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;
import com.zeusadssolutions.easyghaat_empapp.utils.UserAlert_Dialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitin_000 on 9/19/2016.
 */
public class MyEarning extends Fragment {
    List<EarningData> earnList;
    PrefManager prefManager;
    MyEarning_Adapter adapter;
    RecyclerView earnList_recycler;
    ProgressBar progressBar;
    public MyEarning() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_earning, container, false);

        prefManager=new PrefManager(getActivity());
        earnList_recycler = (RecyclerView) view.findViewById(R.id.earningList_recycler);
        progressBar=(ProgressBar)view.findViewById(R.id.progressEarning);
        earnList_recycler.setHasFixedSize(true);
       new sendData().execute();



        return view;
    }

    private class sendData extends AsyncTask<Void, Void, Void>{
        String url=null;
        private int error=0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {

            EmpInfo empInfo=prefManager.EmpInfo_getObject("EmpData");
            url = new StringBuilder(SplashScreen.DOMAIN+"api/EmployeeIncentive/GetEmployeeEarning/ByEmpId/")
                    .append(empInfo.getEmp_id().trim())
                    .toString();

            WebService_Hit webService_hit = new WebService_Hit();
            String json = webService_hit.getUrlContents(url);

            boolean digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;


            }else {
                error=0;
                MyEarningData(json);
            }

            Log.d(SplashScreen.TAG, json);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);
            if(error==1)
            {
                Toast.makeText(getActivity(), "Unable to get list,Please try again ", Toast.LENGTH_SHORT).show();
            }else {

                adapter=new MyEarning_Adapter(earnList,getActivity());
                LinearLayoutManager llm = new LinearLayoutManager(getContext());
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                earnList_recycler.setLayoutManager(llm);

                earnList_recycler.setAdapter(adapter);

            }
        }



        private void MyEarningData(String sJson) {
            if (sJson != null) {

                try {
                    earnList=new ArrayList<>();
                    EarningData earningData = new EarningData();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if (jArray.length() != 0) {
                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject object = jArray.getJSONObject(i);
                            earningData = earningData.EarningDataJsonTo(object);
                            earnList.add(earningData);
                        }
                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }

    }

}
