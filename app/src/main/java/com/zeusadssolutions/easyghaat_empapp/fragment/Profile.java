package com.zeusadssolutions.easyghaat_empapp.fragment;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.maksim88.passwordedittext.PasswordEditText;
import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.activity.Signin;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.model.EmpInfo;
import com.zeusadssolutions.easyghaat_empapp.model.ManagerData;
import com.zeusadssolutions.easyghaat_empapp.service.NotifyService;
import com.zeusadssolutions.easyghaat_empapp.utils.UserAlert_Dialog;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Profile extends Fragment implements View.OnClickListener{
    PrefManager prefManager;
    TextView empId, empName,empEmail,empPhno,txt_reporting_manager,txt_branch_name_profile,
            txt_manager_mobile_profile,txt_manager_callnow_profile,txt_change_password,txt_view_secretPin,txt_change_secretPin;

    EditText editPin;
    EmpInfo empInfo;
    String new_pass,confirm_pass,old_pass,newPin;
    public static int passwordChanged=0;
    ManagerData managerData=null;
    UserAlert_Dialog dialogd;

    private Pattern pattern;
    private Matcher matcher;

    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,8})";


    public Profile() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        empId=(TextView)view.findViewById(R.id.txt_emp_id);
        empName=(TextView)view.findViewById(R.id.txt_emp_name);
        empEmail=(TextView)view.findViewById(R.id.txt_emp_email);
        empPhno=(TextView)view.findViewById(R.id.txt_emp_mobile);

        txt_view_secretPin=(TextView)view.findViewById(R.id.txt_view_secretPin);
        txt_change_secretPin=(TextView)view.findViewById(R.id.txt_change_secretPin);

        editPin=(EditText)view.findViewById(R.id.editPin);

        txt_reporting_manager=(TextView)view.findViewById(R.id.txt_reporting_manager);
        txt_branch_name_profile=(TextView)view.findViewById(R.id.txt_branch_name_profile);
        txt_manager_mobile_profile=(TextView)view.findViewById(R.id.txt_manager_mobile_profile);
        txt_manager_callnow_profile=(TextView)view.findViewById(R.id.txt_manager_callnow_profile);
        txt_change_password=(TextView)view.findViewById(R.id.txt_change_password);

        prefManager=new PrefManager(getActivity());

        empInfo=prefManager.EmpInfo_getObject("EmpData");

        empId.setText(empInfo.getEmp_id());
        empName.setText(empInfo.getEmp_name());
        empEmail.setText(empInfo.getEmail());
        empPhno.setText(empInfo.getPer_phoneNo());
        String pin=prefManager.getStringPreference("pin");
        editPin.setText(pin);

        Log.d(SplashScreen.TAG,"vendor pin---"+pin);
        txt_branch_name_profile.setText(empInfo.getBranchName());
        managerData=prefManager.ManagerData_getObject("ManagerData");
        txt_reporting_manager.setText(managerData.getEmp_name());
        txt_manager_mobile_profile.setText(managerData.getPer_phoneNo());


        txt_manager_callnow_profile.setOnClickListener(this);

        txt_change_password.setOnClickListener(this);

        txt_view_secretPin.setOnClickListener(this);
        txt_change_secretPin.setOnClickListener(this);





        return view;
    }

    void callNow()
    {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + managerData.getPer_phoneNo()));
            startActivity(callIntent);
        } catch (ActivityNotFoundException activityException) {
            Log.e(SplashScreen.TAG, "Call failed", activityException);
        }
    }



    private void showChangeLangDialog()
    {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        dialogBuilder.setView(dialogView);

        final PasswordEditText oldPassword = (PasswordEditText) dialogView.findViewById(R.id.txt_oldPassword);
        final PasswordEditText newPassword = (PasswordEditText) dialogView.findViewById(R.id.txt_newPassword);
        final PasswordEditText confirmPassword = (PasswordEditText) dialogView.findViewById(R.id.txt_confirmPassword);


        dialogBuilder.setTitle("EasyGhaat");
       // dialogBuilder.setMessage("Enter text below");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();

                Log.d(SplashScreen.TAG,"old password:--"+oldPassword.getText().toString());


                new_pass=newPassword.getText().toString();
                old_pass=oldPassword.getText().toString();
                confirm_pass=confirmPassword.getText().toString();

                if(old_pass.equals(empInfo.getPassword()))
                {
                    if (!new_pass.equals(old_pass)) {
                        if (!validate(new_pass)) {

                            if (new_pass.equals(confirm_pass)) {
                                new sendData().execute();
                            } else {
                                dialogd = new UserAlert_Dialog("Password does not match", getActivity(), R.mipmap.cross);
                                dialogd.showDialog_finish();

                            }
                        }else {
                            Toast.makeText(getActivity(), "Enter the 6-8 digits Alphanumeric password with 1Caps password", Toast.LENGTH_SHORT).show();
                        }

                    }else {
                        Toast.makeText(getActivity(), "New password can't be the old password", Toast.LENGTH_SHORT).show();
                    }


                }else{
                    dialogd=new UserAlert_Dialog("Invalid Password",getActivity(),R.mipmap.cross);
                    dialogd.showDialog_finish();

                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                dialog.cancel();
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
        Button bg=b.getButton(DialogInterface.BUTTON_POSITIVE);
       // bg.setBackgroundColor(getActivity().getResources().getColor(R.color.toolBar_color2));
        bg.setTextColor(getActivity().getResources().getColor(R.color.toolBar_color2));

        Button bg1=b.getButton(DialogInterface.BUTTON_NEGATIVE);
        //bg1.setBackgroundColor(getActivity().getResources().getColor(R.color.toolBar_color2));
        bg1.setTextColor(getActivity().getResources().getColor(R.color.toolBar_color2));
    }
    public boolean validate(final String password){
        Log.d(SplashScreen.TAG,"string received--"+password);
        pattern = Pattern.compile(PASSWORD_PATTERN,Pattern.DOTALL);
        matcher = pattern.matcher(password);

        if(!matcher.find())
        {
            return true; // dont know what to place
        }
        return false;

    }
    private void comparePassword()
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());

        dialogBuilder.setTitle("EasyGhaat");
        dialogBuilder.setMessage("Enter Password");

        final EditText input = new EditText(getActivity());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        dialogBuilder.setView(input);
        dialogBuilder.setIcon(R.mipmap.img);

        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                String password = input.getText().toString();

                    if (empInfo.getPassword().equals(password)) {
                        editPin.setEnabled(true);
                        editPin.requestFocus();
                        editPin.getText().clear();
                        editPin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                        editPin.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});

                        editPin.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                                    newPin=editPin.getText().toString().trim();
                                    new changePin().execute();
                                    editPin.setEnabled(false);
                                }
                                return false;
                            }
                        });

                    } else {
                        dialogd = new UserAlert_Dialog("Wrong password", getActivity(),R.mipmap.cross);
                        dialogd.showDialog_finish();
                    }



            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                dialog.cancel();
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
        Button bg=b.getButton(DialogInterface.BUTTON_POSITIVE);
        // bg.setBackgroundColor(getActivity().getResources().getColor(R.color.toolBar_color2));
        bg.setTextColor(getActivity().getResources().getColor(R.color.toolBar_color2));

        Button bg1=b.getButton(DialogInterface.BUTTON_NEGATIVE);
        //bg1.setBackgroundColor(getActivity().getResources().getColor(R.color.toolBar_color2));
        bg1.setTextColor(getActivity().getResources().getColor(R.color.toolBar_color2));
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txt_change_password:
                showChangeLangDialog();
                break;

            case R.id.txt_change_secretPin:
                comparePassword();


                break;
            case R.id.txt_manager_callnow_profile:
                callNow();
                break;

            case R.id.txt_view_secretPin:

                editPin.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
        }
    }


    private class sendData extends AsyncTask<Void, Void, Void> {
        String url=null;
        private ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            EmpInfo empInfo=prefManager.EmpInfo_getObject("EmpData");
            url = new StringBuilder("http://sanjay.cashbachat.com/api/Employees/PutEmployeePassword/?emp_id=")
                    .append(empInfo.getEmp_id().trim())
                    .append("&password=")
                    .append(new_pass)
                    .toString();

            WebService_Hit webService_hit = new WebService_Hit();
            String json = webService_hit.putUrlContents( url);
            if(json.equals("\"SuccessFully Changed\""))
            {
                passwordChanged=1;
            }else {
                passwordChanged=0;
            }
            Log.d(SplashScreen.TAG, json);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            // Dismiss the progress dialog
            if (dialog.isShowing())
                dialog.dismiss();
            if (passwordChanged==1)
            {
                getActivity().stopService(new Intent(getActivity(), NotifyService.class));
                prefManager.setLogin(false);
                Intent intent = new Intent(getActivity(), Signin.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                getActivity().finish();
            }else {
                dialogd=new UserAlert_Dialog("Password does not change, Please try again!",getActivity(),R.mipmap.cross);
                dialogd.showDialog_finish();
            }


        }
    }
    private class changePin extends AsyncTask<Void, Void, Void> {
        String url=null;
        private ProgressDialog dialog;
        private int error=0;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            url = new StringBuilder(SplashScreen.DOMAIN+"api/EmployeeIncentive/UpdateEmployeeSecretPin/ByEmpId/")
                    .append(empInfo.getEmp_id().trim())
                    .append("/")
                    .append(newPin)
                    .toString();

            WebService_Hit webService_hit = new WebService_Hit();
            String json = webService_hit.getUrlContents(url);

            Log.d(SplashScreen.TAG, "json value --" + String.valueOf(!json.equals("200")));

            boolean digitsOnly = TextUtils.isDigitsOnly(json);
            if (!json.equals("200")) {
                Log.d(SplashScreen.TAG,"inside If-");
                error=0;


            }else {
                error = 1;
                Log.d(SplashScreen.TAG,"inside else-"+json);



                }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            // Dismiss the progress dialog
            if (dialog.isShowing())
                dialog.dismiss();

            Log.d(SplashScreen.TAG,"value of error--"+String.valueOf(error));
            if (error==0)
            {

                Toast.makeText(getActivity(), "Secret Pin is change", Toast.LENGTH_SHORT).show();
                prefManager.setStringPreference("pin", newPin);


            }else {
                dialogd=new UserAlert_Dialog("Secret Pin not change, Please try again!",getActivity(),R.mipmap.cross);
                dialogd.showDialog_finish();
            }


        }
    }
}
