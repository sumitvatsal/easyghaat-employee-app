package com.zeusadssolutions.easyghaat_empapp.fragment;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.activity.Invoice;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.adapter.HandOver_Adapter;
import com.zeusadssolutions.easyghaat_empapp.adapter.HouseHoldAdapter;
import com.zeusadssolutions.easyghaat_empapp.adapter.OrderDetails_Adapter;
import com.zeusadssolutions.easyghaat_empapp.adapter.VendorOrderDetails_Adapter;
import com.zeusadssolutions.easyghaat_empapp.model.HandoverData;
import com.zeusadssolutions.easyghaat_empapp.model.InvoiceData;
import com.zeusadssolutions.easyghaat_empapp.model.MakeOrderDetails;
import com.zeusadssolutions.easyghaat_empapp.model.TaskDetails_UserData;
import com.zeusadssolutions.easyghaat_empapp.utils.UserAlert_Dialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 04-10-2016.
 */
public class ViewOrder_vendor extends Fragment implements View.OnClickListener {

    ImageView imgCallNow;
    TextView txtCallNow,textVendorAddress_handover,textCustomerAdd_viewOrder,txtTypeVendor,
            textPickupAdd_viewOrder,textCustomerContactDetail_viewOrder,textTimeSlot_viewOrder,txtClothCount_vendorView;
    Button btnHandoverOrder_viewOrder_vendor;
    private ArrayList<MakeOrderDetails> orderDetailsList;
    private List<HandoverData> handoverDatas;
    private TaskDetails_UserData userData;
    private RecyclerView orderListRecycle;
    private int pos;
    private VendorOrderDetails_Adapter adapter;
    UserAlert_Dialog dialogd;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_view_order_vendor, container, false);
        // Inflate the layout for this fragment
        orderDetailsList = new ArrayList<>();
        orderDetailsList = HandOver_Adapter.makeOrderDetails;

        Log.d(SplashScreen.TAG,"orderDetails--"+orderDetailsList.get(0).getProductName());
        userData= new TaskDetails_UserData();
        userData=HandOver_Adapter.myTaskData;
        handoverDatas=new ArrayList<>();
        handoverDatas=HandOver.handoverDataList;
        pos=HandOver_Adapter.position;
          /*initialize TextView*/
        textVendorAddress_handover=(TextView)view.findViewById(R.id.textVendorAddress_handover);
        textCustomerAdd_viewOrder=(TextView)view.findViewById(R.id.textCustomerAdd_viewOrder);
        textPickupAdd_viewOrder=(TextView)view.findViewById(R.id.textPickupAdd_viewOrder);
        textTimeSlot_viewOrder=(TextView)view.findViewById(R.id.textTimeSlot_viewOrder);
        txtCallNow=(TextView)view.findViewById(R.id.textCallNow_viewOrder_vendor);
        textCustomerContactDetail_viewOrder=(TextView)view.findViewById(R.id.textCustomerContactDetail_viewOrder);
        txtTypeVendor=(TextView)view.findViewById(R.id.txtTypeVendor);

          /*initialize RecyclerView*/
        orderListRecycle = (RecyclerView) view.findViewById(R.id.orderDetail_recycler_vendorView);

        imgCallNow=(ImageView)view.findViewById(R.id.imgCallNow_vendor_view);

        btnHandoverOrder_viewOrder_vendor=(Button)view.findViewById(R.id.btnHandoverOrder_viewOrder_vendor);

        btnHandoverOrder_viewOrder_vendor.setOnClickListener(this);
        imgCallNow.setOnClickListener(this);
        txtCallNow.setOnClickListener(this);

        orderListRecycle.setHasFixedSize(true);
        adapter = new VendorOrderDetails_Adapter(orderDetailsList, getActivity());
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        orderListRecycle.setLayoutManager(llm);

        orderListRecycle.setAdapter(adapter);
        String custAdd=new StringBuilder(userData.getCUSTPICKHOUSENO()+"\t"+userData.getCUSTPICKSTREETNAME()+"\n")
                .append(userData.getCUSTPICKLANDMARK()+"\n")
                .append(userData.getCUSTPICKCITY())
                .toString();

        String vendorAdd=new StringBuilder(userData.getVENDORNAME()+"\n")
                .append(userData.getVNDRHOUSSHOPNO()+"\t"+userData.getVNDRSTREETNAME()+"\n")
                .append(userData.getVNDRLNDMARK()+"\n")
                .append(userData.getVNDRCITYNAME())
                .toString();

        String timeSlot=new StringBuilder(userData.getPickupFromtime()+"-")
                .append(userData.getPickupTotime())
                .toString();
        textCustomerAdd_viewOrder.setText(userData.getCustomerName());

        textCustomerContactDetail_viewOrder.setText(userData.getCustomerMobileNo());

        txtTypeVendor.setText(userData.getType());
        textPickupAdd_viewOrder.setText(custAdd);

        textTimeSlot_viewOrder.setText(timeSlot);
        textVendorAddress_handover.setText(vendorAdd);

        return view;
    }

    void callNow()
    {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"+textCustomerContactDetail_viewOrder.getText().toString()));
            startActivity(callIntent);
        } catch (ActivityNotFoundException activityException) {
            Log.e(SplashScreen.TAG, "Call failed", activityException);
        }
    }
    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.imgCallNow_vendor_view:
               callNow();

                break;

            case R.id.textCallNow_viewOrder_vendor:
                callNow();
                break;
            case R.id.btnHandoverOrder_viewOrder_vendor:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());

                dialogBuilder.setTitle("EasyGhaat");
                dialogBuilder.setMessage("Enter secret pin");

                final EditText input = new EditText(getActivity());
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                dialogBuilder.setView(input);
                dialogBuilder.setIcon(R.mipmap.img);
                input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});
                dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        String pin = input.getText().toString();

                        if (handoverDatas.get(pos).getVendorPin().equals(pin)) {


                            new handoverOrderCreate().execute();
                        } else {
                            dialogd = new UserAlert_Dialog("Wrong pin", getActivity(),R.mipmap.cross);
                            dialogd.showDialog_finish();
                        }


                    }
                });
                AlertDialog b = dialogBuilder.create();
                b.show();
                Button bg=b.getButton(DialogInterface.BUTTON_POSITIVE);
                // bg.setBackgroundColor(getActivity().getResources().getColor(R.color.toolBar_color2));
                bg.setTextColor(getActivity().getResources().getColor(R.color.toolBar_color2));

                break;
        }

    }

    /*generating handover task*/
    private class handoverOrderCreate extends AsyncTask<Void,Void,Void> {
        int error=0;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Updating...");
            dialog.setCancelable(false);
            dialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();

            String instantUrl=new StringBuilder(SplashScreen.DOMAIN+"api/EmployeeIncentive/AddHandoverTask/ByPickUpId/")
                    .append(handoverDatas.get(pos).getPickUpId().trim())
                    .toString();
            String makeJson = webService_hit.getUrlContents(instantUrl);
            Log.d(SplashScreen.TAG, "handover-" + makeJson);

            boolean digitsOnly = TextUtils.isDigitsOnly(makeJson);
            if (digitsOnly) {
                error=1;


            }else {
                error=0;
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (dialog.isShowing())
                dialog.dismiss();
            if(error==1)
            {
                dialogd = new UserAlert_Dialog("Unable to Handover, Please try again",getActivity().getResources().getString(R.string.handover),
                        getActivity(),R.mipmap.cross);
                dialogd.showDialog_newFragment();

            }else {


                dialogd = new UserAlert_Dialog("Handover task to vendor",getActivity().getResources().getString(R.string.handover),
                        getActivity(),R.mipmap.tick);
                dialogd.showDialog_newFragment();


            }
        }

    }
}
