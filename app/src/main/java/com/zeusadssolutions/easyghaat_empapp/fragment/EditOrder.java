package com.zeusadssolutions.easyghaat_empapp.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.activity.Invoice;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.adapter.SelectServiceAdapter;
import com.zeusadssolutions.easyghaat_empapp.adapter.TabLayout_Adapter;
import com.zeusadssolutions.easyghaat_empapp.model.EmpInfo;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;
import com.zeusadssolutions.easyghaat_empapp.utils.StartNewActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 05-10-2016.
 */
public class EditOrder extends Fragment implements View.OnClickListener,SelectServiceAdapter.ServiceHolder.ClickListener{

    public EditOrder() {
    }

    //This is our tablayout
    private TabLayout tabLayout;


    private int[] tabIcons = {
            R.mipmap.regular_wash_tab,
            R.mipmap.premium_cat,
            R.mipmap.woolen_cat_tab,
            R.mipmap.household_cat_tab,
            R.mipmap.acces_cat
    };
    private List<String> serviceList;
    private PrefManager prefManager;
    EmpInfo empInfo;

    public static int serviceId=0,categoryId=0,genderId=0;
    public static String branchId;

    //This is our viewPager
    private ViewPager viewPager;
    Button btnViewDetails,btnCancelOrder;
    private RecyclerView recyclerService;
    private SelectServiceAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_edit_order, container, false);
        serviceList=new ArrayList<>();
        prefManager=new PrefManager(getActivity());
        empInfo=prefManager.EmpInfo_getObject("EmpData");
        branchId=empInfo.getAsin_branchId();
        for(int i=0;i<5;i++)
        {
            serviceList.add(String.valueOf(i+1));
        }


        recyclerService=(RecyclerView)view.findViewById(R.id.recyclerService);
        btnCancelOrder=(Button)view.findViewById(R.id.btnCancelOrder);
        btnViewDetails=(Button)view.findViewById(R.id.btnViewDetails);
        //Initializing viewPager
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        categoryId=1;serviceId=1;
        btnViewDetails.setOnClickListener(this);
        btnCancelOrder.setOnClickListener(this);
        adapter=new SelectServiceAdapter(serviceList,getActivity(),this);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerService.setLayoutManager(llm);
        recyclerService.setAdapter(adapter);

        return view;
    }


    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
        tabLayout.getTabAt(4).setIcon(tabIcons[4]);
    }

    private void setupViewPager(ViewPager viewPager) {

        //Creating our pager adapter
        TabLayout_Adapter adapter = new TabLayout_Adapter(getChildFragmentManager());

        adapter.addFrag(new Regular(), "Regular");
        adapter.addFrag(new Premium(), "Premium");
        adapter.addFrag(new Woolen(), "Woolen");
        adapter.addFrag(new Household(), "Household");
        adapter.addFrag(new Accessories(), "Accessories");
        viewPager.setAdapter(adapter);
    }


    @Override
    public void onClick(View v) {
        String  fragmentName;
        StartNewActivity activity;
        switch (v.getId())
        {
            case R.id.btnViewDetails:

                 fragmentName=getActivity().getResources().getString(R.string.view_order_make);
                activity=new StartNewActivity();
                activity.showActivity(getActivity(), fragmentName);
                getActivity().finish();
                break;
            case R.id.btnCancelOrder:
                 getActivity().finish();

                break;

        }
    }

    @Override
    public void onItemClicked(int position) {
        adapter.toggleSelection(position);
        serviceId=Integer.parseInt(serviceList.get(position));

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

    }


}
