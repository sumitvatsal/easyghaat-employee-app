package com.zeusadssolutions.easyghaat_empapp.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.adapter.Estatement_Adapter;
import com.zeusadssolutions.easyghaat_empapp.adapter.MyEarning_Adapter;
import com.zeusadssolutions.easyghaat_empapp.model.EarningData;
import com.zeusadssolutions.easyghaat_empapp.model.EmpInfo;
import com.zeusadssolutions.easyghaat_empapp.model.EstatementData;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;


public class E_Statement extends Fragment {
    List<EstatementData> eStatementList;
    PrefManager prefManager;
    Estatement_Adapter adapter;
    RecyclerView eStatementList_recycler;
    ProgressBar progressBar;
    Date fromdate = null,today90=null;
    TextView textToDate,textFromDate;
    private String fromDate,toDate;
    SimpleDateFormat sdfDateTime;
    public E_Statement() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes

        View view = inflater.inflate(R.layout.fragment_estatement, container, false);
        prefManager=new PrefManager(getActivity());
        eStatementList_recycler = (RecyclerView) view.findViewById(R.id.eStatementList_recycler);
        progressBar=(ProgressBar)view.findViewById(R.id.progressEstatement);

        textFromDate=(TextView)view.findViewById(R.id.textFromDate);

        textToDate=(TextView)view.findViewById(R.id.textToDate);

        eStatementList_recycler.setHasFixedSize(true);
        // SimpleDateFormat Class
        sdfDateTime = new SimpleDateFormat("yyyy/MM/dd");
        fromDate =  sdfDateTime.format(new Date(System.currentTimeMillis()));
        Log.d(SplashScreen.TAG," SimpleDateFormat today-"+String.valueOf(fromDate));


        try {
            fromdate = sdfDateTime.parse(fromDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = new GregorianCalendar();
        cal.setTime(fromdate);
        Log.d(SplashScreen.TAG, "today-" + String.valueOf(fromdate));
        cal.add(Calendar.DAY_OF_MONTH, +90);
         today90 = cal.getTime();
        cal.setTime(today90);

        toDate = sdfDateTime.format(today90);
        textFromDate.setText(fromDate);
        textToDate.setText(toDate);
        new sendData().execute();

        return view;
    }

    private class sendData extends AsyncTask<Void, Void, Void> {
        String url=null;
        private int error=0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {

            EmpInfo empInfo=prefManager.EmpInfo_getObject("EmpData");
            url = new StringBuilder(SplashScreen.DOMAIN+"api/EmployeeIncentive/GetEmployeeEstatement")
                    .toString();

            // Making a request to url and getting response
            List<NameValuePair> listParams = new ArrayList<NameValuePair>();
            listParams.add(new BasicNameValuePair("EmployeeId", empInfo.getEmp_id().trim()));
            listParams.add(new BasicNameValuePair("FromDate","2016/12/01"));
            listParams.add(new BasicNameValuePair("ToDate", "2016/12/24"));

            WebService_Hit webService_hit = new WebService_Hit();
            String json = webService_hit.postListParamUrlContents(listParams, url);

            boolean digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;
            }else {
                error=0;
                MyeStatementData(json);
            }

            Log.d(SplashScreen.TAG, json);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);
            if(error==1)
            {
                Toast.makeText(getActivity(), "Unable to get data,Please try again ", Toast.LENGTH_SHORT).show();
            }else {
                textFromDate.setText(fromDate);
                textToDate.setText(toDate);
                adapter=new Estatement_Adapter(eStatementList,getActivity());
                LinearLayoutManager llm = new LinearLayoutManager(getContext());
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                eStatementList_recycler.setLayoutManager(llm);
                eStatementList_recycler.setAdapter(adapter);

            }
        }



        private void MyeStatementData(String sJson) {
            if (sJson != null) {

                try {
                    eStatementList=new ArrayList<>();
                    EstatementData estatementData = new EstatementData();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if (jArray.length() != 0) {
                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject object = jArray.getJSONObject(i);
                            estatementData = estatementData.EstatementDataJsonTo(object);
                            eStatementList.add(estatementData);
                        }
                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }

    }
}
