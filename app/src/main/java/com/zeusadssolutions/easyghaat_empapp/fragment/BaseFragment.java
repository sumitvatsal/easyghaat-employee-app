package com.zeusadssolutions.easyghaat_empapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.activity.MainActivity;
import com.zeusadssolutions.easyghaat_empapp.utils.StartNewActivity;


/**
 * Created by Zeusadsolutions on 26/08/2016.
 */
public class BaseFragment extends Fragment implements View.OnClickListener{

    public BaseFragment() {
    }
    String fragmentName=null;

    LinearLayout my_earning_linear,my_task_linear,handOver_linear,cash_trans_linear;
    Button rateList_button;
    @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)  {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes

        View view = inflater.inflate(R.layout.fragment_base, container, false);

        my_earning_linear=(LinearLayout)view.findViewById(R.id.my_earning_linear);
        my_task_linear=(LinearLayout)view.findViewById(R.id.my_task_linear);
        handOver_linear=(LinearLayout)view.findViewById(R.id.handOver_linear);
        cash_trans_linear=(LinearLayout)view.findViewById(R.id.cash_trans_linear);




        my_earning_linear.setOnClickListener(this);
        my_task_linear.setOnClickListener(this);
        cash_trans_linear.setOnClickListener(this);
        handOver_linear.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        String  fragmentName;
        StartNewActivity activity=new StartNewActivity();

      switch (v.getId())
        {
            case R.id.my_earning_linear:
                fragmentName=getResources().getString(R.string.my_earning);
                activity.showActivity(getActivity(), fragmentName);
                break;

            case R.id.my_task_linear:

                fragmentName=getResources().getString(R.string.my_task);
                activity.showActivity(getActivity(), fragmentName);
                break;

            case R.id.handOver_linear:
                fragmentName = getResources().getString(R.string.handover);
                activity.showActivity(getActivity(), fragmentName);
                break;
            case R.id.cash_trans_linear:
                fragmentName = getResources().getString(R.string.collection_handover);
                activity.showActivity(getActivity(), fragmentName);
                break;
        }
    }

}
