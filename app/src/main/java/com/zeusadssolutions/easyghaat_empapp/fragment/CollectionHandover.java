package com.zeusadssolutions.easyghaat_empapp.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.adapter.CashPendingAdapter;
import com.zeusadssolutions.easyghaat_empapp.adapter.HandOver_Adapter;
import com.zeusadssolutions.easyghaat_empapp.model.CashPendingData;
import com.zeusadssolutions.easyghaat_empapp.model.EmpInfo;
import com.zeusadssolutions.easyghaat_empapp.model.HandoverData;
import com.zeusadssolutions.easyghaat_empapp.model.ManagerData;
import com.zeusadssolutions.easyghaat_empapp.utils.MSG91;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;
import com.zeusadssolutions.easyghaat_empapp.utils.UserAlert_Dialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by HP on 30-09-2016.
 */
public class CollectionHandover extends Fragment{
    String AM_PM;
    TextView text_depositDate,textTotalAmount,textBranchName,txtManagerName;
    Button btnHandover_Collection,btnCancel_Collection,btnRegenrateOtp;
    EditText editManagerOtp;
    String date;
    String time;
    Double totalAmount=0.0;
    EmpInfo empInfo;
    ManagerData managerData;

    ProgressBar progressBar;
    public  static List<CashPendingData> cashPendingDatas;
    private RecyclerView cashPending_recycler;
    private PrefManager prefManager;
    private CashPendingAdapter adapter;
    public CollectionHandover() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes

        View view = inflater.inflate(R.layout.fragment_collection_handover, container, false);
        prefManager=new PrefManager(getActivity());
         empInfo=prefManager.EmpInfo_getObject("EmpData");
        managerData=prefManager.ManagerData_getObject("ManagerData");
        text_depositDate=(TextView)view.findViewById(R.id.textDepositDate);
        textTotalAmount=(TextView)view.findViewById(R.id.textTotalAmount);
        progressBar=(ProgressBar)view.findViewById(R.id.progressCollection);
        editManagerOtp=(EditText)view.findViewById(R.id.editManagerOtp);

        textBranchName=(TextView)view.findViewById(R.id.textBranchName);
        txtManagerName=(TextView)view.findViewById(R.id.txtManagerName);
        btnHandover_Collection=(Button)view.findViewById(R.id.btnHandOver_Collection);
        btnRegenrateOtp=(Button)view.findViewById(R.id.btnRegenrateOtp);
        btnCancel_Collection=(Button)view.findViewById(R.id.btnCancel_collection);
        cashPending_recycler=(RecyclerView)view.findViewById(R.id.collectionHandover_recycler);
        Calendar c = Calendar.getInstance();

        int seconds = c.get(Calendar.SECOND);
        int minutes = c.get(Calendar.MINUTE);
        int hour = c.get(Calendar.HOUR);
        int ds = c.get(Calendar.AM_PM);

        if(ds==0)
            AM_PM="am";
        else
            AM_PM="pm";
        time = hour+":"+minutes+":"+seconds+" "+AM_PM;


        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);
        date = day+"/"+month+"/"+year;
        text_depositDate.setText(" \t\t" + date + " \t\t" + time);

        textBranchName.setText(empInfo.getBranchName());

        txtManagerName.setText("\t"+managerData.getEmp_name());

        new sendData().execute();


        return view;
    }




    private class sendData extends AsyncTask<Void, Void, Void>  implements View.OnClickListener {
        String url=null;
        private int error=0;
        int otp;
        UserAlert_Dialog dialogd;
        boolean otpExpire;
          @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
           progressBar.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {


            url = new StringBuilder(SplashScreen.DOMAIN+"api/EmployeeIncentive/GetEmployeeCashPending/ByEmpId/")
                    .append(empInfo.getEmp_id().trim())
                    .toString();

            WebService_Hit webService_hit = new WebService_Hit();
            String json = webService_hit.getUrlContents(url);

            boolean digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;


            }else {
                error=0;
                MyTaskData(json);
            }

            Log.d(SplashScreen.TAG, json);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
       progressBar.setVisibility(View.GONE);
            if(error==1)
            {
                Toast.makeText(getActivity(), "Unable to get collection list,Please try again ", Toast.LENGTH_SHORT).show();
            }else {

                btnCancel_Collection.setOnClickListener(this);
                btnHandover_Collection.setOnClickListener(this);
                btnRegenrateOtp.setOnClickListener(this);

                adapter = new CashPendingAdapter(cashPendingDatas, getActivity());
                cashPending_recycler.setAdapter(adapter);
                for(int i=0;i<cashPendingDatas.size();i++)
                {
                    totalAmount= Double.parseDouble(cashPendingDatas.get(i).getTotalAmount())+
                            totalAmount;


                }
                if(totalAmount==0.0)
                {
                    textTotalAmount.setText(String.valueOf(""));
                }else {
                    textTotalAmount.setText(String.valueOf(totalAmount));
                }
                LinearLayoutManager llm = new LinearLayoutManager(getContext());
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                cashPending_recycler.setLayoutManager(llm);



            }
        }



        private void MyTaskData(String sJson) {
            if (sJson != null) {

                try {
                    cashPendingDatas=new ArrayList<>();
                    CashPendingData myTaskData = new CashPendingData();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if (jArray.length() != 0) {
                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject object = jArray.getJSONObject(i);
                            myTaskData = myTaskData.cashDetailsJsonTo(object);
                            cashPendingDatas.add(myTaskData);
                        }
                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.btnHandOver_Collection:

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            String otpReceived = editManagerOtp.getText().toString();
                            if (otp == Integer.parseInt(otpReceived)) {
                                dialogd= new UserAlert_Dialog("You have handover amount to manager ", "Home", getActivity(), R.mipmap.tick);
                                dialogd.showDialog_newFragment();


                            } else {
                                dialogd = new UserAlert_Dialog("Otp has been Expired, Please generate new OTP ",
                                        getActivity(), R.mipmap.cross);
                                dialogd.showDialog_finish();
                                btnRegenrateOtp.setText("Regenerate Otp");
                            }
                       }
                    }, 5000);


                    break;
                case  R.id.btnCancel_collection:
                    getActivity().finish();
                    getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    break;

                case R.id.btnRegenrateOtp:
                    otp=gen();
                    MSG91 msg91=new MSG91(getResources().getString(R.string.Msg91AuthKey));
                    String msg="Dear \t"+managerData.getEmp_name()+
                            "&#xA;&#xA;" + empInfo.getEmp_name() +
                            "\t\t" + empInfo.getEmp_id()+" \thas requested to submit Rs." +String.valueOf(totalAmount)+

                            "&#xA;&#xA;Please give him OTP- \t" + otp +
                            "\t\tto transfer." +

                            " &#xA;&#xA;Team EasyGhaat";
                    Log.d(SplashScreen.TAG, msg);
                    msg91.composeMessage("ESYGHT", msg);
                    //msg91.to(managerData.getPer_phoneNo());
                    msg91.to("7055620003");
                    msg91.setRoute("4");
                    String sendStatus = msg91.send();
                    editManagerOtp.setEnabled(true);
                    editManagerOtp.requestFocus();
                    editManagerOtp.getText().clear();
                    editManagerOtp.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                    editManagerOtp.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5)});

                    editManagerOtp.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                                final Timer myTimer = new Timer();
                                myTimer.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        String otpReceived = editManagerOtp.getText().toString();
                                        if (otp == Integer.parseInt(otpReceived)) {
                                            otpExpire = false;
                                            myTimer.cancel();
                                        } else {
                                            otpExpire = true;
                                            myTimer.cancel();
                                        }
                                    }
                                }, 0, 50000);

                                dialogd = new UserAlert_Dialog("You have handover amount to manager ", "Home", getActivity(), R.mipmap.tick);
                                dialogd.showDialog_newFragment();

                            }
                            return false;
                        }
                    });
                    Log.d(SplashScreen.TAG, "otp sent:-" + otp + "sendStatus:-" + sendStatus);
                    break;
            }

        }

        public int gen() {
            Random r = new Random( System.currentTimeMillis() );
            return ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
        }



    }
}


