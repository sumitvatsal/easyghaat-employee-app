package com.zeusadssolutions.easyghaat_empapp.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.adapter.ClothOption_Adapter;
import com.zeusadssolutions.easyghaat_empapp.model.ProductData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.zeusadssolutions.easyghaat_empapp.fragment.EditOrder.branchId;
import static com.zeusadssolutions.easyghaat_empapp.fragment.EditOrder.categoryId;
import static com.zeusadssolutions.easyghaat_empapp.fragment.EditOrder.genderId;
import static com.zeusadssolutions.easyghaat_empapp.fragment.EditOrder.serviceId;

/**
 * Created by nitin_000 on 9/12/2016.
 */
public class Woolen extends Fragment implements View.OnClickListener {
    LinearLayout menLinear,menLinearOption,womenLinear,womenLinearOption,kidsLinear,kidsLinearOption;
    ImageView menCol,womenCol,kidsCol,menExpand,womenExpand,kidsExpand;


    RecyclerView menRecycler,womenRecycler,kidsRecycler;
    ClothOption_Adapter adapter;

    private ArrayList<ProductData> dataSelected = new ArrayList<>();
    public Woolen() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.wash_category, container, false);

        menLinear=(LinearLayout)view.findViewById(R.id.menLinear);
        womenLinear=(LinearLayout)view.findViewById(R.id.womenLinear);
        kidsLinear=(LinearLayout)view.findViewById(R.id.kidsLinear);

        menLinearOption=(LinearLayout)view.findViewById(R.id.men_option);
        womenLinearOption=(LinearLayout)view.findViewById(R.id.women_option);
        kidsLinearOption=(LinearLayout)view.findViewById(R.id.kidsOption);

        menExpand=(ImageView)view.findViewById(R.id.menExpand);
        womenExpand=(ImageView)view.findViewById(R.id.womenExpand);
        kidsExpand=(ImageView)view.findViewById(R.id.kidsExpand);

        menCol=(ImageView)view.findViewById(R.id.menCol);
        womenCol=(ImageView)view.findViewById(R.id.womenCol);
        kidsCol=(ImageView)view.findViewById(R.id.kidsCol);

        menRecycler=(RecyclerView)view.findViewById(R.id.recycler_menCloth);
        womenRecycler=(RecyclerView)view.findViewById(R.id.recycler_womenCloth);
        kidsRecycler=(RecyclerView)view.findViewById(R.id.recycler_kidsCloth);

       genderId=1;
        Log.d(SplashScreen.TAG,"service Id-"+serviceId);
        Log.d(SplashScreen.TAG,"gender Id-"+genderId);
        Log.d(SplashScreen.TAG,"branch Id-"+branchId);

        if(serviceId!=0&&genderId!=0&&branchId!=null) {
            new getClothData().execute();
        }else{
            Toast.makeText(getActivity(),"Please select Category and Service",Toast.LENGTH_SHORT).show();
        }



        menExpand.setOnClickListener(this);
        menCol.setOnClickListener(this);
        womenCol.setOnClickListener(this);
        womenExpand.setOnClickListener(this);
        kidsCol.setOnClickListener(this);
        kidsExpand.setOnClickListener(this);
        return view;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {

            case R.id.menExpand:
                genderId=1;
                menCol.setVisibility(View.VISIBLE);
                menExpand.setVisibility(View.GONE);

                womenCol.setVisibility(View.GONE);
                womenExpand.setVisibility(View.VISIBLE);

                kidsCol.setVisibility(View.GONE);
                kidsExpand.setVisibility(View.VISIBLE);

                menLinearOption.setVisibility(View.VISIBLE);
                womenLinearOption.setVisibility(View.GONE);
                kidsLinearOption.setVisibility(View.GONE);
                if(serviceId!=0&&genderId!=0&&branchId!=null) {
                    new getClothData().execute();
                }else{
                    Toast.makeText(getActivity(),"Please select Category and Service",Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.menCol:

                menCol.setVisibility(View.GONE);
                menExpand.setVisibility(View.VISIBLE);

                womenCol.setVisibility(View.GONE);
                womenExpand.setVisibility(View.VISIBLE);

                kidsCol.setVisibility(View.GONE);
                kidsExpand.setVisibility(View.VISIBLE);

                menLinearOption.setVisibility(View.GONE);
                womenLinearOption.setVisibility(View.GONE);
                kidsLinearOption.setVisibility(View.GONE);

                break;
            case R.id.womenExpand:
                genderId=2;
                menCol.setVisibility(View.GONE);
                menExpand.setVisibility(View.VISIBLE);

                womenCol.setVisibility(View.VISIBLE);
                womenExpand.setVisibility(View.GONE);

                kidsCol.setVisibility(View.GONE);
                kidsExpand.setVisibility(View.VISIBLE);

                menLinearOption.setVisibility(View.GONE);
                womenLinearOption.setVisibility(View.VISIBLE);
                kidsLinearOption.setVisibility(View.GONE);
                if(serviceId!=0&&genderId!=0&&branchId!=null) {
                    new getClothData().execute();
                }else{
                    Toast.makeText(getActivity(),"Please select Category and Service",Toast.LENGTH_SHORT).show();
                }
                break;


            case R.id.womenCol:

                menCol.setVisibility(View.GONE);
                menExpand.setVisibility(View.VISIBLE);

                womenCol.setVisibility(View.GONE);
                womenExpand.setVisibility(View.VISIBLE);

                kidsCol.setVisibility(View.GONE);
                kidsExpand.setVisibility(View.VISIBLE);

                menLinearOption.setVisibility(View.GONE);
                womenLinearOption.setVisibility(View.GONE);
                kidsLinearOption.setVisibility(View.GONE);
                break;

            case R.id.kidsExpand:
                genderId=3;
                menCol.setVisibility(View.GONE);
                menExpand.setVisibility(View.VISIBLE);

                womenCol.setVisibility(View.GONE);
                womenExpand.setVisibility(View.VISIBLE);

                kidsCol.setVisibility(View.VISIBLE);
                kidsExpand.setVisibility(View.GONE);

                menLinearOption.setVisibility(View.GONE);
                womenLinearOption.setVisibility(View.GONE);
                kidsLinearOption.setVisibility(View.VISIBLE);
                if(serviceId!=0&&genderId!=0&&branchId!=null) {
                    new getClothData().execute();
                }else{
                    Toast.makeText(getActivity(),"Please select Category and Service",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.kidsCol:

                menCol.setVisibility(View.GONE);
                menExpand.setVisibility(View.VISIBLE);

                womenCol.setVisibility(View.GONE);
                womenExpand.setVisibility(View.VISIBLE);

                kidsCol.setVisibility(View.GONE);
                kidsExpand.setVisibility(View.VISIBLE);

                menLinearOption.setVisibility(View.GONE);
                womenLinearOption.setVisibility(View.GONE);
                kidsLinearOption.setVisibility(View.GONE);
                break;

        }
    }

    private class getClothData extends AsyncTask<Void, Void, Void> {
        String url=null;
        private int error=0;


        @Override
        protected Void doInBackground(Void... params) {


            url = new StringBuilder("http://easyghaat.cashbachat.com/api/Product_Price/ByCat_Service_GenderId/")
                    .append("3")
                    .append("/")
                    .append(serviceId)
                    .append("/")
                    .append(genderId)
                    .append("/")
                    .append(branchId)
                    .toString();



            WebService_Hit webService_hit = new WebService_Hit();
            String json = webService_hit.getUrlContents(url);
            if (webService_hit.responseCode!=200) {
                error=1;
            }else {
                error=0;
                productInfoData(json);
            }

            Log.d(SplashScreen.TAG, json);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(error==1)
            {
                Toast.makeText(getActivity(), "Unable to get data,Please try again ", Toast.LENGTH_SHORT).show();
            }else {
                adapter = new ClothOption_Adapter(dataSelected,getActivity());

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
                RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(getActivity());
                menRecycler.setLayoutManager(mLayoutManager);
                menRecycler.setItemAnimator(new DefaultItemAnimator());
                menRecycler.setAdapter(adapter);


                womenRecycler.setLayoutManager(mLayoutManager1);
                womenRecycler.setItemAnimator(new DefaultItemAnimator());
                womenRecycler.setAdapter(adapter);


                kidsRecycler.setLayoutManager(mLayoutManager2);
                kidsRecycler.setItemAnimator(new DefaultItemAnimator());
                kidsRecycler.setAdapter(adapter);
            }
        }



        private void productInfoData(String sJson) {
            if (sJson != null) {

                try {
                    dataSelected=new ArrayList<>();
                    ProductData data = new ProductData();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if (jArray.length() != 0) {
                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject object = jArray.getJSONObject(i);
                            data = data.ProductDataJsonTo(object);
                            dataSelected.add(data);
                        }
                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }

    }
}
