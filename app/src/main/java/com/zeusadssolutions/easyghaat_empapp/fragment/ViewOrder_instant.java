package com.zeusadssolutions.easyghaat_empapp.fragment;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.adapter.MyTask_Adapter;
import com.zeusadssolutions.easyghaat_empapp.model.EmpInfo;
import com.zeusadssolutions.easyghaat_empapp.model.InstantOrderDetails;
import com.zeusadssolutions.easyghaat_empapp.model.InvoiceData;
import com.zeusadssolutions.easyghaat_empapp.model.TaskCancelReason;
import com.zeusadssolutions.easyghaat_empapp.model.TaskDetails_UserData;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;
import com.zeusadssolutions.easyghaat_empapp.utils.StartNewActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ViewOrder_instant extends Fragment implements View.OnClickListener {

    Button btnEditOrder_viewOrder,btnCancelOrder_instantOrder;
    ImageView imgCallNow;
    TextView txtCallNow,textCustomerContactDetail_instantOrder,
            textCustomerAdd_instantOrder,textPickupAdd_instantOrder,
            textTimeSlot_instantOrder,txtServiceName_instant,txtDeliveryType_instant,
            txtCount_instantView;
    ArrayList<TaskCancelReason> reasonArrayList;
    String url;
    PrefManager prefManager;
    AlertDialog alertDialog1;
    EmpInfo empInfo;
    TaskDetails_UserData userData;
    ArrayList<InstantOrderDetails> orderDetailsArrayList;


    public ViewOrder_instant() {

    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_view_order_instant, container, false);
        // Inflate the layout for this fragment
        imgCallNow=(ImageView)view.findViewById(R.id.imgCallNow_instant_view);
        txtCallNow=(TextView)view.findViewById(R.id.textCallNow_instantView);

        txtCount_instantView=(TextView)view.findViewById(R.id.txtCount_instantView);
        textTimeSlot_instantOrder=(TextView)view.findViewById(R.id.textTimeSlot_instantOrder);
        txtServiceName_instant=(TextView)view.findViewById(R.id.txtServiceName_instant);
        txtDeliveryType_instant=(TextView)view.findViewById(R.id.txtDeliveryType_instant);
        textCustomerAdd_instantOrder=(TextView)view.findViewById(R.id.textCustomerAdd_instantOrder);
        textPickupAdd_instantOrder=(TextView)view.findViewById(R.id.textPickupAdd_instantOrder);



        btnEditOrder_viewOrder =(Button)view.findViewById(R.id.btnEditOrder_instantView);
        textCustomerContactDetail_instantOrder=(TextView)view.findViewById(R.id.textCustomerContactDetail_instantOrder);
        btnCancelOrder_instantOrder=(Button)view.findViewById(R.id.btnCancelOrder_instantView);
        imgCallNow.setOnClickListener(this);
        txtCallNow.setOnClickListener(this);

        prefManager=new PrefManager(getContext());
        empInfo=prefManager.EmpInfo_getObject("EmpData");
        userData= MyTask_Adapter.myTaskData;
        orderDetailsArrayList=MyTask_Adapter.instantOrderDetails;



        textCustomerAdd_instantOrder.setText(userData.getCustomerId()+"\n"+userData.getCustomerName());
        txtCount_instantView.setText(userData.getExpectedCount());
        textCustomerContactDetail_instantOrder.setText(userData.getCustomerMobileNo());
        textPickupAdd_instantOrder.setText("House No.- "+userData.getCUSTPICKHOUSENO()+"\nStreet -\t"+userData.getCUSTPICKSTREETNAME()+"\nNear: \t"
                +userData.getCUSTPICKLANDMARK());
        textTimeSlot_instantOrder.setText(userData.getPickUpDate().substring(0, 10).trim()+"\n"+userData.getPickupFromtime()+"-"+userData.getPickupTotime());

        String instantService=new String();
        for(int i=0;i<orderDetailsArrayList.size();i++)
        {
            instantService=new StringBuilder(instantService)
                    .append(orderDetailsArrayList.get(i).getServiceName()+", ")
                    .toString();
        }
        txtServiceName_instant.setText(instantService);

        txtDeliveryType_instant.setText(userData.getDeliveryType());



        btnEditOrder_viewOrder.setOnClickListener(this);

        btnCancelOrder_instantOrder.setOnClickListener(this);


        return view;
    }



    void callNow()
    {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + textCustomerContactDetail_instantOrder.getText().toString()));
            startActivity(callIntent);
        } catch (ActivityNotFoundException activityException) {
            Log.e(SplashScreen.TAG, "Call failed", activityException);
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnEditOrder_instantView:
                String  fragmentName=getActivity().getResources().getString(R.string.edit_order);
                StartNewActivity activity=new StartNewActivity();
                activity.showActivity(getActivity(), fragmentName);
                getActivity().finish();
                break;
            case R.id.imgCallNow_instant_view:
                callNow();
                break;
            case R.id.textCallNow_instantView:
                callNow();
                break;

            case R.id.btnCancelOrder_instantView:
                reasonArrayList=new ArrayList<>();
                new getReason().execute();
                break;


        }
    }



    private class getReason extends AsyncTask<Void, Void, Void> {

        int error=0;
        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();
            Log.d(SplashScreen.TAG, "RoleId---" + empInfo.getRoleId());
            String url = new StringBuilder(SplashScreen.DOMAIN+"api/CancelReasons/?RoleId=")
                    .append(empInfo.getRoleId().trim())
                    .toString();
            String json = webService_hit.getUrlContents(url);
            Log.d(SplashScreen.TAG, json);

            boolean digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;


            }else {
                error=0;
                rejectionReason(json);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if(error==1)
            {
                Toast.makeText(getActivity(), "Unable to get reasons,Please try again ", Toast.LENGTH_SHORT).show();
            }else {
                CreateAlertDialogWithRadioButtonGroup();
            }
        }

        private void rejectionReason(String sJson) {
            if (sJson != null) {

                try {

                    TaskCancelReason taskCancelReason = new TaskCancelReason();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if (jArray.length() != 0) {

                        for (int i = 0; i < jArray.length(); i++) {


                            JSONObject object = jArray.getJSONObject(i);

                            taskCancelReason = taskCancelReason.TaskRasonJsonTo(object);

                           reasonArrayList.add(taskCancelReason);
                            Log.d(SplashScreen.TAG, "cancel reason :---" + taskCancelReason.getReason());

                        }


                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }
        }


        public void CreateAlertDialogWithRadioButtonGroup() {


            CharSequence[] values = new CharSequence[reasonArrayList.size()];
            for (int i = 0; i < reasonArrayList.size(); i++) {
                values[i] = reasonArrayList.get(i).getReason();

                Log.d(SplashScreen.TAG, "reason----" + reasonArrayList.get(i).getReason());
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setTitle("Select Your Reason");

            builder.setSingleChoiceItems(values, -1, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int item) {
                    url = new StringBuilder(SplashScreen.DOMAIN+"api/OrderCancels/InsertCancelOrder/?EmployeeID=")
                            .append(empInfo.getEmp_id().trim())
                            .append("&CancelReasonId=")
                            .append(reasonArrayList.get(item).getId().trim())
                            .append("&OrderId=")
                            .append(userData.getOrderId().trim())
                            .append("&RoleTypeId=")
                            .append(empInfo.getRoleId().trim())
                            .toString();
                    new cancelOrderData().execute();
                    alertDialog1.dismiss();
                }
            });
            alertDialog1 = builder.create();
            alertDialog1.show();

        }


    }
    private class cancelOrderData extends AsyncTask<Void, Void, Void> {
        int error=0;
        boolean digitsOnly;
        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();
            String json = webService_hit.postUrlContents(url);
            Log.d(SplashScreen.TAG, json);
            digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;


            }else {
                error = 0;

                String urlOrderStatus = new StringBuilder(SplashScreen.DOMAIN + "api/CancelReasons/PutUpdateOrderStatus/?OrderId=")
                        .append(userData.getOrderId().trim()).toString();
                String orderStatus = webService_hit.putUrlContents(urlOrderStatus);
                Log.d(SplashScreen.TAG, orderStatus);
                digitsOnly = TextUtils.isDigitsOnly(orderStatus);
                if (digitsOnly) {
                    error = 1;
                } else {
                    error = 0;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if(error==1)
            {
                Toast.makeText(getActivity(), "Unable to get reasons,Please try again ", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(getActivity(), "Order has been canceled", Toast.LENGTH_SHORT).show();
                StartNewActivity startNewActivity = new StartNewActivity();
                startNewActivity.showActivity(getActivity(), getActivity().getResources().getString(R.string.my_task));
                getActivity().finish();
            }


        }
    }
}
