package com.zeusadssolutions.easyghaat_empapp.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.activity.Invoice;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.adapter.EditLaundryBag_Adapter;
import com.zeusadssolutions.easyghaat_empapp.adapter.EditOrderAdapter;
import com.zeusadssolutions.easyghaat_empapp.adapter.GetLaundryBag_Adapter;
import com.zeusadssolutions.easyghaat_empapp.adapter.MyTask_Adapter;
import com.zeusadssolutions.easyghaat_empapp.adapter.OrderDetails_Adapter;
import com.zeusadssolutions.easyghaat_empapp.adapter.LaundryBag_Adapter;
import com.zeusadssolutions.easyghaat_empapp.model.EmpInfo;
import com.zeusadssolutions.easyghaat_empapp.model.GetLaundryBagData;
import com.zeusadssolutions.easyghaat_empapp.model.InstantOrderDetails;
import com.zeusadssolutions.easyghaat_empapp.model.InvoiceData;
import com.zeusadssolutions.easyghaat_empapp.model.LaundryBagDetails;
import com.zeusadssolutions.easyghaat_empapp.model.MakeOrderDetails;
import com.zeusadssolutions.easyghaat_empapp.model.MyTaskData;
import com.zeusadssolutions.easyghaat_empapp.model.TaskCancelReason;
import com.zeusadssolutions.easyghaat_empapp.model.TaskDetails_UserData;
import com.zeusadssolutions.easyghaat_empapp.utils.UserAlert_Dialog;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;
import com.zeusadssolutions.easyghaat_empapp.utils.StartNewActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by HP on 04-10-2016.
 */
public class ViewOrder_makeOrder extends Fragment {
    /*declare views*/
    private Button btnInvoice_makeOrderView, btnEditOrder_makeView, btnCancelOrder_makeView,btnDelivery_makeOrderView;

    private TextView textdate,txtClothCount_makeOrderView,
            textCustomerId_makeOrderView, textCustomerName_makeOrderView,
            textAmountReceived_makeOrderView,
            txtServiceType_makeOrderView;

    public static   RecyclerView orderListRecycle,laundryBag_recycler,geLaundryBag_recycler;
    private LinearLayout linearEdit_Order,linearLaundryBag,linearDelivery,linearInvoice;
    public static LinearLayout linearEditLaundry_makeOrder,linearGetBagDetails;

    private  AlertDialog alertDialog1;
    /*declare ArrayList*/
    private  ArrayList<TaskCancelReason> reasonArrayList;
    private   List<MyTaskData> taskList;
    private  ArrayList<MakeOrderDetails> orderDetailsList;
    private ArrayList<LaundryBagDetails> laundryBagDetailses;
    public static  ArrayList<GetLaundryBagData> getLaundryBagDataArrayList;

    /*declare variables*/
    private PrefManager prefManager;
    private EmpInfo empInfo;
    public static   TaskDetails_UserData userData;
    public static GetLaundryBag_Adapter getLaundryBagAdapter;
    private ProgressBar progressBar;
    public static  int changeOrder=0,qty=0,position=0,laundryPosition=0,changeLaundry=0,qtyBag=0,selectBag=0;
    private String url;
    private  int error=0,count, totalCount;
    public static InvoiceData invoiceData;
    /*default constructor*/
    public ViewOrder_makeOrder() {
    }

    /*onCreateView method*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_vieworder_makeorder, container, false);

        prefManager = new PrefManager(getContext());
        empInfo = prefManager.EmpInfo_getObject("EmpData");
        userData=new TaskDetails_UserData();
        userData = MyTask_Adapter.myTaskData;

        taskList=new ArrayList<>();
        taskList=MyTask.taskList;
        Log.d(SplashScreen.TAG,"customer name--"+userData.getCustomerName());
        /*initialize TextView*/
        textCustomerId_makeOrderView=(TextView)view.findViewById(R.id.textCustomerId_makeOrderView);
        textCustomerName_makeOrderView=(TextView)view.findViewById(R.id.textCustomerName_makeOrderView);
        textAmountReceived_makeOrderView=(TextView)view.findViewById(R.id.textAmountReceived_makeOrderView);
        txtClothCount_makeOrderView=(TextView)view.findViewById(R.id.txtClothCount_makeOrderView);
        txtServiceType_makeOrderView=(TextView)view.findViewById(R.id.txtServiceType_makeOrderView);
        textdate = (TextView) view.findViewById(R.id.textPickupDate_makeOrderView);


        /*initialize LinearLayout*/
        linearEdit_Order=(LinearLayout)view.findViewById(R.id.linearEdit_makeOrder);
        linearLaundryBag = (LinearLayout) view.findViewById(R.id.linearLaundryBag);
        linearDelivery = (LinearLayout) view.findViewById(R.id.linearDelivery);
        linearInvoice = (LinearLayout) view.findViewById(R.id.linearInvoice);
        linearGetBagDetails=(LinearLayout)view.findViewById(R.id.linearGetBagDetails);
        linearEditLaundry_makeOrder=(LinearLayout)view.findViewById(R.id.linearEditLaundry_makeOrder);

        /*initialize Button*/
        btnInvoice_makeOrderView = (Button) view.findViewById(R.id.btnInvoice_makeOrderView);
        btnEditOrder_makeView = (Button) view.findViewById(R.id.btnEditOrder_makeView);
        btnCancelOrder_makeView = (Button) view.findViewById(R.id.btnCancelOrder_makeView);
        btnDelivery_makeOrderView = (Button) view.findViewById(R.id.btnDelivery_makeOrderView);
        /*initialize RecyclerView*/
        orderListRecycle = (RecyclerView) view.findViewById(R.id.orderDetail_recycler);
        laundryBag_recycler=(RecyclerView)view.findViewById(R.id.laundryBag_recycler);
        geLaundryBag_recycler=(RecyclerView)view.findViewById(R.id.geLaundryBag_recycler);
        /*initialize progressBar*/


        progressBar=(ProgressBar)view.findViewById(R.id.progressMakeOrderView);
        orderListRecycle.setHasFixedSize(true);


            if(userData.getType().equals("Delivery"))
            {
                linearEdit_Order.setVisibility(View.GONE);
                linearLaundryBag.setVisibility(View.GONE);
                linearInvoice.setVisibility(View.GONE);
                linearDelivery.setVisibility(View.VISIBLE);

                setUserData();
                if(orderDetailsList!=null) {
                    btnDelivery_makeOrderView.setEnabled(true);
                    btnDelivery_makeOrderView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new deliveryOrder().execute();
                        }
                    });
                }


            }else {

                new getOrderDetails().execute();


            }
        return view;
    }


    private class getOrderDetails extends AsyncTask<Void, Void, Void>{
        private ProgressDialog dialog;
        int error=0;boolean digitsOnly;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {

            String  url = new StringBuilder(SplashScreen.DOMAIN+"api/ViewMakeOrder/MakeOrder/?TaskId=")
                    .append(taskList.get(position).getTASKID().trim())
                    .toString();

            WebService_Hit webService_hit = new WebService_Hit();
            String json = webService_hit.getUrlContents(url);
            Log.d(SplashScreen.TAG, "customer info-" + json);

            if (webService_hit.responseCode!=200) {
                error=1;


            }else {
                error=0;

                MyOrderData(json);

                    String instantUrl = new StringBuilder(SplashScreen.DOMAIN + "api/ViewMakeOrder/MakeOrderDetail/?OrderId=")
                            .append(taskList.get(position).getORDERID().trim())
                            .toString();
                    String makeJson = webService_hit.getUrlContents(instantUrl);
                    Log.d(SplashScreen.TAG, "OrderDetails-" + makeJson);


                    if (webService_hit.responseCode!=200) {
                        error=1;


                    }else {
                        error = 0;
                        MakeOrderData(makeJson);


                }

            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            String fragmentName;
            super.onPostExecute(result);
            // Dismiss the progress dialog

            if(error==1)
            {
                Toast.makeText(getActivity(), "Unable to get task details,Please try again ", Toast.LENGTH_SHORT).show();
            }else {
                linearEdit_Order.setVisibility(View.VISIBLE);
                linearLaundryBag.setVisibility(View.VISIBLE);
                linearInvoice.setVisibility(View.VISIBLE);
                linearDelivery.setVisibility(View.GONE);


                setUserData();
                new getOrderedLaundryBag().execute();
            }

        }


        private void MyOrderData(String sJson) {
            if (sJson != null) {
                try {

                    userData=new TaskDetails_UserData();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if(jArray.length()!=0) {

                        JSONObject object = jArray.getJSONObject(0);

                        userData = userData.TaskDetailsJsonTo(object);
                        Log.d(SplashScreen.TAG, "my order id received :---" + userData.getOrderId());

                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }




        private void MakeOrderData(String sJson) {
            if (sJson != null) {

                orderDetailsList=new ArrayList<>();
                try {

                    MakeOrderDetails myTaskData=new MakeOrderDetails();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if(jArray.length()!=0) {
                        for (int i=0;i<jArray.length();i++) {
                            JSONObject object = jArray.getJSONObject(i);

                            myTaskData = myTaskData.MakeOrdersJsonTo(object);
                            Log.d(SplashScreen.TAG, "my product name received :---" + myTaskData.getProductName());
                            orderDetailsList.add(myTaskData);


                        }

                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }

    }

    /*delivery Order*/

    private class deliveryOrder extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressBar.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();
            Log.d(SplashScreen.TAG, "RoleId---" + empInfo.getRoleId());
            String deliveryUrl=new StringBuilder(SplashScreen.DOMAIN+"api/Delivery/ByTaskId/")
                    .append(taskList.get(MyTask_Adapter.position).getTASKID().trim())
                    .toString();
            String makeJson = webService_hit.getUrlContents(deliveryUrl);
            Log.d(SplashScreen.TAG, "Delivery response-" + makeJson);
            //boolean digitsOnly = TextUtils.isDigitsOnly(makeJson);
            if (webService_hit.responseCode==200) {
                error=0;

            }else {
                error=1;
               // MakeOrderData(makeJson);
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            progressBar.setVisibility(View.GONE);
            if(error==1)
            {
                UserAlert_Dialog alert_dialog=new UserAlert_Dialog("Unable to refresh, Please try again",
                        getActivity().getResources().getString(R.string.my_task),getActivity(),R.mipmap.cross);

                alert_dialog.showDialog_newFragment();

            }else {

                UserAlert_Dialog alert_dialog=new UserAlert_Dialog("Delivery to customer",
                        getActivity().getResources().getString(R.string.my_task),getActivity(),R.mipmap.tick);

                alert_dialog.showDialog_newFragment();


            }

        }



    }

    /*Showing task data */
    private void setUserData()
    {
        OrderDetails_Adapter adapter = new OrderDetails_Adapter(orderDetailsList, getActivity());
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        orderListRecycle.setLayoutManager(llm);

        orderListRecycle.setAdapter(adapter);

        double totalAmount = 0.0;
        totalCount = 0;
        double subAmount = 0.0;
        for (int i = 0; i < orderDetailsList.size(); i++) {
            Log.d(SplashScreen.TAG, "count---" + orderDetailsList.get(i).getQunatity());
            count = Integer.parseInt(orderDetailsList.get(i).getQunatity());

            Log.d(SplashScreen.TAG, "sub amount---" + orderDetailsList.get(i).getSubAmount());
            subAmount = Double.parseDouble(orderDetailsList.get(i).getSubAmount());

            totalCount = totalCount + count;

            totalAmount = totalAmount + subAmount;

        }

        textCustomerId_makeOrderView.setText("Id- " + userData.getCustomerId());
        ;
        textCustomerName_makeOrderView.setText( userData.getCustomerName());
        txtServiceType_makeOrderView.setText(userData.getType());

        txtClothCount_makeOrderView.setText(String.valueOf(totalCount));
        textdate.setText(userData.getPickupFromtime()+"-"+userData.getPickupTotime());
        String userAddress=new StringBuilder(userData.getCUSTPICKHOUSENO()+"\t")
                                .append(userData.getCUSTPICKSTREETNAME()+"\n")
                                .append(userData.getCUSTPICKLANDMARK()+"\n")
                                .append(userData.getCUSTPICKCITY())
                                .toString();
        textAmountReceived_makeOrderView.setText(userAddress);

    }

    private void setGetLaundryBagData()
    {
        linearEditLaundry_makeOrder.setVisibility(View.VISIBLE);
        linearGetBagDetails.setVisibility(View.VISIBLE);
        getLaundryBagAdapter = new GetLaundryBag_Adapter(getActivity(),getLaundryBagDataArrayList);

        LinearLayoutManager verLayoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        geLaundryBag_recycler.setLayoutManager(verLayoutManager);

        geLaundryBag_recycler.setAdapter(getLaundryBagAdapter);
    }
    /*to get laundry bag details reason*/
    private class getLaundryBag extends AsyncTask<Void, Void, Void> implements View.OnClickListener {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
       progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();
            Log.d(SplashScreen.TAG, "RoleId---" + empInfo.getRoleId());
            String url = new StringBuilder(SplashScreen.DOMAIN+"api/LaundryBag_Tbl/Details ")
                    .toString();
            String json = webService_hit.getUrlContents(url);
            Log.d(SplashScreen.TAG, "Laundry bag details---" + json);
            boolean digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;


            }else {
                error=0;
                bagDetails(json);
            }



            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);
            if(error==1)
            {
                UserAlert_Dialog alert_dialog=new UserAlert_Dialog("Unable to get bag details, Please try again",
                        getActivity().getResources().getString(R.string.my_task),getActivity(),R.mipmap.cross);
                alert_dialog.showDialog_newFragment();
                getActivity().finish();
            }else {

                setUserData();
                btnInvoice_makeOrderView.setOnClickListener(this);
                btnEditOrder_makeView.setOnClickListener(this);
                btnCancelOrder_makeView.setOnClickListener(this);
                linearEdit_Order.setOnClickListener(this);
                linearEditLaundry_makeOrder.setOnClickListener(this);
                Log.d(SplashScreen.TAG, "image url--" + laundryBagDetailses.get(0).getBagImgUrl());

                LaundryBag_Adapter laundryAdapter = new LaundryBag_Adapter(getActivity(), laundryBagDetailses);

                LinearLayoutManager horLayoutManager1
                        = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                laundryBag_recycler.setLayoutManager(horLayoutManager1);

                laundryBag_recycler.setAdapter(laundryAdapter);
            }
        }
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.btnCancelOrder_makeView:
                    reasonArrayList=new ArrayList<>();
                    new getReason().execute();
                    break;

                case R.id.btnEditOrder_makeView:
                    String fragmentName = getActivity().getResources().getString(R.string.edit_order);
                    StartNewActivity activity = new StartNewActivity();
                    activity.showActivity(getActivity(), fragmentName);
                    getActivity().finish();
                    break;

                case R.id.btnInvoice_makeOrderView:


                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                        builder.setTitle("EasyGhaat");
                        builder.setMessage("Do You want to generate Invoice!")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                      new invoiceOrderCreate().execute();
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        android.app.AlertDialog alert = builder.create();
                        alert.show();

                    break;

                case R.id.linearEdit_makeOrder:
                    CreateAlertDialogWithCheckBox();
                    break;

                case R.id.linearEditLaundry_makeOrder:

                    CreateAlertDialogWithCheckBoxFor_Laundry();
                    break;
            }
        }

              private void CreateAlertDialogWithCheckBoxFor_Laundry()
        {

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            LayoutInflater inflater = LayoutInflater.from(getActivity());
            final View dialogView = inflater.inflate(R.layout.edit_order_dialogbox, null);
            builder.setView(dialogView);

            EditLaundryBag_Adapter editLaundryBagAdapter=new EditLaundryBag_Adapter(getLaundryBagDataArrayList, getActivity());

            final  RecyclerView editOrderListRecycle = (RecyclerView) dialogView.findViewById(R.id.orderDetailEdit_recycler);
            final ImageView deleteImage=(ImageView)dialogView.findViewById(R.id.img_deleteRow);

            editOrderListRecycle.setHasFixedSize(true);


            LinearLayoutManager llm = new LinearLayoutManager(getContext());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            editOrderListRecycle.setLayoutManager(llm);
            editOrderListRecycle.setAdapter(editLaundryBagAdapter);

            builder.setTitle("Edit Order");

            deleteImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(getActivity(), "updating information", Toast.LENGTH_SHORT).show();
                    new deletedLaundryBagData().execute();
                    alertDialog1.dismiss();
                }
            });


            builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {

                    Log.d(SplashScreen.TAG,"Laundry bag change----"+String.valueOf(changeLaundry));
                    if(changeLaundry==0)
                    {
                        dialog.cancel();
                    }else {


                        new updateLaundryBag().execute();
                    }



                }
            })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //  Your code when user clicked on Cancel
                            dialog.cancel();

                        }
                    });

            alertDialog1 = builder.create();
            alertDialog1.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            alertDialog1.show();

        }

              private void CreateAlertDialogWithCheckBox()
        {

            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            final View dialogView = inflater.inflate(R.layout.edit_order_dialogbox, null);
            builder.setView(dialogView);




            EditOrderAdapter editOrderAdapter=new EditOrderAdapter(orderDetailsList, getActivity());

            final  RecyclerView editOrderListRecycle = (RecyclerView) dialogView.findViewById(R.id.orderDetailEdit_recycler);
            final ImageView deleteImage=(ImageView)dialogView.findViewById(R.id.img_deleteRow);

            editOrderListRecycle.setHasFixedSize(true);


            LinearLayoutManager llm = new LinearLayoutManager(getContext());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            editOrderListRecycle.setLayoutManager(llm);
            editOrderListRecycle.setAdapter(editOrderAdapter);

            builder.setTitle("Edit Order");

            deleteImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // Toast.makeText(getActivity(), "delete button is clicked", Toast.LENGTH_SHORT).show();
                    new deletedRowData().execute();
                    alertDialog1.dismiss();
                }
            });


            builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {

                    Log.d(SplashScreen.TAG,"Order change----"+String.valueOf(changeOrder));
                    if(changeOrder==0)
                    {
                        dialog.cancel();
                    }else {


                        new updateOrderData().execute();
                    }



                }
            })
                    .setNegativeButton("Add More", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //  Your code when user clicked on Cancel
                            String fragmentName = getActivity().getResources().getString(R.string.edit_order);
                            StartNewActivity activity = new StartNewActivity();
                            activity.showActivity(getActivity(), fragmentName);

                        }
                    });

            alertDialog1 = builder.create();
            alertDialog1.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            alertDialog1.show();

        }

        private void bagDetails(String sJson) {
            if (sJson != null) {

                laundryBagDetailses=new ArrayList<>();
                try {

                    LaundryBagDetails details=new LaundryBagDetails();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if(jArray.length()!=0) {
                        for (int i=0;i<jArray.length();i++) {
                            JSONObject object = jArray.getJSONObject(i);

                            details = details.MakeOrdersJsonTo(object);
                            Log.d(SplashScreen.TAG, "my product name received :---" + details.getMaterialName());
                            laundryBagDetailses.add(details);
                        }

                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }


    }


    /*to get order laundry bag details reason*/
    private class getOrderedLaundryBag extends AsyncTask<Void, Void, Void>{
        int errorValue=0;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();
            Log.d(SplashScreen.TAG, "RoleId---" + empInfo.getRoleId());
            String url = new StringBuilder(SplashScreen.DOMAIN+"api/Order_LaundryBag_Tbl/GetOrderLaundryBag/ByOrderId/")
                    .append(userData.getOrderId())
                    .toString();
            String json = webService_hit.getUrlContents(url);
            Log.d(SplashScreen.TAG, "Laundry bag details---" + json);
            boolean digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;
                errorValue=Integer.valueOf(json);

                Log.d(SplashScreen.TAG,"error Value--"+json);

            }else {
                error=0;
                getBagDetails(json);
            }



            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);
            if(error==1)
            {
                selectBag=0;
                    linearEditLaundry_makeOrder.setVisibility(View.GONE);
                    linearGetBagDetails.setVisibility(View.GONE);

            }else {
                selectBag=1;

                setGetLaundryBagData();


            }
            new getLaundryBag().execute();
        }

        private void getBagDetails(String sJson) {
            if (sJson != null) {

                getLaundryBagDataArrayList=new ArrayList<>();
                try {

                    GetLaundryBagData details=new GetLaundryBagData();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if(jArray.length()!=0) {
                        for (int i=0;i<jArray.length();i++) {
                            JSONObject object = jArray.getJSONObject(i);

                            details = details.GetLaundryBagDataJsonTo(object);
                            Log.d(SplashScreen.TAG, "my product name received :---" + details.getMaterialName());
                            getLaundryBagDataArrayList.add(details);
                        }

                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }


    }


    /*to get order laundry bag details reason*/
    private class refreshOrderedLaundryBag extends AsyncTask<Void, Void, Void>{
        int errorValue=0;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();
            Log.d(SplashScreen.TAG, "RoleId---" + empInfo.getRoleId());
            String url = new StringBuilder(SplashScreen.DOMAIN+"api/Order_LaundryBag_Tbl/GetOrderLaundryBag/ByOrderId/")
                    .append(userData.getOrderId())
                    .toString();
            String json = webService_hit.getUrlContents(url);
            Log.d(SplashScreen.TAG, "Laundry bag details---" + json);
            boolean digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;
                errorValue=Integer.valueOf(json);

                Log.d(SplashScreen.TAG,"error Value--"+json);

            }else {
                error=0;
                getBagDetails(json);
            }



            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);
            if(error==1)
            {
                selectBag=0;
                linearEditLaundry_makeOrder.setVisibility(View.GONE);
                linearGetBagDetails.setVisibility(View.GONE);

            }else {
                selectBag=1;

                setGetLaundryBagData();


            }
        }

        private void getBagDetails(String sJson) {
            if (sJson != null) {

                getLaundryBagDataArrayList=new ArrayList<>();
                try {

                    GetLaundryBagData details=new GetLaundryBagData();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if(jArray.length()!=0) {
                        for (int i=0;i<jArray.length();i++) {
                            JSONObject object = jArray.getJSONObject(i);

                            details = details.GetLaundryBagDataJsonTo(object);
                            Log.d(SplashScreen.TAG, "my product name received :---" + details.getMaterialName());
                            getLaundryBagDataArrayList.add(details);
                        }

                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }


    }

    /*generating Invoice*/
    private class invoiceOrderCreate extends AsyncTask<Void,Void,Void> {
        InvoiceData myTaskData;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
           progressBar.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();

            String instantUrl=new StringBuilder(SplashScreen.DOMAIN+"api/OrderDetail_tbl/GenerateInvoice/")
                    .append(userData.getOrderId().trim())
                    .toString();
            String makeJson = webService_hit.getUrlContents(instantUrl);
            Log.d(SplashScreen.TAG, "Invoice-" + makeJson);

            boolean digitsOnly = TextUtils.isDigitsOnly(makeJson);
            if (digitsOnly) {
                error=1;
            }else {
                error=0;
                InvoiceOrderData(makeJson);
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);
            if(error==1)
            {
                UserAlert_Dialog dialog = new UserAlert_Dialog("Unable to generate Invoice, Please try again",getActivity().getResources().getString(R.string.my_task),
                        getActivity(),R.mipmap.cross);
                dialog.showDialog_newFragment();

            }else {
                if (myTaskData != null) {
                    Intent intent = new Intent(getActivity(), Invoice.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    getActivity().finish();
                } else {
                    UserAlert_Dialog dialog = new UserAlert_Dialog("Invoice is not generated, Please try again", getActivity(),R.mipmap.cross);
                    dialog.showDialog_finish();
                }

            }
        }

        private void InvoiceOrderData(String sJson) {
            if (sJson != null) {
               // prefManager=new PrefManager(getActivity());
                try {

                    myTaskData=new InvoiceData();
                    // Getting JSON Array node
                    JSONObject object=new JSONObject(sJson);
                    myTaskData = myTaskData.InvoiceJsonTo(object);
                            Log.d(SplashScreen.TAG, "order Id received :---" + myTaskData.getOrderId());
                            invoiceData=myTaskData;


                    } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());

                }
            }

        }
    }


    /*to update data*/
    private class updateOrderData extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressBar.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();

            Log.d(SplashScreen.TAG,"orderDetails id--"+orderDetailsList.get(position).getOrderDetailId());
            String instantUrl=new StringBuilder(SplashScreen.DOMAIN+"/api/OrderDetail_tbl/UpdateOrderQty/")
                    .append(orderDetailsList.get(position).getOrderDetailId().trim())
                    .append("/"+qty)
                    .toString();
            String makeJson = webService_hit.getUrlContents(instantUrl);
            Log.d(SplashScreen.TAG, "OrderDetails-" + makeJson);

            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);
            new getRefreshData().execute();

        }

    }


    /*to update laundry bag data*/
    private class updateLaundryBag extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressBar.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();

            Log.d(SplashScreen.TAG, "OrderBagId--" + getLaundryBagDataArrayList.get(laundryPosition).getOrderBagId());
            Log.d(SplashScreen.TAG, "bag qty--" + qtyBag);
            String instantUrl=new StringBuilder(SplashScreen.DOMAIN+"api/Order_LaundryBag_Tbl/UpdateLaundry/")
                    .append(getLaundryBagDataArrayList.get(laundryPosition).getOrderBagId())
                    .append("/"+qtyBag)
                    .toString();
            String makeJson = webService_hit.getUrlContents(instantUrl);
            Log.d(SplashScreen.TAG, "laundryBag-" + makeJson);

            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);
           new refreshOrderedLaundryBag().execute();

        }

    }

    /*to refresh data*/
    private class getRefreshData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressBar.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();
            Log.d(SplashScreen.TAG, "RoleId---" + empInfo.getRoleId());
            String instantUrl=new StringBuilder(SplashScreen.DOMAIN+"api/ViewMakeOrder/MakeOrderDetail/?OrderId=")
                    .append(userData.getOrderId().trim())
                    .toString();
            String makeJson = webService_hit.getUrlContents(instantUrl);
            Log.d(SplashScreen.TAG, "OrderDetails-" + makeJson);
            boolean digitsOnly = TextUtils.isDigitsOnly(makeJson);
            if (digitsOnly) {
                error=1;


            }else {
                error=0;
                MakeOrderData(makeJson);
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            progressBar.setVisibility(View.GONE);
            if(error==1)
            {
                UserAlert_Dialog alert_dialog=new UserAlert_Dialog("Unable to refresh, Please try again",
                        getActivity().getResources().getString(R.string.my_task),getActivity(),R.mipmap.cross);

                alert_dialog.showDialog_newFragment();
                getActivity().finish();
            }else {


                setUserData();

            }

        }

        private void MakeOrderData(String sJson) {
            if (sJson != null) {
                orderDetailsList=new ArrayList<>();
                try {

                    MakeOrderDetails myTaskData=new MakeOrderDetails();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if(jArray.length()!=0) {
                        for (int i=0;i<jArray.length();i++) {
                            JSONObject object = jArray.getJSONObject(i);

                            myTaskData = myTaskData.MakeOrdersJsonTo(object);
                            Log.d(SplashScreen.TAG, "my product name received :---" + myTaskData.getProductName());
                            orderDetailsList.add(myTaskData);


                        }


                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }


    }

    /*to cancel order*/
    private class cancelOrderData extends AsyncTask<Void, Void, Void> {
        boolean digitsOnly;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressBar.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();
                String json = webService_hit.postUrlContents(url);
                Log.d(SplashScreen.TAG, json);
                digitsOnly = TextUtils.isDigitsOnly(json);

                if (digitsOnly) {
                    error=1;

                }else {
                    error = 0;


                }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            progressBar.setVisibility(View.GONE);
            if(error==1)
            {
                Toast.makeText(getActivity(), "Unable cancel order,Please try again ", Toast.LENGTH_SHORT).show();
            }else {

                Toast.makeText(getActivity(), "Order has been canceled", Toast.LENGTH_SHORT).show();
                StartNewActivity startNewActivity = new StartNewActivity();
                startNewActivity.showActivity(getActivity(), getActivity().getResources().getString(R.string.my_task));
                getActivity().finish();
            }

        }
    }

    /*to delete row data*/
    private class deletedRowData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressBar.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();

            String urlDeleteOrder = new StringBuilder(SplashScreen.DOMAIN + "api/OrderDetail_tbl/")
                    .append(orderDetailsList.get(position).getOrderDetailId()).toString();

            Log.d(SplashScreen.TAG,"url for order delete--"+ urlDeleteOrder);

           String orderStatus=webService_hit.deleteUrlContents(urlDeleteOrder);
            Log.d(SplashScreen.TAG, orderStatus);
            boolean digitsOnly = TextUtils.isDigitsOnly(orderStatus);
            if (digitsOnly) {
                error=1;


            }else {
                error = 0;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            progressBar.setVisibility(View.GONE);
            if(error==1)
            {
                UserAlert_Dialog alert_dialog=new UserAlert_Dialog("Unable to delete product, Please try again",
                        getActivity().getResources().getString(R.string.my_task),getActivity(),R.mipmap.cross);
                alert_dialog.showDialog_newFragment();
                getActivity().finish();
            }else {

                new getRefreshData().execute();
            }
        }
    }

     /*to delete laundry bag row data*/
    private class deletedLaundryBagData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressBar.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();
            Log.d(SplashScreen.TAG, "OrderBagId--" + getLaundryBagDataArrayList.get(laundryPosition).getOrderBagId());
            String urlDeleteOrder = new StringBuilder(SplashScreen.DOMAIN + "api/Order_LaundryBag_Tbl/")
                    .append(getLaundryBagDataArrayList.get(laundryPosition).getOrderBagId()).toString();

            Log.d(SplashScreen.TAG,"url for laundry bag delete--"+ urlDeleteOrder);

           String orderStatus = webService_hit.deleteUrlContents(urlDeleteOrder);
            Log.d(SplashScreen.TAG, orderStatus);
            if (orderStatus.equals("200")) {
                error=0;


            }else {
                error = 1;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            progressBar.setVisibility(View.GONE);
            if(error==1)
            {
                UserAlert_Dialog alert_dialog=new UserAlert_Dialog("Unable to delete product, Please try again",
                        getActivity().getResources().getString(R.string.view_order_make),getActivity(),R.mipmap.cross);
                alert_dialog.showDialog_newFragment();
            }else {

               new refreshOrderedLaundryBag().execute();
            }
        }
    }

    /*to get cancel reason*/
    private class getReason extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressBar.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();
            Log.d(SplashScreen.TAG, "RoleId---" + empInfo.getRoleId());
            String url = new StringBuilder(SplashScreen.DOMAIN+"api/CancelReasons/?RoleId=")
                    .append(empInfo.getRoleId().trim())
                    .toString();
            String json = webService_hit.getUrlContents(url);
            Log.d(SplashScreen.TAG, json);
            boolean digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;


            }else {
                error=0;
                rejectionReason(json);
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            progressBar.setVisibility(View.VISIBLE);
            if(error==1)
            {
                UserAlert_Dialog alert_dialog=new UserAlert_Dialog("Unable to get reasons, Please try again",
                        getActivity().getResources().getString(R.string.my_task),getActivity(),R.mipmap.cross);
                alert_dialog.showDialog_newFragment();
                getActivity().finish();
            }else {


                CreateAlertDialogWithRadioButtonGroup();
            }

        }

        private void rejectionReason(String sJson) {
            if (sJson != null) {

                try {

                    TaskCancelReason taskCancelReason = new TaskCancelReason();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if (jArray.length() != 0) {

                        for (int i = 0; i < jArray.length(); i++) {


                            JSONObject object = jArray.getJSONObject(i);

                            taskCancelReason = taskCancelReason.TaskRasonJsonTo(object);

                            reasonArrayList.add(taskCancelReason);
                            Log.d(SplashScreen.TAG, "cancel reason :---" + taskCancelReason.getReason());

                        }


                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }
        }


        public void CreateAlertDialogWithRadioButtonGroup() {


            CharSequence[] values = new CharSequence[reasonArrayList.size()];
            for (int i = 0; i < reasonArrayList.size(); i++) {
                values[i] = reasonArrayList.get(i).getReason();

                Log.d(SplashScreen.TAG, "reason----" + reasonArrayList.get(i).getReason());
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setTitle("Select Your Reason");

            builder.setSingleChoiceItems(values, -1, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int item) {


                    url = new StringBuilder(SplashScreen.DOMAIN+"api/OrderCancels/InsertCancelOrder/?EmployeeID=")
                            .append(empInfo.getEmp_id().trim())
                            .append("&CancelReasonId=")
                            .append(reasonArrayList.get(item).getId().trim())
                            .append("&OrderId=")
                            .append(userData.getOrderId().trim())
                            .append("&RoleTypeId=")
                            .append(empInfo.getRoleId().trim())
                            .toString();
                    new cancelOrderData().execute();
                    alertDialog1.dismiss();
                }
            });
            alertDialog1 = builder.create();
            alertDialog1.show();

        }


    }

}
