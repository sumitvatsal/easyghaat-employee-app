package com.zeusadssolutions.easyghaat_empapp.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.adapter.ClothOption_Adapter;
import com.zeusadssolutions.easyghaat_empapp.adapter.HouseHoldAdapter;
import com.zeusadssolutions.easyghaat_empapp.model.ProductData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.zeusadssolutions.easyghaat_empapp.fragment.EditOrder.branchId;
import static com.zeusadssolutions.easyghaat_empapp.fragment.EditOrder.categoryId;
import static com.zeusadssolutions.easyghaat_empapp.fragment.EditOrder.genderId;
import static com.zeusadssolutions.easyghaat_empapp.fragment.EditOrder.serviceId;

public class Household extends Fragment  {

    RecyclerView houseHoldRecycler;
    HouseHoldAdapter adapter;

    private ArrayList<ProductData> dataSelected;
    public Household() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.household_cat, container, false);

        houseHoldRecycler=(RecyclerView)view.findViewById(R.id.recycler_household);
        Log.d(SplashScreen.TAG,"service Id-"+serviceId);
        Log.d(SplashScreen.TAG,"gender Id-"+genderId);
        Log.d(SplashScreen.TAG,"branch Id-"+branchId);

        if(serviceId!=0&&branchId!=null) {
            new getClothData().execute();
        }else{
            Toast.makeText(getActivity(),"Please select Category and Service",Toast.LENGTH_SHORT).show();
        }
        return view;

    }
    private class getClothData extends AsyncTask<Void, Void, Void> {
        String url=null;
        private int error=0;


        @Override
        protected Void doInBackground(Void... params) {


            url = new StringBuilder("http://easyghaat.cashbachat.com/api/Product_Price/ByCat_ServiceId/")
                    .append("4")
                    .append("/")
                    .append(serviceId)
                    .append("/")
                    .append(branchId)
                    .toString();



            WebService_Hit webService_hit = new WebService_Hit();
            String json = webService_hit.getUrlContents(url);
            if (webService_hit.responseCode!=200) {
                error=1;
            }else {
                error=0;
                productInfoData(json);
            }

            Log.d(SplashScreen.TAG, json);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(error==1)
            {
                Toast.makeText(getActivity(), "Unable to get data,Please try again ", Toast.LENGTH_SHORT).show();
            }else {

                adapter = new HouseHoldAdapter(getActivity(),dataSelected);

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

                houseHoldRecycler.setLayoutManager(mLayoutManager);
                houseHoldRecycler.setItemAnimator(new DefaultItemAnimator());
                houseHoldRecycler.setAdapter(adapter);
            }
        }



        private void productInfoData(String sJson) {
            if (sJson != null) {

                try {
                    dataSelected=new ArrayList<>();
                    ProductData data = new ProductData();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if (jArray.length() != 0) {
                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject object = jArray.getJSONObject(i);
                            data = data.ProductDataJsonTo(object);
                            dataSelected.add(data);
                        }
                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }

    }
}
