package com.zeusadssolutions.easyghaat_empapp.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.adapter.MyTask_Adapter;
import com.zeusadssolutions.easyghaat_empapp.model.EmpInfo;
import com.zeusadssolutions.easyghaat_empapp.model.MyTaskData;
import com.zeusadssolutions.easyghaat_empapp.utils.UserAlert_Dialog;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyTask extends Fragment  {
    private Button btnCancelTask,btnStartJob;
    public  static List<MyTaskData> taskList;
    private RecyclerView taskList_recycler;
    private PrefManager prefManager;
    private MyTask_Adapter adapter;
    private SwipeRefreshLayout swipeContainer;

    public MyTask() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes

        View view = inflater.inflate(R.layout.fragment_my_task, container, false);
        btnCancelTask=(Button)view.findViewById(R.id.btnCancel_myTask);
        btnStartJob=(Button)view.findViewById(R.id.btnStart_job_myTask);
        prefManager=new PrefManager(getActivity());
        taskList_recycler = (RecyclerView) view.findViewById(R.id.taskList_recycler);
        // Lookup the swipe container view
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer_myTask);

        taskList_recycler.setHasFixedSize(true);

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeContainer.setRefreshing(true);
                new sendData().execute();
            }
        });
        //Make call to AsyncTask
        new sendData().execute();
        return view;
    }

    private class sendData extends AsyncTask<Void, Void, Void> implements View.OnClickListener{
        String url=null;
        private int error=0;
        @Override
        protected Void doInBackground(Void... params) {

            EmpInfo empInfo=prefManager.EmpInfo_getObject("EmpData");
            url = new StringBuilder(SplashScreen.DOMAIN+"api/GetMyTask/GetMyTaskById/?EmpId=")
                    .append(empInfo.getEmp_id().trim())
                    .toString();

            WebService_Hit webService_hit = new WebService_Hit();
            String json = webService_hit.getUrlContents(url);

            boolean digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;


            }else {
                error=0;
                MyTaskData(json);
            }

            Log.d(SplashScreen.TAG, json);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if(error==1)
            {
                Toast.makeText(getActivity(), "Unable to get task list,Please try again ", Toast.LENGTH_SHORT).show();
            }else {

                adapter = new MyTask_Adapter(taskList, getActivity());
                taskList_recycler.setAdapter(adapter);

                LinearLayoutManager llm = new LinearLayoutManager(getContext());
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                taskList_recycler.setLayoutManager(llm);

                btnCancelTask.setOnClickListener(this);
                btnStartJob.setOnClickListener(this);
                swipeContainer.setRefreshing(false);

            }
        }



        private void MyTaskData(String sJson) {
            if (sJson != null) {

                try {
                    taskList=new ArrayList<>();
                    MyTaskData myTaskData = new MyTaskData();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if (jArray.length() != 0) {
                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject object = jArray.getJSONObject(i);
                            myTaskData = myTaskData.TaskDetailsJsonTo(object);
                            taskList.add(myTaskData);
                        }
                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }


        @Override
        public void onClick(View v) {

            switch (v.getId()) {


                case R.id.btnCancel_myTask:

                    getActivity().finish();
                    getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    break;

                case R.id.btnStart_job_myTask:
                    UserAlert_Dialog dialog = new UserAlert_Dialog("You have started you job",
                            getActivity().getResources().getString(R.string.my_task), getActivity(),R.mipmap.tick);
                    dialog.showDialog_newFragment();
                    getActivity().finish();
                    break;
            }
        }

    }

}
