package com.zeusadssolutions.easyghaat_empapp.utils;

import android.app.Application;

import com.zeusadssolutions.easyghaat_empapp.service.ConnectivityReceiver;

/**
 * Created by HP on 20-12-2016.
 */
public class MyApplication extends Application {

    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }
}
