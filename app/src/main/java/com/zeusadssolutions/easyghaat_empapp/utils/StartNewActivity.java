package com.zeusadssolutions.easyghaat_empapp.utils;

import android.app.Activity;
import android.content.Intent;


import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.activity.MainActivity;


/**
 * Created by HP on 07-10-2016.
 */
public  class StartNewActivity {


    public StartNewActivity() {

    }

    public  void showActivity(Activity activity, String fragmentName)
    {

        Intent intent = new Intent(activity, MainActivity.class);
        intent.putExtra("PageName", fragmentName);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
       activity.startActivity(intent);
       activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        //activity.finish();

    }
}
