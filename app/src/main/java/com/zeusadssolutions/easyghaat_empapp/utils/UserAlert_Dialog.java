package com.zeusadssolutions.easyghaat_empapp.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Button;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.activity.MainActivity;

/**
 * Created by HP on 04-10-2016.
 */
public class UserAlert_Dialog {

        String message,fragmentName;
      Activity context;
    int image;

    public UserAlert_Dialog(String message, String fragmentName, Activity context,int image) {
        this.message = message;
        this.fragmentName = fragmentName;
        this.context = context;
        this.image=image;
    }

    public UserAlert_Dialog(String message, Activity context,int image) {
        this.message = message;
        this.context = context;
        this.image=image;
    }


    public void showDialog_two()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("EasyGhaat");
        builder.setMessage(message)
                .setCancelable(false)
                .setIcon(image)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        context.finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
        Button bg=alert.getButton(DialogInterface.BUTTON_POSITIVE);
        bg.setTextColor(context.getResources().getColor(R.color.toolBar_color2));
        Button bg_negative=alert.getButton(DialogInterface.BUTTON_POSITIVE);
        bg_negative.setTextColor(context.getResources().getColor(R.color.toolBar_color2));

    }
  public void showDialog_newFragment()
    {
        AlertDialog alertDialog = new AlertDialog.Builder(
                context ).create();

        // Setting Dialog Title
        alertDialog.setTitle("EasyGhaat");

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Icon to Dialog
        alertDialog.setIcon(image);


        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtra("PageName", fragmentName);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                context.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                context.finish();
            }
        });

        // Showing Alert Message
        alertDialog.show();
        Button bg=alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        bg.setTextColor(context.getResources().getColor(R.color.toolBar_color2));
    }
    public void showDialog_finish()
    {
        AlertDialog alertDialog = new AlertDialog.Builder(
                context ).create();

        // Setting Dialog Title
        alertDialog.setTitle("EasyGhaat");

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Icon to Dialog
        alertDialog.setIcon(image);


        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
              dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
        Button bg=alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);

        bg.setTextColor(context.getResources().getColor(R.color.toolBar_color2));
    }

}
