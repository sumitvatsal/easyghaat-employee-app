package com.zeusadssolutions.easyghaat_empapp.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.test.ActivityUnitTestCase;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by HP on 09-12-2016.
 */
public class LoadImageTask extends AsyncTask<String, Void, Bitmap> {



    ImageView bmImage;
    Activity context;
    public LoadImageTask( ImageView bmImage,Activity context) {
        this.context=context;
        this.bmImage = bmImage;
    }
    private ProgressDialog dialog;

    @Override
    protected Bitmap doInBackground(String... args) {
        String url=args[0];
        Log.d(SplashScreen.TAG,"image url--"+url);

        Bitmap bitmap=null;
        try {

            InputStream in = new java.net.URL(url).openStream();
            bitmap = BitmapFactory.decodeStream(in);

        } catch (IOException e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        // Dismiss the progress dialog

        if (bitmap != null) {

            bmImage.setImageBitmap(bitmap);

        }else{
            Toast.makeText(context,"Unable to download image",Toast.LENGTH_SHORT).show();
        }

    }
}
