package com.zeusadssolutions.easyghaat_empapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.model.EmpInfo;
import com.zeusadssolutions.easyghaat_empapp.model.InstantOrderDetails;
import com.zeusadssolutions.easyghaat_empapp.model.MakeOrderDetails;
import com.zeusadssolutions.easyghaat_empapp.model.ManagerData;
import com.zeusadssolutions.easyghaat_empapp.model.MyTaskData;
import com.zeusadssolutions.easyghaat_empapp.model.TaskData;
import com.zeusadssolutions.easyghaat_empapp.model.TaskDetails_UserData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by HP on 26-10-2016.
 */
public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "EasyGhaat_Emp";

    private static final String ISAVAILABLE= "IsFirstTimeLaunch";
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    public void setLogin(boolean isLoggedIn) {

        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);

        // commit changes
        editor.commit();

        Log.d(SplashScreen.TAG, "User login session modified!");
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }
    public void setAvailable(boolean isAvailable) {

        editor.putBoolean(ISAVAILABLE, isAvailable);

        // commit changes
        editor.commit();

        Log.d(SplashScreen.TAG, "User Available session modified!");
    }

    public boolean isAvailable(){
        return pref.getBoolean(ISAVAILABLE, false);
    }
      /*storing empData*/

    public void EmpInfo_putObject(String key, EmpInfo object) {

//        Log.d(SplashScreen.TAG,"Saving Values in Shared prefrence:"+key);
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        Gson gson = new Gson();
        String json = gson.toJson(object);
        editor.putString(key, json);
        editor.commit();
    }

    public EmpInfo EmpInfo_getObject(String key) {
//        Log.d(SplashScreen.TAG, "getting Values in Shared prefrence:" + key);
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        Gson gson = new Gson();
        String jsonFavorites = pref.getString(key, null);
        EmpInfo customerInfo = gson.fromJson(jsonFavorites,
                EmpInfo.class);

        return customerInfo;
    }

      /*storing empData*/

    public void ManagerData_putObject(String key, ManagerData object) {
//        Log.d(SplashScreen.TAG,"Saving Values in Shared prefrence:"+key);
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        Gson gson = new Gson();
        String json = gson.toJson(object);
        editor.putString(key, json);
        editor.commit();
    }

    public ManagerData ManagerData_getObject(String key) {
//        Log.d(SplashScreen.TAG, "getting Values in Shared prefrence:" + key);
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        Gson gson = new Gson();
        String jsonFavorites = pref.getString(key, null);
        ManagerData customerInfo = gson.fromJson(jsonFavorites,
                ManagerData.class);

        return customerInfo;
    }


    /*Storing string value*/
    public String getStringPreference( String key) {
        String value = null;

        Log.d(SplashScreen.TAG, "getting Preference---"+key + "\t" + value);
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        if (pref != null) {
            value = pref.getString(key, null);
        }
        return value;
    }

    public  void setStringPreference(String key, String value) {
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        if (pref != null && !TextUtils.isEmpty(key)) {
            editor.putString(key, value);
             editor.commit();
        }
        Log.d(SplashScreen.TAG, key + "\t" + value);
    }


}