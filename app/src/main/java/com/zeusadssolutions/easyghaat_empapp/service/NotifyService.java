package com.zeusadssolutions.easyghaat_empapp.service;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.activity.OrderDetail;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.model.EmpInfo;
import com.zeusadssolutions.easyghaat_empapp.model.TaskData;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by HP on 30-11-2016.
 */
public class NotifyService extends Service {
    final static String ACTION = "NotifyServiceAction";
    final static String STOP_SERVICE = "";
    final static int RQS_STOP_SERVICE = 1;
    NotifyServiceReceiver notifyServiceReceiver;
    TaskData taskData=null;
    public static ArrayList<TaskData> arrayList;
    Boolean isNotifyActtive=false;
    Intent myIntent;
    Timer mTimer=new Timer();
    private int error=0;
    PrefManager prefManager;
    private static final int MY_NOTIFICATION_ID=1;
    public class NotifyServiceReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            // TODO Auto-generated method stub
            Log.d(SplashScreen.TAG,"Service Stop....");
            /*int rqs = arg1.getIntExtra("RQS", 0);
            if (rqs == RQS_STOP_SERVICE){
                stopSelf();
            }*/
            arg0.startService(new Intent(arg0,NotifyService.class));
        }


    }


    @Override
    public void onCreate() {
// TODO Auto-generated method stub
        notifyServiceReceiver = new NotifyServiceReceiver();
        super.onCreate();
        mTimer=new Timer();
        mTimer.schedule(timerTask,50000,50*1000);
        prefManager=new PrefManager(getApplicationContext());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
// TODO Auto-generated method stub

        try{

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
// TODO Auto-generated method stub

        try {

            mTimer.cancel();
            timerTask.cancel();

        } catch (Exception e)
        {
            e.printStackTrace();
        }
//      this.unregisterReceiver(notifyServiceReceiver);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent arg0) {
// TODO Auto-generated method stub
        return null;
    }


    TimerTask timerTask=new TimerTask() {
        @Override
        public void run() {

            syncData();
            Log.d(SplashScreen.TAG, "Running......");
            //  notifiy();
            //  prefManager.saveSelectedArrayList(getApplicationContext(),"TaskList",arrayList);

            if (error == 1) {
                Log.d(SplashScreen.TAG,"No task assign");
            } else {
                if (arrayList.size() != 0) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        taskData = new TaskData();
                        taskData = arrayList.get(i);

                        notifiy();
                        Log.d(SplashScreen.TAG, "Task Id :---" + taskData.getTASKID());
                    }
                }
            }
        }
    };


    private synchronized void syncData() {
        // call your rest service here

        WebService_Hit webService_hit=new WebService_Hit();
        EmpInfo empInfo=prefManager.EmpInfo_getObject("EmpData");
        String notificationUrl=SplashScreen.DOMAIN+"api/EmployeeNotifications/EmpNotification/?EmpId="+empInfo.getEmp_id().trim();
        String  json=webService_hit.getUrlContents(notificationUrl);

        boolean digitsOnly = TextUtils.isDigitsOnly(json);
        if (digitsOnly) {
            error=1;


        }else {
            error=0;
            TaskInfo(json);
        }



    }



    private void TaskInfo(String sJson) {
        if (sJson != null) {

            try {

                // Getting JSON Array node
                arrayList=new ArrayList<TaskData>();
                JSONArray jArray = new JSONArray(sJson);
                if(jArray.length()!=0) {
                    for(int i=0;i<jArray.length();i++) {
                        JSONObject object = jArray.getJSONObject(i);
                        taskData=new TaskData();
                        taskData = taskData.TaskDetailsJsonTo(object);
                        arrayList.add(taskData);
                    }


                }
            } catch (final JSONException e) {
                Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());

                //Toast.makeText(getApplicationContext(), "You don't have any new task", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void notifiy()
    {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION);
       // registerReceiver(notifyServiceReceiver, intentFilter);

        Context context=getApplicationContext();



        TaskStackBuilder stackBuilder= TaskStackBuilder.create(context);
        stackBuilder.addParentStack(OrderDetail.class);
        stackBuilder.addNextIntent(myIntent);
        myIntent=new Intent(getApplicationContext(),OrderDetail.class);
        myIntent.putExtra("taskId",taskData.getTASKID() );

        PendingIntent pendingIntent=PendingIntent.getActivity(getBaseContext(), Integer.parseInt(taskData.getTASKID()), myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder;

        builder=new NotificationCompat.Builder(context).setContentTitle("EasyGhaat")
                                .setContentText(taskData.getType()+"\t\t"+taskData.getCUSTNAME()+"\t\t"+taskData.getPIKUPFROMTIME()+"-"+taskData.getPICKUPTOTIME()+"\t\t"+taskData.getPICKUPDATE().substring(0, 10))
        .setDefaults(Notification.DEFAULT_SOUND)
                                .setContentIntent(pendingIntent)
                                .setTicker("EasyGhaat")
            .setAutoCancel(true)
            .setSmallIcon(R.mipmap.header_logo);


        Notification notification=builder.build();
        NotificationManager notificationManager=(NotificationManager)getSystemService(context.NOTIFICATION_SERVICE);
        notificationManager.notify(Integer.parseInt(taskData.getTASKID()),notification);

        isNotifyActtive=true;



    }
}
