package com.zeusadssolutions.easyghaat_empapp.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.model.EmpInfo;
import com.zeusadssolutions.easyghaat_empapp.model.TaskCancelReason;
import com.zeusadssolutions.easyghaat_empapp.model.TaskData;
import com.zeusadssolutions.easyghaat_empapp.service.NotifyService;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;
import com.zeusadssolutions.easyghaat_empapp.utils.StartNewActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OrderDetail extends AppCompatActivity implements View.OnClickListener{


    TextView txt_task_id_order,txt_order_id_order,txt_assignment_date_order,txt_task_type_order,
            txt_customer_id_order,txt_customer_name_order,txt_customer_area_order,txt_vendor_id_order,
            txt_vendor_name_order,txt_vendor_area_order,txt_pickup_date_order,txt_time_slot_order;
    ArrayList<TaskData> taskDatas;
    Button btnAccept,btnReject;
    EmpInfo empInfo;
    String url=null;int position=0,accept=0;
    ArrayList<TaskCancelReason> reasonArrayList;

    AlertDialog alertDialog1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        txt_vendor_area_order=(TextView)findViewById(R.id.txt_vendor_area_order);
        txt_vendor_id_order=(TextView)findViewById(R.id.txt_vendor_id_order);
        txt_vendor_name_order=(TextView)findViewById(R.id.txt_vendor_name_order);
        txt_customer_area_order=(TextView)findViewById(R.id.txt_customer_area_order);
        txt_customer_name_order=(TextView)findViewById(R.id.txt_customer_name_order);
        txt_pickup_date_order=(TextView)findViewById(R.id.txt_pickup_date_order);
        txt_time_slot_order=(TextView)findViewById(R.id.txt_time_slot_order);
        txt_customer_id_order=(TextView) findViewById(R.id.txt_customer_id_order);
        txt_task_type_order=(TextView)findViewById(R.id.txt_task_type_order);
        txt_assignment_date_order=(TextView)findViewById(R.id.txt_assignment_date_order);
        txt_order_id_order=(TextView)findViewById(R.id.txt_order_id_order);
        txt_task_id_order=(TextView)findViewById(R.id.txt_task_id_order);

        btnAccept=(Button)findViewById(R.id.received_accept_button);
        btnReject=(Button)findViewById(R.id.received_reject_button);

        Intent intent=getIntent();
        String taskId=intent.getStringExtra("taskId");

        PrefManager prefManager=new PrefManager(getApplicationContext());

        taskDatas=new ArrayList<>();

        taskDatas= NotifyService.arrayList;

//        Log.d(SplashScreen.TAG,"Task name---"+taskDatas.get(Integer.valueOf(taskId)).getType());

        Log.d(SplashScreen.TAG,"Task id received---"+taskId);

        for(int i=0;i<taskDatas.size();i++)
        {
            if(taskDatas.get(i).getTASKID().equals(taskId))
            {
                position=i;
                break;
            }

        }

        Log.d(SplashScreen.TAG,"Position---"+String.valueOf(position));

        txt_task_id_order.setText(taskDatas.get(position).getTASKID());
        txt_order_id_order.setText(taskDatas.get(position).getORDID());
        txt_assignment_date_order.setText(taskDatas.get(position).getASGNMNTDT().substring(0, 10));
        txt_task_type_order.setText(taskDatas.get(position).getType());
        txt_pickup_date_order.setText(taskDatas.get(position).getPICKUPDATE().substring(0, 10));
        txt_customer_id_order.setText(taskDatas.get(position).getCUSTOMERID());
        txt_customer_name_order.setText(taskDatas.get(position).getCUSTNAME());
        txt_customer_area_order.setText(taskDatas.get(position).getPICKUPAREA());
        txt_vendor_id_order.setText(taskDatas.get(position).getVNDRID());
        txt_vendor_name_order.setText(taskDatas.get(position).getVendorName());
        txt_vendor_area_order.setText(taskDatas.get(position).getDELAREA());
        txt_time_slot_order.setText(taskDatas.get(position).getPIKUPFROMTIME()+"-"+taskDatas.get(position).getPICKUPTOTIME());

        btnReject.setOnClickListener(this);
        btnAccept.setOnClickListener(this);

        empInfo=prefManager.EmpInfo_getObject("EmpData");


    }

    @Override
    public void onBackPressed() {

        StartNewActivity startNewActivity=new StartNewActivity();
        String fragmentName=getResources().getString(R.string.home);
        startNewActivity.showActivity(OrderDetail.this,fragmentName);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.received_accept_button:
                accept=1;
                url = new StringBuilder(SplashScreen.DOMAIN+"api/EmployeeReaction_tbl/PostEmployeeReaction_tblAccpeted/?TaskId=")
                        .append(taskDatas.get(position).getTASKID().trim())
                        .append("&EmployeeId=")
                        .append(empInfo.getEmp_id().trim())
                        .toString();
                new sendData().execute();


                break;

            case R.id.received_reject_button:
                accept=0;
                reasonArrayList=new ArrayList<>();
                new getReason().execute();
                break;
        }
    }


    private class getReason extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();
            String url = SplashScreen.DOMAIN+"api/EmployeeRejactionReasons";
            String json = webService_hit.getUrlContents(url);
            Log.d(SplashScreen.TAG, json);

            rejectionReason(json);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            CreateAlertDialogWithRadioButtonGroup();

        }

        private void rejectionReason(String sJson) {
            if (sJson != null) {

                try {

                    TaskCancelReason taskCancelReason = new TaskCancelReason();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if (jArray.length() != 0) {

                        for (int i = 0; i < jArray.length(); i++) {


                            JSONObject object = jArray.getJSONObject(i);

                            taskCancelReason = taskCancelReason.TaskRasonJsonTo(object);

                            reasonArrayList.add(taskCancelReason);

                        }
                        Log.d(SplashScreen.TAG, "cancel reason :---" + taskCancelReason.getReason());

                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }
        }


        public void CreateAlertDialogWithRadioButtonGroup() {

            CharSequence[] values = new CharSequence[reasonArrayList.size()];
            for (int i = 0; i < reasonArrayList.size(); i++) {
                values[i] = reasonArrayList.get(i).getReason();

                Log.d(SplashScreen.TAG, "reason----" + reasonArrayList.get(i).getReason());
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(OrderDetail.this);

            builder.setTitle("Select Your Reason");

            builder.setSingleChoiceItems(values, -1, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int item) {
                    url = new StringBuilder(SplashScreen.DOMAIN+"api/EmployeeReaction_tbl/PostEmployeeReaction_tblRejected/?TaskId=")
                            .append(taskDatas.get(position).getTASKID().trim())
                            .append("&RejectionReasonId=")
                            .append(reasonArrayList.get(item).getId().trim())
                            .append("&EmployeeId=")
                            .append(empInfo.getEmp_id().trim())
                            .toString();
                    new sendData().execute();
                    alertDialog1.dismiss();
                }
            });
            alertDialog1 = builder.create();
            alertDialog1.show();

        }
    }
    private class sendData extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();
            String json = webService_hit.postUrlContents( url);
            Log.d(SplashScreen.TAG, json);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if(accept==1)
            {
                StartNewActivity startNewActivity=new StartNewActivity();
                startNewActivity.showActivity(OrderDetail.this,getApplicationContext().getResources().getString(R.string.my_task));
                finish();

            }else {
                Toast.makeText(getApplicationContext(),"You have rejected your task",Toast.LENGTH_SHORT).show();
                StartNewActivity startNewActivity=new StartNewActivity();
                startNewActivity.showActivity(OrderDetail.this,getApplicationContext().getResources().getString(R.string.home));
                finish();
            }
        }
    }

}
