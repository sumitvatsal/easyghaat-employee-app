package com.zeusadssolutions.easyghaat_empapp.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;

import com.zeusadssolutions.easyghaat_empapp.fragment.BaseFragment;
import com.zeusadssolutions.easyghaat_empapp.fragment.CollectionHandover;
import com.zeusadssolutions.easyghaat_empapp.fragment.E_Statement;
import com.zeusadssolutions.easyghaat_empapp.fragment.EditOrder;
import com.zeusadssolutions.easyghaat_empapp.fragment.HandOver;
import com.zeusadssolutions.easyghaat_empapp.fragment.MyEarning;
import com.zeusadssolutions.easyghaat_empapp.fragment.MyTask;
import com.zeusadssolutions.easyghaat_empapp.fragment.Profile;

import com.zeusadssolutions.easyghaat_empapp.fragment.ViewOrder_vendor;
import com.zeusadssolutions.easyghaat_empapp.fragment.ViewOrder_instant;
import com.zeusadssolutions.easyghaat_empapp.fragment.ViewOrder_makeOrder;
import com.zeusadssolutions.easyghaat_empapp.model.EmpInfo;
import com.zeusadssolutions.easyghaat_empapp.service.NotifyService;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;
import com.zeusadssolutions.easyghaat_empapp.utils.StartNewActivity;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    private ActionBarDrawerToggle drawerToggle;
    private SwitchCompat switcher;
    String fragmentName = null;
    PrefManager prefManager;
    EmpInfo empInfo;
    Fragment fragment = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        String pageName = intent.getStringExtra("PageName");

        prefManager = new PrefManager(getApplicationContext());
        empInfo=prefManager.EmpInfo_getObject("EmpData");
//        Log.d(SplashScreen.TAG, pageName);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);

//        switcher=(SwitchCompat)nvDrawer.getMenu().findItem(R.id.nav_switch_Availability);
//get the menu from the navigation view
        Menu menu = nvDrawer.getMenu();

        View hView =  nvDrawer.getHeaderView(0);
        TextView nav_user = (TextView)hView.findViewById(R.id.drawer_UserName);

        nav_user.setText("Welcome "+empInfo.getEmp_name());

//get switch view
        switcher = (SwitchCompat) MenuItemCompat.getActionView(menu.findItem(R.id.nav_switch_Availability)).findViewById(R.id.drawer_switch);



        if(prefManager.isLoggedIn())
        {
            startService(new Intent(MainActivity.this, NotifyService.class));
            Log.d(SplashScreen.TAG, "Notification receive starts");

            switcher.setChecked(prefManager.isAvailable());
        }else {
            stopService(new Intent(MainActivity.this, NotifyService.class));
            Toast.makeText(MainActivity.this, "Task Notifications stop",Toast.LENGTH_SHORT).show();
            Log.d(SplashScreen.TAG, "Notification receive stops");
            switcher.setChecked(prefManager.isAvailable());
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(pageName.toUpperCase());
        getSupportActionBar().setDisplayShowTitleEnabled(true);


        drawerToggle = setupDrawerToggle();
        // Insert the fragment by replacing any existing fragment
        fragment = new BaseFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        // Setup drawer view
        setupDrawerContent(nvDrawer);
        if (pageName.equals(getResources().getString(R.string.home))) {
            fragment = new BaseFragment();
        }else if (pageName.equals(getResources().getString(R.string.my_profile))) {
            fragment = new Profile();
        }else if (pageName.equals(getResources().getString(R.string.collection_handover))) {
            pageName = "Daily Collection";
            fragment = new CollectionHandover();
        }else if (pageName.equals(getResources().getString(R.string.e_statement))) {
            fragment = new E_Statement();
        }else if (pageName.equals(getResources().getString(R.string.my_earning))) {
            fragment = new MyEarning();
        }else if (pageName.equals(getResources().getString(R.string.my_task))) {
            fragment = new MyTask();
        }else if (pageName.equals(getResources().getString(R.string.handover))) {
            fragment = new HandOver();
        }else if (pageName.equals(getResources().getString(R.string.view_order_vendor))) {
            pageName = "View Order";
            fragment = new ViewOrder_vendor();
        }else if (pageName.equals(getResources().getString(R.string.view_order_instant))) {
            pageName = "View Order";
            fragment = new ViewOrder_instant();
        }else if (pageName.equals(getResources().getString(R.string.edit_order))) {
            fragment = new EditOrder();
        }else if (pageName.equals(getResources().getString(R.string.view_order_make))) {
            pageName = "View Order";
            fragment = new ViewOrder_makeOrder();
        }

        getSupportActionBar().setTitle(pageName.toUpperCase());

        fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction()
                        .replace(R.id.flContent, fragment)
                        .commit();


    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    private void setupDrawerContent(NavigationView navigationView) {

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }


    private void selectDrawerItem(MenuItem menuItem) {

        StartNewActivity startNewActivity = new StartNewActivity();
        switch (menuItem.getItemId()) {

            case R.id.profile:
                fragmentName = getResources().getString(R.string.my_profile);
                startNewActivity.showActivity(MainActivity.this, fragmentName);
                break;
            case R.id.home:
                fragmentName = getResources().getString(R.string.home);
                startNewActivity.showActivity(MainActivity.this, fragmentName);
                break;

            case R.id.my_earning:
                fragmentName = getResources().getString(R.string.my_earning);
                startNewActivity.showActivity(MainActivity.this, fragmentName);
                break;

            case R.id.collection_handover:
                fragmentName = getResources().getString(R.string.collection_handover);
                startNewActivity.showActivity(MainActivity.this, fragmentName);
                break;

            case R.id.handover_order:
                fragmentName = getResources().getString(R.string.handover);
                startNewActivity.showActivity(MainActivity.this, fragmentName);
                break;

            case R.id.e_statement:
                fragmentName = getResources().getString(R.string.e_statement);
                startNewActivity.showActivity(MainActivity.this, fragmentName);
                break;
            case R.id.my_task:
                fragmentName = getResources().getString(R.string.my_task);
                startNewActivity.showActivity(MainActivity.this, fragmentName);
                break;

            case R.id.sign_out:
                new sendData().execute();

                break;

            default:
                fragmentName = getResources().getString(R.string.home);
                startNewActivity.showActivity(MainActivity.this, fragmentName);
        }

//
        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        setTitle(menuItem.getTitle());
        // Close the navigation drawer
        mDrawer.closeDrawers();
    }


    @Override
    protected void onResume() {
        super.onResume();
        switcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    Log.d(SplashScreen.TAG, "employee is login....");


                    new attendanceInTime().execute();
                } else {
                    prefManager.setAvailable(false);
                    Log.d(SplashScreen.TAG, "employee is offline....");
                    new attendanceOutTime().execute();
                }
            }
        });


    }

    // `onPostCreate` called when activity start-up is complete after `onStart()`
    // NOTE! Make sure to override the method with only a single `Bundle` argument
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }



    private class sendData extends AsyncTask<Void, Void, Void> {
        private ProgressDialog dialog;
        String url=null;
        int error;
        @Override
        protected Void doInBackground(Void... params) {

            String sessionId=prefManager.getStringPreference("sessionId");

            Log.d(SplashScreen.TAG,"sessionId before logout----"+sessionId);

            url = SplashScreen.DOMAIN+"api/EmployeeSessions/PutEmployeeSessionLogOut/?SessionId="
                                                                            +sessionId.trim();
            WebService_Hit webService_hit = new WebService_Hit();
            String json = webService_hit.putUrlContents( url);
            boolean digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;


            }else {
                error = 0;
            }
            Log.d(SplashScreen.TAG, json);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if(error==1)
            {
                Toast.makeText(MainActivity.this, "Unable to Logout,Please try again ", Toast.LENGTH_SHORT).show();
            }else {
                // Dismiss the progress dialog

                prefManager.setLogin(false);
                prefManager.setAvailable(false);
                Intent intent = new Intent(getApplicationContext(), SplashScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                finish();
            }

        }
    }


    private class attendanceInTime extends AsyncTask<Void, Void, Void> {
        String url=null;
        int error;
        @Override
        protected Void doInBackground(Void... params) {

            url = SplashScreen.DOMAIN+"api/EmployeeAttendence";

            JSONObject postDataParams = new JSONObject();
            try {
                postDataParams.put("EmpId", empInfo.getEmp_id().trim());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            WebService_Hit webService_hit = new WebService_Hit();
            String json = webService_hit.postWithParamUrlContents(postDataParams, url);
            boolean digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;

            }else {

                if(json.equals("300"))
                {
                    error=1;
                }else {
                    error = 0;
                }
            }
            Log.d(SplashScreen.TAG, json);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if(error==1)
            {
                prefManager.setAvailable(false);
                Toast.makeText(MainActivity.this, "Unable to Online!", Toast.LENGTH_SHORT).show();
            }else {
                // Dismiss the progress dialog
                Toast.makeText(MainActivity.this, "Yor are Online! ", Toast.LENGTH_SHORT).show();

                prefManager.setAvailable(true);
            }

        }
    }

    private class attendanceOutTime extends AsyncTask<Void, Void, Void> {
        String url=null;
        int error;
        @Override
        protected Void doInBackground(Void... params) {

            url = SplashScreen.DOMAIN+"api/EmployeeAttendence/Outtime/ByEmpId/"
                    +empInfo.getEmp_id().trim();


            WebService_Hit webService_hit = new WebService_Hit();
            String json = webService_hit.getUrlContents(url);
            boolean digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;


            }else {
                if(json.equals("300"))
                {
                    error=1;
                }else {
                    error = 0;
                }
            }
            Log.d(SplashScreen.TAG, json);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if(error==1)
            {
                prefManager.setAvailable(true);
                Toast.makeText(MainActivity.this, "You are still Online! ", Toast.LENGTH_SHORT).show();
            }else {
                // Dismiss the progress dialog

                prefManager.setAvailable(false);
                Toast.makeText(MainActivity.this, "Unable to Online!", Toast.LENGTH_SHORT).show();
            }

        }
    }
}
