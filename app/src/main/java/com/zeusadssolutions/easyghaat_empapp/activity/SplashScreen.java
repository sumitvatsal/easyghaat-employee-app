package com.zeusadssolutions.easyghaat_empapp.activity;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.service.ConnectivityReceiver;
import com.zeusadssolutions.easyghaat_empapp.utils.MyApplication;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;
import com.zeusadssolutions.easyghaat_empapp.utils.StartNewActivity;


public class SplashScreen extends AppCompatActivity implements View.OnClickListener,ConnectivityReceiver.ConnectivityReceiverListener {

    public static String TAG="EasyGhaat-EmpApp---";
    public static String DOMAIN="http://sanjay.cashbachat.com/";
    Button sign_button;
        PrefManager session;
        StartNewActivity startNewActivity=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Log.d(TAG, "Welcome to EasyGhaat Emp App");

        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);

        startNewActivity=new StartNewActivity();



        sign_button=(Button)findViewById(R.id.sign_button);
        LinearLayout linear_count=(LinearLayout)findViewById(R.id.linear_option);

        // Session manager
        session = new PrefManager(getApplicationContext());

        // Check if user is already logged in or not
        if (!session.isLoggedIn()) {
            // User is already logged in. Take him to main activity

            linear_count.setVisibility(View.VISIBLE);
            sign_button.setOnClickListener(this);

        }else{
            linear_count.setVisibility(View.GONE);
            Thread timerThread = new Thread(){
                public void run(){
                    try{
                        sleep (1500);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }finally{
                        startNewActivity.showActivity(SplashScreen.this,getResources().getString(R.string.home));
                        finish();
                    }
                }
            };
            timerThread.start();
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.sign_button:
                Intent intent=new Intent(SplashScreen.this,Signin.class);
                startActivity(intent);
                finish();
                break;


        }
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Connected to Internet";
            color = Color.WHITE;
        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }
       // Toast.makeText(SplashScreen.this,message,Toast.LENGTH_SHORT).show();

        Snackbar snackbar= Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();

    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MyApplication.getInstance().setConnectivityListener(this);
    }


    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
}
