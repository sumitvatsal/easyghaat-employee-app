package com.zeusadssolutions.easyghaat_empapp.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.maksim88.passwordedittext.PasswordEditText;
import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.model.EmpInfo;
import com.zeusadssolutions.easyghaat_empapp.model.ManagerData;
import com.zeusadssolutions.easyghaat_empapp.utils.UserAlert_Dialog;
import com.zeusadssolutions.easyghaat_empapp.utils.MSG91;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;
import com.zeusadssolutions.easyghaat_empapp.utils.StartNewActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Signin extends AppCompatActivity implements View.OnClickListener  {
    TextInputLayout usernameWrapper, passwordWrapper ;
    private EditText inputEmail;
    int login=0;
    PrefManager prefManager;
    PasswordEditText passwordEditText;
    Button signIn;
    String userLogin,userPass;
    TextView text_forgetPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        passwordEditText=(PasswordEditText)findViewById(R.id.input_password);
        inputEmail = (EditText) findViewById(R.id.edit_userid);
        usernameWrapper = (TextInputLayout) findViewById(R.id.usernameWrapper);
        passwordWrapper = (TextInputLayout) findViewById(R.id.passwordWrapper);
        text_forgetPassword=(TextView)findViewById(R.id.txt_ForgetPassword);
        signIn=(Button)findViewById(R.id.btn_signin);
        prefManager=new PrefManager(getApplicationContext());
        signIn.setOnClickListener(this);
        text_forgetPassword.setOnClickListener(this);

        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if(validateData())
                    {
                        userLogin=inputEmail.getText().toString();
                        userPass=passwordEditText.getText().toString();
                        new sendData().execute();
                    }
                }
                return false;
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.btn_signin:
                if(validateData())
                {
                    userLogin=inputEmail.getText().toString();



                    userPass=passwordEditText.getText().toString();
                    new sendData().execute();
                }
                break;
            case R.id.txt_ForgetPassword:
                EmpInfo empInfo=prefManager.EmpInfo_getObject("EmpData");
                MSG91 msg91=new MSG91(getResources().getString(R.string.Msg91AuthKey));
                String msg="Dear \t"+empInfo.getEmp_name()+"&#xA;Your Password is :- \t" + empInfo.getPassword() + "&#xA;Please don't share with Others." +
                       " &#xA;&#xA;Delivering Happiness" +
                       " &#xA;Team EasyGhaat";
                Log.d(SplashScreen.TAG,msg);
                msg91.composeMessage("ESYGHT", msg);
                msg91.to(empInfo.getPer_phoneNo());
                //msg91.to("7055620003");
                msg91.setRoute("4");
                String sendStatus = msg91.send();
                Toast.makeText(getApplicationContext(),"Password sent to your mobile number",Toast.LENGTH_SHORT).show();
                Log.d(SplashScreen.TAG,"Old password sent:-"+empInfo.getPassword()+"sendStatus:-"+sendStatus);
                break;
        }

    }

    private boolean validateData() {
        boolean result = true;

        String password = passwordEditText.getText().toString();
        if (password == null || password.equals("")) {
            passwordWrapper.setError(getString(R.string.err_msg_password));
            requestFocus(passwordEditText);
            result = false;
        }
        else
            passwordWrapper.setErrorEnabled(false);

        String userid = inputEmail.getText().toString();
        if (userid == null || userid.equals("")) {
            usernameWrapper.setError(getString(R.string.err_msg_empid));
            requestFocus(inputEmail);
            result = false;
        }
        else
            usernameWrapper.setErrorEnabled(false);



        return result;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private class sendData extends AsyncTask<Void, Void, Void> {
        private ProgressDialog dialog;
        String url=null;
        boolean digitsOnly;int error=0;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            dialog = new ProgressDialog(Signin.this);
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            userLogin=userLogin.trim();
            userPass=userPass.trim();
            digitsOnly = TextUtils.isDigitsOnly(userLogin);
            if (digitsOnly) {

                    url = new StringBuilder(SplashScreen.DOMAIN+"api/Emp_Info/GetEmployeeByPhoneNo/?PhoneNo=")
                            .append(userLogin)
                            .append("&password=")
                            .append(userPass)
                            .toString();
                   Log.d(SplashScreen.TAG, "field is int value");

            }else {
                url = new StringBuilder(SplashScreen.DOMAIN+"api/Emp_Info/GetEmployeeByEmail/?EmailId=")
                        .append(userLogin)
                        .append("&password=")
                        .append(userPass)
                        .toString();


                Log.d(SplashScreen.TAG, "field is string value");
            }



            WebService_Hit webService_hit = new WebService_Hit();
            String json = webService_hit.getUrlContents(url);

            Log.d(SplashScreen.TAG, "get data---" + json);
            digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;
            }else {
                error = 0;
                empData(json);

            EmpInfo empInfo=prefManager.EmpInfo_getObject("EmpData");

            if(empInfo.getRoleName().equals("DeliveryExcutive")) {
                login=1;
                String postUrl = SplashScreen.DOMAIN+"api/EmployeeSessions/PostEmployeeSessionLogin/?EmployeeId=" + empInfo.getEmp_id().trim() + "&LoginFrom=App";
                String postData = webService_hit.postUrlContents(postUrl);
                Log.d(SplashScreen.TAG, "post data----" + postData);
                digitsOnly = TextUtils.isDigitsOnly(postData);
                if (digitsOnly) {
                    error=1;
                }else {
                    error = 0;
                    sessionId(postData);

                    url = new StringBuilder(SplashScreen.DOMAIN + "api/ReportIngManager/GetReportIngManager/?EmpId=")
                            .append(empInfo.getEmp_id().trim())
                            .toString();

                    String mjson = webService_hit.getUrlContents(url);
                    digitsOnly = TextUtils.isDigitsOnly(mjson);
                    if (digitsOnly) {
                        error=1;
                    }else {
                        error = 0;
                        Log.d(SplashScreen.TAG, "get data---" + mjson);
                        ManagerData(mjson);
                    }
                }
            }else {
                 login=0;
                 }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (dialog.isShowing())
                dialog.dismiss();
            if(error==1)
            {
                Toast.makeText(Signin.this, "Unable to login,Please try again ", Toast.LENGTH_SHORT).show();
            }else {
                if (login == 1) {
                    StartNewActivity startNewActivity = new StartNewActivity();
                    startNewActivity.showActivity(Signin.this, getApplicationContext().getResources().getString(R.string.home));
                    finish();
                } else {
                    UserAlert_Dialog dialog = new UserAlert_Dialog("You are not authorize to login!", Signin.this,R.mipmap.cross);
                    dialog.showDialog_finish();

                }
            }

        }


        private void empData(String sJson) {
            if (sJson != null) {

                try {
                   EmpInfo empInfo = new EmpInfo();

                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if(jArray.length()!=0) {
                        prefManager.setLogin(true);

                        JSONObject object = jArray.getJSONObject(0);

                        empInfo = empInfo.UserDetailsJsonTo(object);

                        prefManager.EmpInfo_putObject("EmpData", empInfo);


                    }else{
                        login=0;
                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }

        private void ManagerData(String sJson) {
            if (sJson != null) {

                try {

                    ManagerData managerData = new ManagerData();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if (jArray.length() != 0) {
                        JSONObject object = jArray.getJSONObject(0);

                        managerData = managerData.UserDetailsJsonTo(object);
                        prefManager.ManagerData_putObject("ManagerData", managerData);
                        Log.d(SplashScreen.TAG, "manager name :---" + managerData.getEmp_name());

                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }
        }
        private void sessionId(String sJson) {
            if (sJson != null) {

                try {

                    String session=new String();
                    // Getting JSON Array node

                        JSONObject object = new JSONObject(sJson);

                        session =  object.getString(String.valueOf("Id"));
                        prefManager.setStringPreference("sessionId", session);
                        Log.d(SplashScreen.TAG,"sessionId :---"+session);


                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }


        }
    }

    @Override
    public void onBackPressed() {

        UserAlert_Dialog dialog=new UserAlert_Dialog("Are you sure you want to exit?",Signin.this,R.mipmap.img);
        dialog.showDialog_two();
    }
}
