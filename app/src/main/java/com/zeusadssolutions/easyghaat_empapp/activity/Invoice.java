package com.zeusadssolutions.easyghaat_empapp.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.adapter.MyTask_Adapter;
import com.zeusadssolutions.easyghaat_empapp.fragment.MyTask;
import com.zeusadssolutions.easyghaat_empapp.fragment.ViewOrder_instant;
import com.zeusadssolutions.easyghaat_empapp.fragment.ViewOrder_makeOrder;
import com.zeusadssolutions.easyghaat_empapp.model.EmpInfo;
import com.zeusadssolutions.easyghaat_empapp.model.InvoiceData;
import com.zeusadssolutions.easyghaat_empapp.model.TaskCancelReason;
import com.zeusadssolutions.easyghaat_empapp.model.TaskDetails_UserData;
import com.zeusadssolutions.easyghaat_empapp.utils.UserAlert_Dialog;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;
import com.zeusadssolutions.easyghaat_empapp.utils.StartNewActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

public class Invoice extends AppCompatActivity  implements View.OnClickListener{
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private SignaturePad mSignaturePad;
    private Button btnCancelOrder_invoice,btnConfirmOrder_invoice,mClearButton;
    private String url,taskId;
    private TextView textCustomerId_invoice,textCustomerName_invoice,textAmountReceived_invoice,
            textpaymentType_invoice,total_sub_amount_invoice,pick_delivery_charge_invoice,
            sur_charges_invoice,package_adjustment_invoice,taxable_amount_invoice,
            service_tax_invoice,wallet_adjustment_invoice,laundry_bag_amount_invoice,
            chashBack_invoice,netAmount_invoice,order_id_invoice,task_id_invoice;

    private LinearLayout linearCashBack,linearSubAmount,linearPickupCharge,linearSurCharge,
            linearLaundryBag,linearPackageAdjustment,linearTaxableAmount,linearServiceTax,linearWalletAdjustment;
    private PrefManager prefManager;
    private EmpInfo empInfo;
    private TaskDetails_UserData userData;
    private ArrayList<TaskCancelReason> reasonArrayList;
    private ProgressDialog dialog;
    private AlertDialog alertDialog1;
    InvoiceData invoiceInfo;
    Double packageAdjustment=0.0;

    private int error=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        setContentView(R.layout.activity_invoice);
        verifyStoragePermissions(Invoice.this);


            invoiceInfo=ViewOrder_makeOrder.invoiceData;


        prefManager = new PrefManager(Invoice.this);
        empInfo = prefManager.EmpInfo_getObject("EmpData");
        userData = MyTask_Adapter.myTaskData;
        mSignaturePad=(SignaturePad)findViewById(R.id.signature_pad);


        /*customer information*/
        textCustomerId_invoice=(TextView)findViewById(R.id.textCustomerId_invoice);
        textCustomerName_invoice=(TextView)findViewById(R.id.textCustomerName_invoice);
        textpaymentType_invoice=(TextView)findViewById(R.id.textpaymentType_invoice);
        textAmountReceived_invoice=(TextView)findViewById(R.id.textAmountReceived_invoice);
        order_id_invoice=(TextView)findViewById(R.id.order_id_invoice);
        task_id_invoice=(TextView)findViewById(R.id.task_id_invoice);

        /*OrderDetails information*/

        linearWalletAdjustment=(LinearLayout)findViewById(R.id.linearWalletAdjustment);
        linearServiceTax=(LinearLayout)findViewById(R.id.linearServiceTax);
        linearTaxableAmount=(LinearLayout)findViewById(R.id.linearTaxableAmount);
        linearPackageAdjustment=(LinearLayout)findViewById(R.id.linearPackageAdjustment);
        linearLaundryBag=(LinearLayout)findViewById(R.id.linearLaundryBag);
        linearSurCharge=(LinearLayout)findViewById(R.id.linearSurCharge);
        linearPickupCharge=(LinearLayout)findViewById(R.id.linearPickupCharge);
        linearSubAmount=(LinearLayout)findViewById(R.id.linearSubAmount);
        linearCashBack=(LinearLayout)findViewById(R.id.linearCashBack);

        total_sub_amount_invoice=(TextView)findViewById(R.id.total_sub_amount_invoice);
        pick_delivery_charge_invoice=(TextView)findViewById(R.id.pick_delivery_charge_invoice);
        sur_charges_invoice=(TextView)findViewById(R.id.sur_charges_invoice);
        package_adjustment_invoice=(TextView)findViewById(R.id.package_adjustment_invoice);
        taxable_amount_invoice=(TextView)findViewById(R.id.taxable_amount_invoice);
        service_tax_invoice=(TextView)findViewById(R.id.service_tax_invoice);
        wallet_adjustment_invoice=(TextView)findViewById(R.id.wallet_adjustment_invoice);
        laundry_bag_amount_invoice=(TextView)findViewById(R.id.laundry_bag_amount_invoice);
        chashBack_invoice=(TextView)findViewById(R.id.chashBack_invoice);
        netAmount_invoice=(TextView)findViewById(R.id.netAmount_invoice);

        btnCancelOrder_invoice = (Button) findViewById(R.id.btnCancelOrder_invoice);
        btnConfirmOrder_invoice = (Button) findViewById(R.id.btnConfirmOrder_invoice);

        mClearButton = (Button) findViewById(R.id.clear_button);

        mClearButton.setOnClickListener(this);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {

                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {

                mClearButton.setEnabled(false);
            }
        });

        taskId=MyTask.taskList.get(MyTask_Adapter.position).getTASKID();
        textCustomerId_invoice.setText(userData.getCustomerId());
        textCustomerName_invoice.setText(userData.getCustomerName());

        order_id_invoice.setText(userData.getOrderId());
        task_id_invoice.setText(taskId);
        textAmountReceived_invoice.setText("Rs. "+invoiceInfo.getTotalAmount());

       if(userData.getPaymentType()==null && Double.parseDouble(invoiceInfo.getNetBalance())>0) {
            textpaymentType_invoice.setText("(Cash on Delivery) ");

       }else{
            String paymentType;
            if(userData.getPaymentType().equals("COD"))
                paymentType="Cash on Delivery";
            else
                paymentType="Cash on Pickup";

            textpaymentType_invoice.setText("(" + paymentType + ")");
        }
        if(invoiceInfo.getCashBackAmount()!=null && Double.parseDouble(invoiceInfo.getCashBackAmount())>0) {
            chashBack_invoice.setText(invoiceInfo.getCashBackAmount());
        }else {
            linearCashBack.setVisibility(View.GONE);
        }
        if(invoiceInfo.getSubAmount()!=null && Double.parseDouble(invoiceInfo.getSubAmount())>0) {
            total_sub_amount_invoice.setText(invoiceInfo.getSubAmount());
        }else {
            linearSubAmount.setVisibility(View.GONE);
        }

        if(invoiceInfo.getPickUp_DeliveryCharges()!=null && Double.parseDouble(invoiceInfo.getPickUp_DeliveryCharges())>0) {
            pick_delivery_charge_invoice.setText(invoiceInfo.getPickUp_DeliveryCharges());

        }else {
            linearPickupCharge.setVisibility(View.GONE);
        }

        if(invoiceInfo.getSurgingAmount()!=null && Double.parseDouble(invoiceInfo.getSurgingAmount())>0) {
            sur_charges_invoice.setText(invoiceInfo.getSurgingAmount());
        }else {
            linearSurCharge.setVisibility(View.GONE);
        }

        if(invoiceInfo.getPackageAdjustmentAmt()!=null && Double.parseDouble(invoiceInfo.getPackageAdjustmentAmt())>0) {
            package_adjustment_invoice.setText(invoiceInfo.getPackageAdjustmentAmt());
           packageAdjustment = Double.parseDouble(invoiceInfo.getPackageAdjustmentAmt());
        }else {
            linearPackageAdjustment.setVisibility(View.GONE);
        }


        if(invoiceInfo.getLaundryBagAmount()!=null && Double.parseDouble(invoiceInfo.getLaundryBagAmount())>0) {
            laundry_bag_amount_invoice.setText(invoiceInfo.getLaundryBagAmount());
        }else {
            linearLaundryBag.setVisibility(View.GONE);
        }
        double totalService=Double.parseDouble(invoiceInfo.getServiceTaxAmt())+Double.parseDouble(invoiceInfo.getSwachhBharatCess())+
                Double.parseDouble(invoiceInfo.getKrishiKalyanCessAmt());

        if(totalService!=0.0) {

            service_tax_invoice.setText(String.valueOf(totalService));
        }else {
            linearServiceTax.setVisibility(View.GONE);
        }
        if(invoiceInfo.getWalletAdjustment()!=null&& Double.parseDouble(invoiceInfo.getWalletAdjustment())>0) {
            wallet_adjustment_invoice.setText(invoiceInfo.getWalletAdjustment());
        }else {
            linearWalletAdjustment.setVisibility(View.GONE);
        }
        if(invoiceInfo.getNetBalance()!=null&& Double.parseDouble(invoiceInfo.getNetBalance())>0) {
            netAmount_invoice.setText(invoiceInfo.getNetBalance());
        }else {
            netAmount_invoice.setVisibility(View.GONE);
        }


        double totalAmount=Double.parseDouble(invoiceInfo.getSubAmount())+Double.parseDouble(invoiceInfo.getPickUp_DeliveryCharges())+
                Double.parseDouble(invoiceInfo.getSurgingAmount())+ Double.parseDouble(invoiceInfo.getLaundryBagAmount());

        double totalDiscount=Double.parseDouble(invoiceInfo.getPackageAdjustmentAmt())+ Double.parseDouble(invoiceInfo.getCashBackAmount());

        double taxableAmount=totalAmount-totalDiscount;
        if(taxableAmount!=0.0) {

            taxable_amount_invoice.setText(String.valueOf(taxableAmount));
        }else {
            linearTaxableAmount.setVisibility(View.GONE);
       }
        btnCancelOrder_invoice.setOnClickListener(this);
        btnConfirmOrder_invoice.setOnClickListener(this);
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        Log.d(SplashScreen.TAG, "Focus changed !");

        if(!hasFocus) {
            Log.d(SplashScreen.TAG, "Lost focus !");

            windowCloseHandler.postDelayed(windowCloserRunnable, 0);
        }
    }

    private void toggleRecents() {
        Intent closeRecents = new Intent("com.android.systemui.recent.action.TOGGLE_RECENTS");
        closeRecents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        ComponentName recents = new ComponentName("com.android.systemui", "com.android.systemui.recent.RecentsActivity");
        closeRecents.setComponent(recents);
        this.startActivity(closeRecents);
    }

    private Handler windowCloseHandler = new Handler();
    private Runnable windowCloserRunnable = new Runnable() {
        @Override
        public void run() {
            ActivityManager am = (ActivityManager)getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
            ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

            if (cn != null && cn.getClassName().equals("com.android.systemui.recent.RecentsActivity")) {
                toggleRecents();
            }
        }
    };
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.clear_button:
                mSignaturePad.clear();
                break;

            case R.id.btnConfirmOrder_invoice:


                Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                if (addJpgSignatureToGallery(signatureBitmap)) {
                    new confirmOrderData().execute();
                } else {
                    Toast.makeText(getApplicationContext(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.btnCancelOrder_invoice:
                reasonArrayList=new ArrayList<>();
                new getReason().execute();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Cannot write images to external storage", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    public void saveBitmapToJpg(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 1  , stream);
        stream.close();
    }
    public boolean addJpgSignatureToGallery(Bitmap signature) {
        boolean result = false;
        try {
            File photo = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.png", System.currentTimeMillis()));
            saveBitmapToJpg(signature, photo);
            scanMediaFile(photo);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }




    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    /*to cancel order*/
    private class cancelOrderData extends AsyncTask<Void, Void, Void> {
        boolean digitsOnly;

        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();

            if (packageAdjustment > 0.0) {
                String urlPackageBalance = new StringBuilder(SplashScreen.DOMAIN + "api/EmployeeIncentive/UpdateMyPackge/")
                        .append(userData.getOrderId().trim()).toString();

                String packageBalance = webService_hit.getUrlContents(urlPackageBalance);
                digitsOnly = TextUtils.isDigitsOnly(packageBalance);
                if (digitsOnly) {
                    error = 1;

                } else {
                    error = 0;
                }
            }


                String urlDeleteInvoice = new StringBuilder(SplashScreen.DOMAIN + "api/EmployeeIncentive/DeleteInvoice/ByOrderId/")
                        .append(userData.getOrderId().trim()).toString();

                String deleteInvoice = webService_hit.getUrlContents(urlDeleteInvoice);
                digitsOnly = TextUtils.isDigitsOnly(deleteInvoice);
                if (digitsOnly) {
                    error = 1;

                } else {
                    error = 0;
                    String json = webService_hit.postUrlContents(url);
                    Log.d(SplashScreen.TAG, json);


                    digitsOnly = TextUtils.isDigitsOnly(json);
                    if (digitsOnly) {
                        error = 1;


                    } else {
                        error = 0;


                    }
                }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (error == 1) {
                UserAlert_Dialog userAlert_dialog = new UserAlert_Dialog("Unable connect to server,please try again", Invoice.this,R.mipmap.cross);
                userAlert_dialog.showDialog_finish();
            } else {
                Toast.makeText(getApplicationContext(), "Order has been canceled", Toast.LENGTH_SHORT).show();
                StartNewActivity startNewActivity = new StartNewActivity();
                startNewActivity.showActivity(Invoice.this, getResources().getString(R.string.my_task));
                finish();
            }

        }
    }
    /*to get cancel reason*/
    private class getReason extends AsyncTask<Void, Void, Void>
    {


        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();
            Log.d(SplashScreen.TAG, "RoleId---" + empInfo.getRoleId());
            String url = new StringBuilder(SplashScreen.DOMAIN+"api/CancelReasons/?RoleId=")
                    .append(empInfo.getRoleId().trim())
                    .toString();
            String json = webService_hit.getUrlContents(url);
            Log.d(SplashScreen.TAG, json);

            boolean digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;

            }else {
                error=0;
                rejectionReason(json);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (error == 1) {
                UserAlert_Dialog userAlert_dialog = new UserAlert_Dialog("Unable connect to server,please try again", Invoice.this,R.mipmap.cross);
                userAlert_dialog.showDialog_finish();
            } else {
                CreateAlertDialogWithRadioButtonGroup();
            }
        }

        private void rejectionReason(String sJson) {
            if (sJson != null) {

                try {

                    TaskCancelReason taskCancelReason = new TaskCancelReason();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if (jArray.length() != 0) {

                        for (int i = 0; i < jArray.length(); i++) {


                            JSONObject object = jArray.getJSONObject(i);

                            taskCancelReason = taskCancelReason.TaskRasonJsonTo(object);

                            reasonArrayList.add(taskCancelReason);
                            Log.d(SplashScreen.TAG, "cancel reason :---" + taskCancelReason.getReason());

                        }


                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }
        }


        public void CreateAlertDialogWithRadioButtonGroup() {


            CharSequence[] values = new CharSequence[reasonArrayList.size()];
            for (int i = 0; i < reasonArrayList.size(); i++) {
                values[i] = reasonArrayList.get(i).getReason();

                Log.d(SplashScreen.TAG, "reason----" + reasonArrayList.get(i).getReason());
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(Invoice.this);

            builder.setTitle("Select Your Reason");

            builder.setSingleChoiceItems(values, -1, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int item) {
                    url = new StringBuilder(SplashScreen.DOMAIN + "api/OrderCancels/InsertCancelOrder/?EmployeeID=")
                            .append(empInfo.getEmp_id().trim())
                            .append("&CancelReasonId=")
                            .append(reasonArrayList.get(item).getId().trim())
                            .append("&OrderId=")
                            .append(userData.getOrderId().trim())
                            .append("&RoleTypeId=")
                            .append(empInfo.getRoleId().trim())
                            .toString();
                    new cancelOrderData().execute();
                    alertDialog1.dismiss();
                }
            });
            alertDialog1 = builder.create();
            alertDialog1.show();

        }


    }

    /*to confirm order*/
    private class confirmOrderData extends AsyncTask<Void, Void, Void> {
          @Override
               protected void onPreExecute() {
                   super.onPreExecute();
                   // Showing progress dialog
                   dialog = new ProgressDialog(Invoice.this);
                   dialog.setMessage("Updating...");
                   dialog.setCancelable(false);
                   dialog.show();
               }
        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();

            String urlOrderStatus=new StringBuilder(SplashScreen.DOMAIN+"api/OrderDetail_tbl/Confirm_Order/")
                        .append(taskId.trim()).toString();

            String orderStatus=webService_hit.getUrlContents(urlOrderStatus);
            Log.d(SplashScreen.TAG, orderStatus);

            boolean digitsOnly = TextUtils.isDigitsOnly(orderStatus);
            if (digitsOnly) {
                error=1;

            }else {
                error=0;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            super.onPostExecute(result);

            if(error==1)
            {
               UserAlert_Dialog userAlert_dialog=new UserAlert_Dialog("Order is not confirm",Invoice.this,R.mipmap.cross);
                userAlert_dialog.showDialog_finish();
            }else {

                new empIncentive().execute();
            }

        }
    }
    /*to emp incentive  reason*/
    private class empIncentive extends AsyncTask<Void, Void, Void>
    {


        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();
            Log.d(SplashScreen.TAG, "RoleId---" + empInfo.getRoleId());
            String url = new StringBuilder(SplashScreen.DOMAIN+"api/CancelReasons/?RoleId=")
                    .append(empInfo.getRoleId().trim())
                    .toString();
            String json = webService_hit.getUrlContents(url);
            Log.d(SplashScreen.TAG, json);

            boolean digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;

            }else {
                error=0;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (dialog.isShowing())
                dialog.dismiss();

            if (error == 1) {
                UserAlert_Dialog userAlert_dialog = new UserAlert_Dialog("unable to generate incentive", Invoice.this,R.mipmap.cross);
                userAlert_dialog.showDialog_finish();
            } else

                if (userData.getPaymentType().equals("COD"))
                {

                        UserAlert_Dialog dialog1 = new UserAlert_Dialog("Order is confirmed",
                                getResources().getString(R.string.my_task), Invoice.this, R.mipmap.tick);
                        dialog1.showDialog_newFragment();

            }else{
                    if(Double.parseDouble(invoiceInfo.getNetBalance())>0) {
                        UserAlert_Dialog dialog1 = new UserAlert_Dialog("Please collect the amount "+invoiceInfo.getTotalAmount(),
                                getResources().getString(R.string.my_task), Invoice.this, R.mipmap.tick);
                        dialog1.showDialog_newFragment();
                    }else {
                        UserAlert_Dialog dialog1 = new UserAlert_Dialog("Order is confirmed",
                                getResources().getString(R.string.my_task), Invoice.this, R.mipmap.tick);
                        dialog1.showDialog_newFragment();
                    }
            }
        }


    }




    @Override

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //boolean result;
        if (keyCode == KeyEvent.KEYCODE_HOME) {
            //Do Code Here
            Log.d(SplashScreen.TAG,"Home key press");
            // If want to block just return false
            return false;
        }
        if (keyCode == KeyEvent.KEYCODE_MOVE_HOME) {
            //Do Code Here
            // If want to block just return false

            Log.d(SplashScreen.TAG,"MOVE_HOME key press");

            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public void onBackPressed() {

    }




}
