package com.zeusadssolutions.easyghaat_empapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;


import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.model.ProductData;
import com.zeusadssolutions.easyghaat_empapp.utils.LoadImageTask;

import java.util.ArrayList;

/**
 * Created by HP on 17-10-2016.
 */
public class HouseHoldAdapter  extends RecyclerView.Adapter<HouseHoldAdapter.MyViewHolder> {
    int count=0;
    private ArrayList<ProductData> householdList;
    Activity activity;
    public HouseHoldAdapter() {
    }

    public HouseHoldAdapter( Activity activity, ArrayList<ProductData> householdList) {
        this.activity = activity;
        this.householdList = householdList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.household_cat_row, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        holder.clothOption.setText(householdList.get(position).getProduct_Name());
        String imgUrl=new StringBuilder("http://admin.cashbachat.com")
                .append(householdList.get(position).getProduct_Imgurl()).toString();

        new LoadImageTask(holder.clothImage, activity).execute(imgUrl);
    }

    @Override
    public int getItemCount() {
        return householdList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView clothOption, clothQty;
        public CardView card_view_household;
        public ImageView clothImage, qty_plus, qty_sub;
        public Spinner sizeCloth_spinner;
        public MyViewHolder(View itemView) {
            super(itemView);

            clothOption = (TextView) itemView.findViewById(R.id.clothOption_household);
            qty_plus = (ImageView) itemView.findViewById(R.id.qty_plus_img_household);
            qty_sub = (ImageView) itemView.findViewById(R.id.qty_minus_img_household);
            clothQty = (TextView) itemView.findViewById(R.id.txtQty_household);
            clothImage = (ImageView) itemView.findViewById(R.id.pImageCat_household);
            sizeCloth_spinner=(Spinner)itemView.findViewById(R.id.spinnerSize_household);
            card_view_household=(CardView)itemView.findViewById(R.id.card_view_household);
            ArrayAdapter adapter = ArrayAdapter.createFromResource(activity, R.array.sizeCloth, android.R.layout.simple_spinner_item);

            // Set the layout to use for each dropdown item
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sizeCloth_spinner.setAdapter(adapter);
            card_view_household.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    count=0;
                    return false;

                }
            });
            qty_sub.setOnClickListener(this);
            qty_plus.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                case R.id.qty_minus_img_household:
                    count=count-1;
                    if(count<0)
                    {
                        clothQty.setText("0");
                    }else
                    {
                        clothQty.setText(String.valueOf(count));
                    }


                case R.id.qty_plus_img_household:

                    count=count+1;
                    if(count<0)
                    {
                        clothQty.setText("0");
                    }else
                    {
                        clothQty.setText(String.valueOf(count));
                    }

                    break;
            }
        }
    }
}
