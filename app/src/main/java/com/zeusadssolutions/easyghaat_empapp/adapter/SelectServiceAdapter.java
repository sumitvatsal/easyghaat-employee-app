package com.zeusadssolutions.easyghaat_empapp.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.model.MakeOrderDetails;
import com.zeusadssolutions.easyghaat_empapp.utils.SelectableAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 07-Feb-17.
 */

public class SelectServiceAdapter extends SelectableAdapter<SelectServiceAdapter.ServiceHolder> {

    List<String> serviceList;
    static Activity  activity;
    private static View lastCheckedV = null;
    int serviceName[]={R.string.wash_iron,R.string.steam_iron,R.string.premium_wash,
                    R.string.dry_clean,R.string.only_iron};

    int serviceImg[]={R.mipmap.wash_iron_detail,R.mipmap.steam_iron,R.mipmap.premium_wash,
            R.mipmap.dry_clean_cat,R.mipmap.wash_iron_cat};

    private ServiceHolder.ClickListener clickListener;

    //public static List<Integer> serviceIDList;

    public SelectServiceAdapter(List<String> serviceList, Activity activity,ServiceHolder.ClickListener clickListener) {
        this.serviceList = serviceList;
        this.activity = activity;
        this.clickListener=clickListener;
       // serviceIDList=new ArrayList<>();
    }

    @Override
    public ServiceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.servicecard, parent, false);

        return new ServiceHolder(itemView,clickListener);
    }

    @Override
    public void onBindViewHolder(final ServiceHolder holder, final int position) {
        //   holder.text_VendorAddress.setText("1");
        holder.txtServiceName.setText(serviceName[position]);
        holder.imgService.setImageResource(serviceImg[position]);

        if(position==0)
        {
            lastCheckedV=holder.viewServiceLine;
            holder.viewServiceLine.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public int getItemCount() {
        return serviceList.size();
    }

    public static class ServiceHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        public TextView txtServiceName;
        public LinearLayout linearServiceId;
        public ImageView imgService;
        public View viewServiceLine;
        private ClickListener listener;

        public ServiceHolder(View itemView, ClickListener listener) {
            super(itemView);
            this.listener = listener;
            viewServiceLine=(View)itemView.findViewById(R.id.viewServiceLine);
            txtServiceName=(TextView)itemView.findViewById(R.id.txtServiceName);
            linearServiceId=(LinearLayout)itemView.findViewById(R.id.linearServiceId);
            imgService=(ImageView)itemView.findViewById(R.id.imgService);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onItemClicked(getAdapterPosition());
                View checked_v=(View)viewServiceLine;
                if(lastCheckedV != null){
                    lastCheckedV.setBackgroundColor(activity.getResources().getColor(R.color.toolBar_color2));
                }
                lastCheckedV=checked_v;
                checked_v.setBackgroundColor(activity.getResources().getColor(R.color.headerbg));
            }

        }

        public interface ClickListener {
            public void onItemClicked(int position);

        }
    }

}
