package com.zeusadssolutions.easyghaat_empapp.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.model.HandoverData;
import com.zeusadssolutions.easyghaat_empapp.model.InstantOrderDetails;
import com.zeusadssolutions.easyghaat_empapp.model.MakeOrderDetails;
import com.zeusadssolutions.easyghaat_empapp.model.MyTaskData;
import com.zeusadssolutions.easyghaat_empapp.model.TaskDetails_UserData;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;
import com.zeusadssolutions.easyghaat_empapp.utils.UserAlert_Dialog;
import com.zeusadssolutions.easyghaat_empapp.utils.StartNewActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 01-10-2016.
 */
public class HandOver_Adapter extends RecyclerView.Adapter<HandOver_Adapter.MyViewHolder>  {

    List<HandoverData> taskList;
    Activity activity;
    UserAlert_Dialog dialogd;
    public static   ArrayList<MakeOrderDetails> makeOrderDetails;
    public static int position=0;
    public static  TaskDetails_UserData myTaskData;
    private PrefManager prefManager;
    public HandOver_Adapter(List<HandoverData> taskList, Activity activity) {
        this.taskList = taskList;
        this.activity = activity;
        prefManager=new PrefManager(activity);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.handover_cardview, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position)  {


           if(position==0){
                holder.expand.setVisibility(View.GONE);
                holder.collapse.setVisibility(View.VISIBLE);
                holder.linearDetail_Vendor.setVisibility(View.VISIBLE);
            }


        holder.textTaskId_handover.setText(taskList.get(position).getTaskID());
        holder.textCustId_handover.setText(taskList.get(position).getCustomerId());
        holder.textOrderId_handover.setText(taskList.get(position).getOrderId());

        holder.textDeliveryType_handover.setText(taskList.get(position).getDeliveryType());
        holder.textFrom_handover.setText(taskList.get(position).getFromArea());
        holder.textTo_handover.setText(taskList.get(position).getToArea());

        Log.d(SplashScreen.TAG, "vendor pin @ handover-" + taskList.get(position).getVendorPin());
        prefManager.setStringPreference("pin", taskList.get(position).getVendorPin());
       String vendorAddress= new StringBuilder(taskList.get(position).getVendorName()+"\n")
                .append(taskList.get(position).getVendorShopNo() + "\t" + taskList.get(position).getVendorStreet() + "\n")
               .append(taskList.get(position).getVendroLandmark()+"\n"+taskList.get(position).getVendorCity()+"\n")

                .toString();
        holder.text_VendorAddress.setText(vendorAddress);

    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }
    // Clean all elements of the recycler
    public void clear() {
        taskList.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addAll( List<HandoverData> taskList) {
        taskList.addAll(taskList);
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        public TextView text_VendorAddress,textTaskId_handover,textCustId_handover,textFrom_handover,textTo_handover,
                textDeliveryType_handover,textOrderId_handover;
        public Button btnView,btnHandover;
        public ImageView expand,collapse;
        public LinearLayout linearDetail_Vendor;
        public MyViewHolder(View itemView) {
            super(itemView);

            text_VendorAddress=(TextView)itemView.findViewById(R.id.textVendorAddress_handover);
            textTaskId_handover=(TextView)itemView.findViewById(R.id.textTaskId_handover);
            textCustId_handover=(TextView)itemView.findViewById(R.id.textCustId_handover);
            textFrom_handover=(TextView)itemView.findViewById(R.id.textFrom_handover);
            textTo_handover=(TextView)itemView.findViewById(R.id.textTo_handover);
            textDeliveryType_handover=(TextView)itemView.findViewById(R.id.textDeliveryType_handover);
            textOrderId_handover=(TextView)itemView.findViewById(R.id.textOrderId_handover);


            btnHandover=(Button)itemView.findViewById(R.id.btnHandover_handover);
            btnView=(Button)itemView.findViewById(R.id.btnView_handover);

            expand=(ImageView)itemView.findViewById(R.id.expand_handover);
            collapse=(ImageView)itemView.findViewById(R.id.collapse_handover);
            linearDetail_Vendor=(LinearLayout)itemView.findViewById(R.id.linearDetail_handover);

            btnView.setOnClickListener(this);
            expand.setOnClickListener(this);
            collapse.setOnClickListener(this);
            btnHandover.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                case R.id.expand_handover:
                    expand.setVisibility(View.GONE);
                    collapse.setVisibility(View.VISIBLE);
                   linearDetail_Vendor.setVisibility(View.VISIBLE);
                    break;

                case R.id.collapse_handover:
                   expand.setVisibility(View.VISIBLE);
                    collapse.setVisibility(View.GONE);
                    linearDetail_Vendor.setVisibility(View.GONE);
                    break;

                case R.id.btnView_handover:

                    position=getPosition();
                    Log.d(SplashScreen.TAG, "order id:-" + taskList.get(getPosition()).getOrderId());
                    new getOrderDetails().execute();

                    break;

                case R.id.btnHandover_handover:

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

                    dialogBuilder.setTitle("EasyGhaat");
                    dialogBuilder.setMessage("Enter secret pin");

                    final EditText input = new EditText(activity);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);
                    dialogBuilder.setView(input);
                    dialogBuilder.setIcon(R.mipmap.img);
                    input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});
                    dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            String pin = input.getText().toString();
                            if (taskList.get(getPosition()).getVendorPin().equals(pin)) {

                               new handoverOrderCreate().execute();
                            } else {
                                dialogd = new UserAlert_Dialog("Wrong pin", activity,R.mipmap.cross);
                                dialogd.showDialog_finish();
                            }


                        }
                    });
                    AlertDialog b = dialogBuilder.create();
                    b.show();
                    Button bg=b.getButton(DialogInterface.BUTTON_POSITIVE);
                    // bg.setBackgroundColor(getActivity().getResources().getColor(R.color.toolBar_color2));
                    bg.setTextColor(activity.getResources().getColor(R.color.toolBar_color2));

                    break;
            }

        }

        /*generating handover task*/
        private class handoverOrderCreate extends AsyncTask<Void,Void,Void> {
            int error=0;
            ProgressDialog dialog;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                // Showing progress dialog
                dialog = new ProgressDialog(activity);
                dialog.setMessage("Updating...");
                dialog.setCancelable(false);
                dialog.show();
            }
            @Override
            protected Void doInBackground(Void... params) {
                WebService_Hit webService_hit = new WebService_Hit();

                String instantUrl=new StringBuilder(SplashScreen.DOMAIN+"api/EmployeeIncentive/AddHandoverTask/ByPickUpId/")
                        .append(taskList.get(position).getPickUpId().trim())
                        .toString();
                String makeJson = webService_hit.getUrlContents(instantUrl);
                Log.d(SplashScreen.TAG, "handover-" + makeJson);

                boolean digitsOnly = TextUtils.isDigitsOnly(makeJson);
                if (digitsOnly) {
                    error=1;


                }else {
                    error=0;
                }


                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                // Dismiss the progress dialog
                if (dialog.isShowing())
                    dialog.dismiss();
                if(error==1)
                {
                    dialogd = new UserAlert_Dialog("Unable to Handover, Please try again",activity.getResources().getString(R.string.handover),
                            activity,R.mipmap.cross);
                    dialogd.showDialog_newFragment();

                }else {


                    dialogd = new UserAlert_Dialog("Handover task to vendor",activity.getResources().getString(R.string.handover),
                            activity,R.mipmap.tick);
                    dialogd.showDialog_newFragment();


                }
            }

        }
        private class getOrderDetails extends AsyncTask<Void, Void, Void> {
            private ProgressDialog dialog;
            int error=0;boolean digitsOnly;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                // Showing progress dialog
                dialog = new ProgressDialog(activity);
                dialog.setMessage("Please wait...");
                dialog.setCancelable(false);
                dialog.show();
            }

            @Override
            protected Void doInBackground(Void... params) {

                String  url = new StringBuilder(SplashScreen.DOMAIN+"api/ViewMakeOrder/MakeOrder/?TaskId=")
                        .append(taskList.get(position).getTaskID().trim())
                        .toString();

                WebService_Hit webService_hit = new WebService_Hit();
                String json = webService_hit.getUrlContents(url);
                Log.d(SplashScreen.TAG, "OrderDetails-" + json);
                digitsOnly = TextUtils.isDigitsOnly(json);
                if (digitsOnly) {
                    error=1;


                }else {
                    error=0;

                    MyOrderData(json);

                        String instantUrl = new StringBuilder(SplashScreen.DOMAIN + "api/ViewMakeOrder/MakeOrderDetail/?OrderId=")
                                .append(taskList.get(position).getOrderId().trim())
                                .toString();
                        String makeJson = webService_hit.getUrlContents(instantUrl);
                        Log.d(SplashScreen.TAG, "OrderDetails-" + makeJson);
                    digitsOnly = TextUtils.isDigitsOnly(makeJson);
                        if (digitsOnly) {
                            error=1;


                        }else {
                            error = 0;
                            MakeOrderData(makeJson);
                        }



                }
                return null;
            }


            @Override
            protected void onPostExecute(Void result) {
                String fragmentName;
                super.onPostExecute(result);
                // Dismiss the progress dialog
                if (dialog.isShowing())
                    dialog.dismiss();

                if(error==1)
                {
                    Toast.makeText(activity, "Unable to get task details,Please try again ", Toast.LENGTH_SHORT).show();
                }else {

                        fragmentName = activity.getResources().getString(R.string.view_order_vendor);


                    StartNewActivity startNewActivity = new StartNewActivity();
                    startNewActivity.showActivity(activity, fragmentName);
                }

            }


            private void MyOrderData(String sJson) {
                if (sJson != null) {
                    try {

                        myTaskData=new TaskDetails_UserData();
                        // Getting JSON Array node
                        JSONArray jArray = new JSONArray(sJson);
                        if(jArray.length()!=0) {

                            JSONObject object = jArray.getJSONObject(0);

                            myTaskData = myTaskData.TaskDetailsJsonTo(object);

                        }
                    } catch (final JSONException e) {
                        Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                    }
                }

            }


            private void MakeOrderData(String sJson) {
                if (sJson != null) {
                    makeOrderDetails=new ArrayList<>();
                    try {

                        MakeOrderDetails myTaskData=new MakeOrderDetails();
                        // Getting JSON Array node
                        JSONArray jArray = new JSONArray(sJson);
                        if(jArray.length()!=0) {
                            for (int i=0;i<jArray.length();i++) {
                                JSONObject object = jArray.getJSONObject(i);
                                myTaskData = myTaskData.MakeOrdersJsonTo(object);
                                makeOrderDetails.add(myTaskData);
                            }

                        }
                    } catch (final JSONException e) {
                        Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                    }
                }

            }

        }
    }
}
