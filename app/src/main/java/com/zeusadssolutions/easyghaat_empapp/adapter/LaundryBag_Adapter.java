package com.zeusadssolutions.easyghaat_empapp.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;


import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.fragment.ViewOrder_makeOrder;
import com.zeusadssolutions.easyghaat_empapp.model.GetLaundryBagData;
import com.zeusadssolutions.easyghaat_empapp.model.LaundryBagDetails;
import com.zeusadssolutions.easyghaat_empapp.utils.LoadImageTask;
import com.zeusadssolutions.easyghaat_empapp.utils.StartNewActivity;
import com.zeusadssolutions.easyghaat_empapp.utils.UserAlert_Dialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by Zeusadsolutions on 31/08/2016.
 */
public class LaundryBag_Adapter extends RecyclerView.Adapter<LaundryBag_Adapter.MyViewHolder>{

    Activity activity;
    private ArrayList<LaundryBagDetails> laundryBagDetailses;
    private int currentPosition;
    public static int count=0;

    private GetLaundryBag_Adapter getLaundryBagAdapter;
    MyViewHolder holder;
    private  AlertDialog alertDialog;

    public class MyViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        public TextView bagName,bagName_price;
        public ImageView bagImage;



        public MyViewHolder(View view) {
            super(view);
            bagName = (TextView) view.findViewById(R.id.txtBagName);
            bagName_price = (TextView) view.findViewById(R.id.txtBagBrice);

            bagImage=(ImageView)view.findViewById(R.id.bagImage);


            bagImage.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()){

                case R.id.bagImage:
                    currentPosition=getAdapterPosition();
                    CreateAlertDialogWithCheckBox();
                    break;
            }


        }
    }

    public LaundryBag_Adapter(Activity context, ArrayList<LaundryBagDetails> horizontalList) {
        activity=context;
        this.laundryBagDetailses = horizontalList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.laundrybag_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        this.holder=holder;
        holder.bagName.setText(laundryBagDetailses.get(position).getMaterialName());
        holder.bagName_price.setText("Rs." + laundryBagDetailses.get(position).getPrice());
        new LoadImageTask(holder.bagImage, activity).execute(laundryBagDetailses.get(position).getBagImgUrl());

    }


    @Override
    public int getItemCount() {
        return laundryBagDetailses.size();
    }

    private void CreateAlertDialogWithCheckBox()
    {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
     LayoutInflater inflater = LayoutInflater.from(activity);
        final View dialogView = inflater.inflate(R.layout.spinner_dialog, null);
        dialogBuilder.setView(dialogView);
        final ImageView bagImageLarge=(ImageView)dialogView.findViewById(R.id.bagImageLarge);
        final TextView bagName_qty=(TextView)dialogView.findViewById(R.id.bagName_qty);
        final TextView bagPrice_qty=(TextView)dialogView.findViewById(R.id.bagPrice_qty);
        bagName_qty.setText("Bag Name: " + laundryBagDetailses.get(currentPosition).getMaterialName());
        bagPrice_qty.setText("Price Rs." + laundryBagDetailses.get(currentPosition).getPrice());

        new LoadImageTask(bagImageLarge, activity).execute(laundryBagDetailses.get(currentPosition).getBagImgUrl());
        final NumberPicker numberPicker = (NumberPicker)dialogView.findViewById(R.id.numberPicker1);
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(10);
        numberPicker.setWrapSelectorWheel(false);
        dialogBuilder.setTitle("Select Quantity");

        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
              //  Toast.makeText(activity,String.valueOf(newVal),Toast.LENGTH_SHORT).show();
                count=newVal;

            }
        });


        dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        if (count == 0) {

                            Toast.makeText(activity, "Not selected any Bag, Please select Numbers of Bag required.", Toast.LENGTH_SHORT).show();
                        } else {
                            new postOrderData().execute();

                        }
                    }
                }

            )
                    .

            setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            //  Your code when user clicked on Cancel
                            // holder.txtQty.setText("0");

                            String fragmentName = activity.getResources().getString(R.string.view_order_make);
                            StartNewActivity activity1 = new StartNewActivity();
                            activity1.showActivity(activity, fragmentName);



                        }
                    }

            );

            alertDialog=dialogBuilder.create();
            alertDialog.getWindow().

            setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

            alertDialog.show();

        }

    /*to post laundry bag data*/
        private class postOrderData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog

        }
        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();

                String instantUrl = new StringBuilder(SplashScreen.DOMAIN + "/api/Order_LaundryBag_Tbl")
                        .toString();
                JSONObject postDataParams = new JSONObject();
                try {
                    postDataParams.put("OrderId", ViewOrder_makeOrder.userData.getOrderId().trim());
                    postDataParams.put("LaundryBagId", laundryBagDetailses.get(currentPosition).getLaundryBagId());
                    postDataParams.put("Count", count);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String makeJson = webService_hit.postWithParamUrlContents(postDataParams, instantUrl);
                Log.d(SplashScreen.TAG, "postLaundry bag-" + makeJson);

            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
// Dismiss the progress dialog
              new getOrderedLaundryBag().execute();


        }

    }

    /*to get ordered laundry bag details reason*/
    private class getOrderedLaundryBag extends AsyncTask<Void, Void, Void>{

        int error=0;
        @Override
        protected Void doInBackground(Void... params) {
            WebService_Hit webService_hit = new WebService_Hit();
           ;
            String url = new StringBuilder(SplashScreen.DOMAIN+"api/Order_LaundryBag_Tbl/GetOrderLaundryBag/ByOrderId/")
                    .append(ViewOrder_makeOrder.userData.getOrderId())
                    .toString();
            String json = webService_hit.getUrlContents(url);
            Log.d(SplashScreen.TAG, "Laundry bag details---" + json);
            boolean digitsOnly = TextUtils.isDigitsOnly(json);
            if (digitsOnly) {
                error=1;


            }else {
                error=0;
                getBagDetails(json);
            }



            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(error==1)
            {
                ViewOrder_makeOrder.selectBag=0;
                ViewOrder_makeOrder.linearEditLaundry_makeOrder.setVisibility(View.GONE);
                ViewOrder_makeOrder.linearGetBagDetails.setVisibility(View.GONE);
            }else {
                ViewOrder_makeOrder.selectBag=1;
                ViewOrder_makeOrder.linearEditLaundry_makeOrder.setVisibility(View.VISIBLE);
                ViewOrder_makeOrder.linearGetBagDetails.setVisibility(View.VISIBLE);
                getLaundryBagAdapter = new GetLaundryBag_Adapter(activity, ViewOrder_makeOrder.getLaundryBagDataArrayList);

                LinearLayoutManager verLayoutManager
                        = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
                ViewOrder_makeOrder.geLaundryBag_recycler.setLayoutManager(verLayoutManager);

                ViewOrder_makeOrder.geLaundryBag_recycler.setAdapter(getLaundryBagAdapter);
            }
        }

        private void getBagDetails(String sJson) {
            if (sJson != null) {

                ViewOrder_makeOrder.getLaundryBagDataArrayList=new ArrayList<>();
                try {

                    GetLaundryBagData details=new GetLaundryBagData();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if(jArray.length()!=0) {
                        for (int i=0;i<jArray.length();i++) {
                            JSONObject object = jArray.getJSONObject(i);

                            details = details.GetLaundryBagDataJsonTo(object);
                            Log.d(SplashScreen.TAG, "my product name received :---" + details.getMaterialName());
                            ViewOrder_makeOrder. getLaundryBagDataArrayList.add(details);
                        }

                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }


    }


}
