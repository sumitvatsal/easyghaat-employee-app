package com.zeusadssolutions.easyghaat_empapp.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.model.CashPendingData;
import com.zeusadssolutions.easyghaat_empapp.model.MakeOrderDetails;

import java.util.List;

/**
 * Created by HP on 22-12-2016.
 */
public class CashPendingAdapter extends RecyclerView.Adapter<CashPendingAdapter.MyViewHolder> {
    List<CashPendingData> cashPendingDataList;
    Activity activity;

    public CashPendingAdapter(List<CashPendingData> cashPendingDataList, Activity activity) {
        this.cashPendingDataList = cashPendingDataList;
        this.activity = activity;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.order_details_card, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        //   holder.text_VendorAddress.setText("1");

        holder.txt_jobId.setText(cashPendingDataList.get(position).getTaskId());
        holder.txtcusId.setText(cashPendingDataList.get(position).getCustomerId());

        holder.txt_OrderId.setText(cashPendingDataList.get(position).getOrderId());
        holder.txtDate.setText(cashPendingDataList.get(position).getCreateDate().substring(0,10));

        holder.txtCollectionAmount.setText(cashPendingDataList.get(position).getTotalAmount());


    }

    @Override
    public int getItemCount() {
        return cashPendingDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder  {

        public TextView txt_jobId, txtcusId,
                txt_OrderId, txtDate,
                txtCollectionAmount;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtcusId = (TextView) itemView.findViewById(R.id.txt_category_orderDetails);
            txt_jobId = (TextView) itemView.findViewById(R.id.txt_clothName_orderDetails);
            txt_OrderId = (TextView) itemView.findViewById(R.id.txt_service_orderDetails);
            txtDate = (TextView) itemView.findViewById(R.id.txt_count_orderDetails);
            txtCollectionAmount = (TextView) itemView.findViewById(R.id.txt_subAmount_orderDetails);


        }

    }
}
