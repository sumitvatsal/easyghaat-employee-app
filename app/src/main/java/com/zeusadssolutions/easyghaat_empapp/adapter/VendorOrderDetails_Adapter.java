package com.zeusadssolutions.easyghaat_empapp.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.model.MakeOrderDetails;

import java.util.List;

/**
 * Created by HP on 22-12-2016.
 */
public class VendorOrderDetails_Adapter  extends RecyclerView.Adapter<VendorOrderDetails_Adapter.MyViewHolder> {
    List<MakeOrderDetails> orderList;
    Activity activity;

    public VendorOrderDetails_Adapter(List<MakeOrderDetails> orderList, Activity activity) {
        this.orderList = orderList;
        this.activity = activity;
        Log.d(SplashScreen.TAG, "orderDetails-received-" + orderList.get(0).getProductName());
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.vendororderdetails_card, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        holder.txt_clothName.setText(orderList.get(position).getProductName());
        holder.txt_category.setText(orderList.get(position).getCategoryName());

        holder.txt_service.setText(orderList.get(position).getService());
        holder.txt_count.setText(orderList.get(position).getQunatity());




    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder  {

        public TextView txt_clothName, txt_category,
                txt_service, txt_count;


        public MyViewHolder(View itemView) {
            super(itemView);

            txt_category = (TextView) itemView.findViewById(R.id.txt_category_vendorDetails);
            txt_clothName = (TextView) itemView.findViewById(R.id.txt_clothName_vendorDetails);
            txt_count = (TextView) itemView.findViewById(R.id.txt_count_vendorDetails);
            txt_service = (TextView) itemView.findViewById(R.id.txt_service_vendorDetails);



        }

    }
}
