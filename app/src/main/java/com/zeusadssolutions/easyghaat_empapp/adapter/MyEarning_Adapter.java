package com.zeusadssolutions.easyghaat_empapp.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.model.EarningData;
import com.zeusadssolutions.easyghaat_empapp.utils.UserAlert_Dialog;

import java.util.List;

/**
 * Created by HP on 04-10-2016.
 */
public class MyEarning_Adapter extends RecyclerView.Adapter<MyEarning_Adapter.MyViewHolder>  {

    List<EarningData> earnList;
    Activity activity;

    public MyEarning_Adapter(List<EarningData> earnList, Activity activity) {
        this.earnList = earnList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.earning_cadrview, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position)  {
//        holder.text_VendorAddress.setText("1");
        double incentive=Double.parseDouble(earnList.get(position).getTotalIncentiveAmount());

        double distance=Double.parseDouble(earnList.get(position).getTotalDistanceAmount());


        Double totalAmount=incentive+distance;

        holder.txtTotalEarning_earning.setText(String.valueOf(totalAmount));
        holder.txtIncentive_earing.setText("Rs. "+earnList.get(position).getTotalIncentiveAmount());
        holder.txtDistance_earning.setText("Rs. "+earnList.get(position).getTotalDistanceAmount());
        holder.txtDate_eaning.setText(earnList.get(position).getOnDate().substring(0,10));
    }

    @Override
    public int getItemCount() {
        return earnList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        public TextView txtIncentive_earing,txtDistance_earning,txtTotalEarning_earning,txtDate_eaning;

        public ImageView imgDetail;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtDate_eaning=(TextView)itemView.findViewById(R.id.txtDate_eaning);
            txtIncentive_earing=(TextView)itemView.findViewById(R.id.txtIncentive_earing);
            txtDistance_earning=(TextView)itemView.findViewById(R.id.txtDistance_earning);
            txtTotalEarning_earning=(TextView)itemView.findViewById(R.id.txtTotalEarning_earning);
            imgDetail=(ImageView)itemView.findViewById(R.id.imgDetail_earning);



            imgDetail.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                case R.id.imgDetail_earning:
//                    Toast.makeText(activity,"Clicked on expand",Toast.LENGTH_SHORT).show();
                    UserAlert_Dialog dialog=new UserAlert_Dialog("Incentive:- Rs5/order\t Number of Orders:- 10\t Total Incentive: 10*5=50\n" +
                            " Distance:- Rs5/km\t Distance in Km.:- 10\t Total Distance: 10*5=50\n" +
                            " ",activity.getResources().getString(R.string.my_earning),activity,R.mipmap.img);

                    dialog.showDialog_finish();
                    break;


            }

        }
    }
}
