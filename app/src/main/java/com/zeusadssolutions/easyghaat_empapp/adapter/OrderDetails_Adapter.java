package com.zeusadssolutions.easyghaat_empapp.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.activity.OrderDetail;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.model.MakeOrderDetails;
import com.zeusadssolutions.easyghaat_empapp.model.MyTaskData;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;

import java.util.List;

/**
 * Created by HP on 07-12-2016.
 */
public class OrderDetails_Adapter  extends RecyclerView.Adapter<OrderDetails_Adapter.MyViewHolder> {
    List<MakeOrderDetails> orderList;
    Activity activity;

    public OrderDetails_Adapter(List<MakeOrderDetails> orderList, Activity activity) {
        this.orderList = orderList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.order_details_card, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        //   holder.text_VendorAddress.setText("1");
        if(Double.parseDouble(orderList.get(position).getLength())>0.0 && Double.parseDouble(orderList.get(position).getBreadth())>0.0 ) {

           // Double size=Double.parseDouble(orderList.get(position).getLength())*
                       // Double.parseDouble(orderList.get(position).getBreadth());


            holder.txt_clothName_orderDetails.setText(orderList.get(position).getProductName()+"\nSize: "+
                    orderList.get(position).getLength()+"*"+orderList.get(position).getBreadth());
        }else
            holder.txt_clothName_orderDetails.setText(orderList.get(position).getProductName());


        holder.txt_category_orderDetails.setText(orderList.get(position).getCategoryName());

        holder.txt_service_orderDetails.setText(orderList.get(position).getService());
        holder.txt_count_orderDetails.setText(orderList.get(position).getQunatity());

        holder.txt_subAmount_orderDetails.setText(orderList.get(position).getSubAmount());


    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder  {

        public TextView txt_clothName_orderDetails, txt_category_orderDetails,
                txt_service_orderDetails, txt_count_orderDetails,
                txt_subAmount_orderDetails;

        public MyViewHolder(View itemView) {
            super(itemView);

            txt_category_orderDetails = (TextView) itemView.findViewById(R.id.txt_category_orderDetails);
            txt_clothName_orderDetails = (TextView) itemView.findViewById(R.id.txt_clothName_orderDetails);
            txt_count_orderDetails = (TextView) itemView.findViewById(R.id.txt_count_orderDetails);
            txt_service_orderDetails = (TextView) itemView.findViewById(R.id.txt_service_orderDetails);
            txt_subAmount_orderDetails = (TextView) itemView.findViewById(R.id.txt_subAmount_orderDetails);


        }

    }
}
