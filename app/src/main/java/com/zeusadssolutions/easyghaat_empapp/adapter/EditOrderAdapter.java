package com.zeusadssolutions.easyghaat_empapp.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.fragment.ViewOrder_makeOrder;
import com.zeusadssolutions.easyghaat_empapp.model.MakeOrderDetails;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 07-12-2016.
 */
public class EditOrderAdapter extends RecyclerView.Adapter<EditOrderAdapter.MyViewHolder>  {

    ArrayList<MakeOrderDetails> orderList;
    Activity activity;
    int position=0;
    public static String selectedOrderList[];
    int i=0;


    public EditOrderAdapter(ArrayList<MakeOrderDetails> orderList, Activity activity) {
        this.orderList = orderList;
        this.activity = activity;

        Log.d(SplashScreen.TAG,"Recived data list--"+orderList.get(0).getProductName());
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.editorder_cards, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        holder.clothName.setText(orderList.get(position).getProductName());
        holder.category.setText(orderList.get(position).getCategoryName());

        holder.service.setText(orderList.get(position).getService());
        holder.countCloth.setText(orderList.get(position).getQunatity());


    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView clothName,
                category,service
        , countCloth;

        public ImageView plusImage,minusImage;
        public CheckBox checkBoxEdit;

        public MyViewHolder(View itemView) {
            super(itemView);
             checkBoxEdit=(CheckBox)itemView.findViewById(R.id.check_editOrder);
              clothName=(TextView)itemView.findViewById(R.id.txt_clothName_editOrder);
            category=(TextView)itemView.findViewById(R.id.txt_category_editOrder);
            service=(TextView)itemView.findViewById(R.id.txt_service_editOrder);
             countCloth=(TextView)itemView.findViewById(R.id.product_quantity_editOrder);

            plusImage=(ImageView)itemView.findViewById(R.id.qty_plus_editOrder);
            minusImage=(ImageView)itemView.findViewById(R.id.qty_minus_editOrder);


            checkBoxEdit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        ViewOrder_makeOrder.position = getAdapterPosition();
                        ViewOrder_makeOrder.qty = Integer.parseInt(orderList.get(getAdapterPosition()).getQunatity());
                        Log.d(SplashScreen.TAG, orderList.get(getPosition()).getQunatity());
                        minusImage.setClickable(true);
                        plusImage.setClickable(true);
                        minusImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (ViewOrder_makeOrder.qty <= 1)
                                {
                                    ViewOrder_makeOrder.qty = 1;
                                ViewOrder_makeOrder.changeOrder = 0;

                            }else{
                                ViewOrder_makeOrder.qty = ViewOrder_makeOrder.qty - 1;
                                ViewOrder_makeOrder.changeOrder = 1;
                            }

                            countCloth.setText(String.valueOf(ViewOrder_makeOrder.qty));
                        }
                    });


                        plusImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ViewOrder_makeOrder.qty=ViewOrder_makeOrder.qty+1;
                                countCloth.setText(String.valueOf(ViewOrder_makeOrder.qty));
                                ViewOrder_makeOrder.changeOrder=1;
                            }
                        });

                    } else {

                        minusImage.setClickable(false);
                        plusImage.setClickable(false);
                    }
                }
            });


        }



    }
}
