package com.zeusadssolutions.easyghaat_empapp.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.model.EmpInfo;
import com.zeusadssolutions.easyghaat_empapp.model.InstantOrderDetails;
import com.zeusadssolutions.easyghaat_empapp.model.MakeOrderDetails;
import com.zeusadssolutions.easyghaat_empapp.model.MyTaskData;
import com.zeusadssolutions.easyghaat_empapp.model.TaskDetails_UserData;
import com.zeusadssolutions.easyghaat_empapp.utils.PrefManager;
import com.zeusadssolutions.easyghaat_empapp.utils.StartNewActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 05-10-2016.
 */
public class MyTask_Adapter extends RecyclerView.Adapter<MyTask_Adapter.MyViewHolder>  {

    List<MyTaskData> taskList;
    Activity activity;
    PrefManager prefManager=null;
    public static  ArrayList<MakeOrderDetails> makeOrderDetails;
    public static  ArrayList<InstantOrderDetails> instantOrderDetails;
    public static TaskDetails_UserData myTaskData;
    public static int position=0;

    public MyTask_Adapter(List<MyTaskData> taskList, Activity activity) {
        this.taskList = taskList;
        this.activity = activity;
        prefManager=new PrefManager(activity);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.mytask_cardview, parent, false);

        return new MyViewHolder(itemView);
    }
    // Clean all elements of the recycler
    public void clear() {
        taskList.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addAll( List<MyTaskData> taskList) {
        taskList.addAll(taskList);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position)  {

        holder.txt_jobId_myTask.setText(taskList.get(position).getTASKID());
        holder.txt_type_myTask.setText(taskList.get(position).getTaskType());


        if(taskList.get(position).getTaskType().equals("Pick Up")) {
            holder.txt_to_myTask.setText(taskList.get(position).getVENDORAREA());

        }else{
            holder.txt_to_myTask.setText(taskList.get(position).getCUSTDELVAREA());
        }

        if(taskList.get(position).getTaskType().equals("Pick Up")) {
            holder.txt_from_myTask.setText(taskList.get(position).getCUSTPICKAREA());

        }else{
            holder.txt_from_myTask.setText(taskList.get(position).getVENDORAREA());
        }

        holder.txt_custId_myTask.setText(taskList.get(position).getCUSTOMERID());
        holder.txt_jobId_myTask.setText(taskList.get(position).getTASKID());

        holder.txt_timeSlot_myTask.setText(taskList.get(position).getPICKUPFROMTIME()+"-"+taskList.get(position).getPICKUPTOTIME());


    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        public ImageView imgDetail;

        public TextView txt_timeSlot_myTask,txt_type_myTask,txt_to_myTask,txt_from_myTask,txt_custId_myTask,txt_jobId_myTask;
        public MyViewHolder(View itemView) {
            super(itemView);


            imgDetail=(ImageView)itemView.findViewById(R.id.imgdetail_task);
            txt_custId_myTask=(TextView)itemView.findViewById(R.id.txt_custId_myTask);

            txt_timeSlot_myTask=(TextView)itemView.findViewById(R.id.txt_timeSlot_myTask);
            txt_type_myTask=(TextView)itemView.findViewById(R.id.txt_type_myTask);
            txt_to_myTask=(TextView)itemView.findViewById(R.id.txt_to_myTask);
            txt_from_myTask=(TextView)itemView.findViewById(R.id.txt_from_myTask);
            txt_jobId_myTask=(TextView)itemView.findViewById(R.id.txt_jobId_myTask);



            imgDetail.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                case R.id.imgdetail_task:
                    position=getAdapterPosition();
                    Log.d(SplashScreen.TAG, "order Type:-" + taskList.get(getPosition()).getOrderType());
                   new getOrderDetails().execute();
                    break;


            }

        }





    private class getOrderDetails extends AsyncTask<Void, Void, Void>{
        private ProgressDialog dialog;
        int error=0;boolean digitsOnly;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            dialog = new ProgressDialog(activity);
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String  url = new StringBuilder(SplashScreen.DOMAIN+"api/ViewMakeOrder/MakeOrder/?TaskId=")
                    .append(taskList.get(position).getTASKID().trim())
                    .toString();

            WebService_Hit webService_hit = new WebService_Hit();
            String json = webService_hit.getUrlContents(url);
            Log.d(SplashScreen.TAG, "customer info-" + json);

            if (webService_hit.responseCode!=200) {
                error=1;


            }else {
                error=0;

                MyOrderData(json);


                if (taskList.get(position).getOrderType().equals("Instant Order")) {
                    Log.d(SplashScreen.TAG,"inside if--orderType---"+taskList.get(position).getOrderType());
                    String instantUrl = new StringBuilder(SplashScreen.DOMAIN + "api/ViewInstantServices/ServiceRequired/?OrderId=")
                            .append(taskList.get(position).getORDERID().trim())
                            .toString();
                    String instantJson = webService_hit.getUrlContents(instantUrl);
                    Log.d(SplashScreen.TAG, "serviceDetails-" + instantJson);
                    //digitsOnly = TextUtils.isDigitsOnly(instantJson);
                    if (webService_hit.responseCode!=200) {
                        error=1;


                    }else {
                        error = 0;
                        InstantOrderData(instantJson);
                    }

                } else {
                    Log.d(SplashScreen.TAG,"inside else--orderType---"+taskList.get(position).getOrderType());
                    String instantUrl = new StringBuilder(SplashScreen.DOMAIN + "api/ViewMakeOrder/MakeOrderDetail/?OrderId=")
                            .append(taskList.get(position).getORDERID().trim())
                            .toString();
                    String makeJson = webService_hit.getUrlContents(instantUrl);
                    Log.d(SplashScreen.TAG, "OrderDetails-" + makeJson);


                    if (webService_hit.responseCode!=200) {
                        error=1;


                    }else {
                        error = 0;
                        MakeOrderData(makeJson);
                    }

                }

            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            String fragmentName;
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (dialog.isShowing())
                dialog.dismiss();

            if(error==1)
            {
                Toast.makeText(activity, "Unable to get task details,Please try again ", Toast.LENGTH_SHORT).show();
            }else {
                if (taskList.get(position).getOrderType().equals("Instant Order")) {

                    fragmentName = activity.getResources().getString(R.string.view_order_instant);

                } else {
                    fragmentName = activity.getResources().getString(R.string.view_order_make);
                }

                StartNewActivity startNewActivity = new StartNewActivity();
                startNewActivity.showActivity(activity, fragmentName);
                activity.finish();
            }

        }


        private void MyOrderData(String sJson) {
            if (sJson != null) {
                try {

                    myTaskData=new TaskDetails_UserData();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if(jArray.length()!=0) {

                        JSONObject object = jArray.getJSONObject(0);

                        myTaskData = myTaskData.TaskDetailsJsonTo(object);
                        Log.d(SplashScreen.TAG, "my order id received :---" + myTaskData.getOrderId());

                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }

        private void InstantOrderData(String sJson) {
            if (sJson != null) {

                instantOrderDetails=new ArrayList<>();
                try {

                    InstantOrderDetails myTaskData=new InstantOrderDetails();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if(jArray.length()!=0) {
                        for (int i=0;i<jArray.length();i++) {
                            JSONObject object = jArray.getJSONObject(i);

                            myTaskData = myTaskData.InstantOrdersJsonTo(object);
                            Log.d(SplashScreen.TAG, "my order id received :---" + myTaskData.getServiceName());

                            instantOrderDetails.add(myTaskData);

                        }


                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }


        private void MakeOrderData(String sJson) {
            if (sJson != null) {
                prefManager=new PrefManager(activity);
                makeOrderDetails=new ArrayList<>();
                try {

                    MakeOrderDetails myTaskData=new MakeOrderDetails();
                    // Getting JSON Array node
                    JSONArray jArray = new JSONArray(sJson);
                    if(jArray.length()!=0) {
                        for (int i=0;i<jArray.length();i++) {
                            JSONObject object = jArray.getJSONObject(i);

                            myTaskData = myTaskData.MakeOrdersJsonTo(object);
                            Log.d(SplashScreen.TAG, "my product name received :---" + myTaskData.getProductName());
                            makeOrderDetails.add(myTaskData);


                        }

                    }
                } catch (final JSONException e) {
                    Log.e(SplashScreen.TAG, "Json parsing error: " + e.getMessage());
                }
            }

        }

    }
}
}
