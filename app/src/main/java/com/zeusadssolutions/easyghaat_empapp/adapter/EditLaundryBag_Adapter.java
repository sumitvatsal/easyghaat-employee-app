package com.zeusadssolutions.easyghaat_empapp.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.fragment.ViewOrder_makeOrder;
import com.zeusadssolutions.easyghaat_empapp.model.GetLaundryBagData;
import com.zeusadssolutions.easyghaat_empapp.model.MakeOrderDetails;

import java.util.ArrayList;

/**
 * Created by HP on 30-12-2016.
 */
public class EditLaundryBag_Adapter extends RecyclerView.Adapter<EditLaundryBag_Adapter.MyViewHolder>{

    ArrayList<GetLaundryBagData> orderList;
    Activity activity;


    public EditLaundryBag_Adapter(ArrayList<GetLaundryBagData> orderList, Activity activity) {
        this.orderList = orderList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.editlaundry_card, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        holder.clothName.setText(orderList.get(position).getMaterialName());
        holder.price.setText(orderList.get(position).getSubAmount());
        holder.countCloth.setText(orderList.get(position).getCount());


    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView clothName,
                count,price
                , countCloth;

        public ImageView plusImage,minusImage;
        public CheckBox checkBoxEdit;

        public MyViewHolder(View itemView) {
            super(itemView);
            checkBoxEdit=(CheckBox)itemView.findViewById(R.id.check_editLaundry);
            clothName=(TextView)itemView.findViewById(R.id.txt_clothName_editLaundry);
            price=(TextView)itemView.findViewById(R.id.txt_price_editLaundry);
            countCloth=(TextView)itemView.findViewById(R.id.product_quantity_editLaundry);

            plusImage=(ImageView)itemView.findViewById(R.id.qty_plus_editLaundry);
            minusImage=(ImageView)itemView.findViewById(R.id.qty_minus_editLaundry);


            checkBoxEdit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                       // Toast.makeText(activity,"clicked on checked button selected",Toast.LENGTH_SHORT).show();
                        ViewOrder_makeOrder.laundryPosition = getAdapterPosition();
                        Log.d(SplashScreen.TAG,"laundry list position---"+String.valueOf( ViewOrder_makeOrder.laundryPosition));
                        ViewOrder_makeOrder.qtyBag = Integer.parseInt(orderList.get(getAdapterPosition()).getCount());
                        Log.d(SplashScreen.TAG,"laundry bag count---"+ orderList.get(getAdapterPosition()).getCount());

                        minusImage.setClickable(true);
                        plusImage.setClickable(true);
                        minusImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                               // Toast.makeText(activity, "clicked on minus button", Toast.LENGTH_SHORT).show();
                                if (ViewOrder_makeOrder.qtyBag <= 1) {
                                    ViewOrder_makeOrder.qtyBag = 1;
                                    ViewOrder_makeOrder.changeLaundry = 1;
                                    countCloth.setText(String.valueOf(ViewOrder_makeOrder.qtyBag));
                                } else {
                                    ViewOrder_makeOrder.changeLaundry = 1;
                                    ViewOrder_makeOrder.qtyBag = ViewOrder_makeOrder.qtyBag - 1;
                                    countCloth.setText(String.valueOf(ViewOrder_makeOrder.qtyBag));
                                }
                                Log.d(SplashScreen.TAG,"laundry bag count---"+ String.valueOf(ViewOrder_makeOrder.qtyBag));

                        }
                    });


                        plusImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                               // Toast.makeText(activity,"clicked on plus button",Toast.LENGTH_SHORT).show();

                                    ViewOrder_makeOrder.qtyBag = ViewOrder_makeOrder.qtyBag + 1;

                                    countCloth.setText(String.valueOf(ViewOrder_makeOrder.qtyBag));
                                    ViewOrder_makeOrder.changeLaundry = 1;


                                Log.d(SplashScreen.TAG,"laundry bag count---"+ String.valueOf( ViewOrder_makeOrder.qtyBag));

                            }
                        });

                    } else {
                       // Toast.makeText(activity,"clicked on checked not selected button",Toast.LENGTH_SHORT).show();
                        minusImage.setClickable(false);
                        plusImage.setClickable(false);
                    }
                }
            });


        }



    }
}
