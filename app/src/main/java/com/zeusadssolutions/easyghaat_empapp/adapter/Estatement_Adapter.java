package com.zeusadssolutions.easyghaat_empapp.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.model.EstatementData;
import com.zeusadssolutions.easyghaat_empapp.utils.UserAlert_Dialog;

import java.util.List;

/**
 * Created by HP on 29-12-2016.
 */

public class Estatement_Adapter extends RecyclerView.Adapter<Estatement_Adapter.MyViewHolder> {

    List<EstatementData> estatementDataList;
    Activity activity;

    public Estatement_Adapter(List<EstatementData> estatementDataList, Activity activity) {
        this.estatementDataList = estatementDataList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.estatement_cardview, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position)  {
//        holder.text_VendorAddress.setText("1");
        holder.txtTransferDate.setText(estatementDataList.get(position).getTransferDate().substring(0,10));
        holder.txtBankName.setText(estatementDataList.get(position).getBankName());
        holder.txtBankReferenceNo.setText(estatementDataList.get(position).getBankReferenceNo());
        holder.txtReason.setText(estatementDataList.get(position).getReason());
        holder.txtRemark.setText(estatementDataList.get(position).getRemark());
        holder.txtTransferedAmount.setText("Rs. "+estatementDataList.get(position).getTransferedAmount());
    }

    @Override
    public int getItemCount() {
        return estatementDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        public TextView txtTransferDate,txtBankName,txtBankReferenceNo,txtReason,txtRemark,txtTransferedAmount;

        public ImageView imgDetail;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtTransferDate=(TextView)itemView.findViewById(R.id.txtTransferDate);
            txtBankName=(TextView)itemView.findViewById(R.id.txtBankName);
            txtBankReferenceNo=(TextView)itemView.findViewById(R.id.txtBankReferenceNo);
            txtReason=(TextView)itemView.findViewById(R.id.txtReason);
            txtRemark=(TextView)itemView.findViewById(R.id.txtRemark);
            txtTransferedAmount=(TextView)itemView.findViewById(R.id.txtTransferedAmount);
            imgDetail.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                case R.id.imgDetail_earning:
//                    Toast.makeText(activity,"Clicked on expand",Toast.LENGTH_SHORT).show();
                    UserAlert_Dialog dialog=new UserAlert_Dialog("Incentive:- Rs5/order\t Number of Orders:- 10\t Total Incentive: 10*5=50\n" +
                            " Distance:- Rs5/km\t Distance in Km.:- 10\t Total Distance: 10*5=50\n" +
                            " ",activity.getResources().getString(R.string.my_earning),activity,R.mipmap.img);

                    dialog.showDialog_finish();
                    break;


            }

        }
    }
}
