package com.zeusadssolutions.easyghaat_empapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.WebService.WebService_Hit;
import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;
import com.zeusadssolutions.easyghaat_empapp.model.ProductData;
import com.zeusadssolutions.easyghaat_empapp.model.TaskDetails_UserData;
import com.zeusadssolutions.easyghaat_empapp.utils.LoadImageTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.zeusadssolutions.easyghaat_empapp.fragment.EditOrder.branchId;
import static com.zeusadssolutions.easyghaat_empapp.fragment.EditOrder.genderId;
import static com.zeusadssolutions.easyghaat_empapp.fragment.EditOrder.serviceId;

public class ClothOption_Adapter extends RecyclerView.Adapter<ClothOption_Adapter.MyViewHolder>  {
    int count=0,subAmount=0;
    public static ArrayList<ProductData> selectedProductList;
    Activity activity;
    TaskDetails_UserData userData;
    int pos;
    public ClothOption_Adapter(ArrayList<ProductData> selectedProductList, Activity activity) {

        this.selectedProductList=selectedProductList;
        this.activity=activity;
        userData= MyTask_Adapter.myTaskData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cloth_option_adapter, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.clothOption.setText(selectedProductList.get(position).getProduct_Name());

        String imgUrl=new StringBuilder("http://admin.cashbachat.com")
                .append(selectedProductList.get(position).getProduct_Imgurl()).toString();

        new LoadImageTask(holder.clothImage, activity).execute(imgUrl);

    }

    @Override
    public int getItemCount() {
        return selectedProductList.size();
    }

    public class MyViewHolder  extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView clothOption,clothQty;
        ImageView clothImage,qty_plus,qty_sub;
        public MyViewHolder(View itemView) {
            super(itemView);

            clothOption=(TextView)itemView.findViewById(R.id.clothOption);
            qty_plus=(ImageView)itemView.findViewById(R.id.qty_plus_img);
            qty_sub=(ImageView)itemView.findViewById(R.id.qty_minus_img);
            clothQty=(TextView)itemView.findViewById(R.id.cart_product_quantity_tv);
            clothImage=(ImageView)itemView.findViewById(R.id.pImageCart);

            qty_sub.setOnClickListener(this);
            qty_plus.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int rate;
            count=0;
            switch (v.getId())
            {
                case R.id.qty_minus_img:
                    Log.d(SplashScreen.TAG,"Clicked on minus image--");

                    pos=getAdapterPosition();

                    if (count < 0)
                    {
                        count = 0;

                    }else{
                        count = count- 1;
                    }

                    clothQty.setText(String.valueOf(count));
                    Log.d(SplashScreen.TAG,"val of count --"+String.valueOf(count));
                        rate=Integer.parseInt(selectedProductList.get(pos).getPrice().replaceAll("\\D",""));
                        subAmount=rate*count;
                        new addProduct().execute();
                    break;



                case R.id.qty_plus_img:
                    pos=getAdapterPosition();
                    Log.d(SplashScreen.TAG,"Clicked on minus image--");
                    if (count < 0)
                    {
                        count = 0;

                    }else{
                        count = count+ 1;
                    }

                    clothQty.setText(String.valueOf(count));
                    Log.d(SplashScreen.TAG,"val of count --"+String.valueOf(count));
                    rate=Integer.parseInt(selectedProductList.get(pos).getPrice().replaceAll("\\D",""));
                    subAmount=rate*count;
                    new addProduct().execute();

                    break;
            }
        }
    }


    private class addProduct extends AsyncTask<Void, Void, Void> {
        String url=null;
        private int error=0;


        @Override
        protected Void doInBackground(Void... params) {


            url = new StringBuilder("http://easyghaat.cashbachat.com/api/OrderDetail_tbl")
                    .toString();



            WebService_Hit webService_hit = new WebService_Hit();
            JSONObject postDataParams = new JSONObject();
            try {
                postDataParams.put("OrderId", userData.getOrderId().trim());
                postDataParams.put("PriceId", selectedProductList.get(pos).getPriceId().trim());
                postDataParams.put("Qunatity",count);
                postDataParams.put("SubAmount", subAmount);
                postDataParams.put("Length", 0);
                postDataParams.put("Breath", 0);
                postDataParams.put("CustomerId", userData.getCustomerId().trim());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String json = webService_hit.postWithParamUrlContents(postDataParams,url);
            if (webService_hit.responseCode!=201) {
                error=1;
            }else {
                error=0;
            }

            Log.d(SplashScreen.TAG, json);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(error==1)
            {
                Toast.makeText(activity, "Unable to get data,Please try again ", Toast.LENGTH_SHORT).show();
            }else {
            }

        }

    }
}
