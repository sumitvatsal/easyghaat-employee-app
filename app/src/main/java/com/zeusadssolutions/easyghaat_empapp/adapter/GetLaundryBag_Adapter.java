package com.zeusadssolutions.easyghaat_empapp.adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeusadssolutions.easyghaat_empapp.R;
import com.zeusadssolutions.easyghaat_empapp.model.GetLaundryBagData;
import com.zeusadssolutions.easyghaat_empapp.model.LaundryBagDetails;
import com.zeusadssolutions.easyghaat_empapp.utils.LoadImageTask;

import java.util.ArrayList;

/**
 * Created by HP on 30-12-2016.
 */
public class GetLaundryBag_Adapter extends RecyclerView.Adapter<GetLaundryBag_Adapter.MyViewHolder> {
    Activity activity;
    private ArrayList<GetLaundryBagData> getLaundryBagDatas;
    public static int count=0;

    public GetLaundryBag_Adapter(Activity activity, ArrayList<GetLaundryBagData> getLaundryBagDatas) {
        this.activity = activity;
        this.getLaundryBagDatas = getLaundryBagDatas;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder  {
        public TextView txtBagName_get,txtBagBrice_get,txtQty_get;
        public MyViewHolder(View view) {
            super(view);
            txtBagName_get = (TextView) view.findViewById(R.id.txtBagName_get);
            txtBagBrice_get = (TextView) view.findViewById(R.id.txtBagBrice_get);
            txtQty_get = (TextView) view.findViewById(R.id.txtQty_get);
        }

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.getlaundry_bag_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.txtBagName_get.setText(getLaundryBagDatas.get(position).getMaterialName());
        if(getLaundryBagDatas.get(position).getSubAmount().equals("0")) {
            holder.txtBagBrice_get.setText("");
        }else {
            holder.txtBagBrice_get.setText("Rs." + getLaundryBagDatas.get(position).getSubAmount());
        }
        holder.txtQty_get.setText(getLaundryBagDatas.get(position).getCount());


    }


    @Override
    public int getItemCount() {
        return getLaundryBagDatas.size();
    }

}
