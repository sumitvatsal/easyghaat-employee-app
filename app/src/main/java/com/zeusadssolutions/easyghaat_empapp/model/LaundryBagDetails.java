package com.zeusadssolutions.easyghaat_empapp.model;

import android.util.Log;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by HP on 09-12-2016.
 */
public class LaundryBagDetails {

    String laundryBagId,MaterialName,BagImgUrl,weightCapacity,CountCapacity,Price;

    public LaundryBagDetails() {
    }

    public String getLaundryBagId() {
        return laundryBagId;
    }

    public void setLaundryBagId(String laundryBagId) {
        this.laundryBagId = laundryBagId;
    }

    public String getMaterialName() {
        return MaterialName;
    }

    public void setMaterialName(String materialName) {
        MaterialName = materialName;
    }

    public String getBagImgUrl() {
        return BagImgUrl;
    }

    public void setBagImgUrl(String bagImgUrl) {
        BagImgUrl = bagImgUrl;
    }

    public String getWeightCapacity() {
        return weightCapacity;
    }

    public void setWeightCapacity(String weightCapacity) {
        this.weightCapacity = weightCapacity;
    }

    public String getCountCapacity() {
        return CountCapacity;
    }

    public void setCountCapacity(String countCapacity) {
        CountCapacity = countCapacity;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }


    public LaundryBagDetails MakeOrdersJsonTo(JSONObject object) {
        LaundryBagDetails laundryBagDetails =new LaundryBagDetails();
        Log.d(SplashScreen.TAG, "Inside MakeOrdersJsonTo method");
        try {
            // Check for error node in json
            Log.d(SplashScreen.TAG, String.valueOf(object.getString(String.valueOf("Id"))));

            laundryBagDetails.setLaundryBagId(object.getString(String.valueOf("Id")));
            laundryBagDetails.setMaterialName(object.getString(String.valueOf("MaterialName")));
            laundryBagDetails.setBagImgUrl(object.getString(String.valueOf("BagImgUrl")));
            laundryBagDetails.setWeightCapacity(object.getString(String.valueOf("weightCapacity")));
            laundryBagDetails.setCountCapacity(object.getString(String.valueOf("CountCapacity")));
            laundryBagDetails.setPrice(object.getString(String.valueOf("Price")));
            return laundryBagDetails;
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.getLogger(EmpInfo.class.getName()).log(Level.SEVERE, null, e);

        }
        return null;
    }
}
