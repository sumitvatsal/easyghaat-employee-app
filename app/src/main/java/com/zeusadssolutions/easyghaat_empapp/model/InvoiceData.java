package com.zeusadssolutions.easyghaat_empapp.model;

import android.util.Log;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by HP on 15-12-2016.
 */
public class InvoiceData {

    String Id,OrderId,SubAmount,PickUp_DeliveryCharges,SurgingAmount,PackageAdjustmentAmt,LaundryBagAmount,
            CashBackAmount,ServiceTaxAmt,SwachhBharatCess,KrishiKalyanCessAmt,TotalAmount,RoundOffAmount,
            WalletAdjustment,NetBalance;

    public InvoiceData() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getSubAmount() {
        return SubAmount;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public void setSubAmount(String subAmount) {
        SubAmount = subAmount;
    }

    public String getPickUp_DeliveryCharges() {
        return PickUp_DeliveryCharges;
    }

    public void setPickUp_DeliveryCharges(String pickUp_DeliveryCharges) {
        PickUp_DeliveryCharges = pickUp_DeliveryCharges;
    }

    public String getSurgingAmount() {
        return SurgingAmount;
    }

    public void setSurgingAmount(String surgingAmount) {
        SurgingAmount = surgingAmount;
    }

    public String getPackageAdjustmentAmt() {
        return PackageAdjustmentAmt;
    }

    public void setPackageAdjustmentAmt(String packageAdjustmentAmt) {
        PackageAdjustmentAmt = packageAdjustmentAmt;
    }

    public String getLaundryBagAmount() {
        return LaundryBagAmount;
    }

    public void setLaundryBagAmount(String laundryBagAmount) {
        LaundryBagAmount = laundryBagAmount;
    }

    public String getCashBackAmount() {
        return CashBackAmount;
    }

    public void setCashBackAmount(String cashBackAmount) {
        CashBackAmount = cashBackAmount;
    }

    public String getServiceTaxAmt() {
        return ServiceTaxAmt;
    }

    public void setServiceTaxAmt(String serviceTaxAmt) {
        ServiceTaxAmt = serviceTaxAmt;
    }

    public String getSwachhBharatCess() {
        return SwachhBharatCess;
    }

    public void setSwachhBharatCess(String swachhBharatCess) {
        SwachhBharatCess = swachhBharatCess;
    }

    public String getKrishiKalyanCessAmt() {
        return KrishiKalyanCessAmt;
    }

    public void setKrishiKalyanCessAmt(String krishiKalyanCessAmt) {
        KrishiKalyanCessAmt = krishiKalyanCessAmt;
    }

    public String getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        TotalAmount = totalAmount;
    }

    public String getRoundOffAmount() {
        return RoundOffAmount;
    }

    public void setRoundOffAmount(String roundOffAmount) {
        RoundOffAmount = roundOffAmount;
    }

    public String getWalletAdjustment() {
        return WalletAdjustment;
    }

    public void setWalletAdjustment(String walletAdjustment) {
        WalletAdjustment = walletAdjustment;
    }

    public String getNetBalance() {
        return NetBalance;
    }

    public void setNetBalance(String netBalance) {
        NetBalance = netBalance;
    }


    public InvoiceData InvoiceJsonTo(JSONObject object) {
        InvoiceData invoiceData =new InvoiceData();
        Log.d(SplashScreen.TAG, "Inside InvoiceJsonTo method");
        try {
            // Check for error node in json

            invoiceData.setId(object.getString(String.valueOf("Id")));
            invoiceData.setOrderId(object.getString(String.valueOf("OrderId")));
            invoiceData.setSubAmount(object.getString(String.valueOf("SubAmount")));

            invoiceData.setPickUp_DeliveryCharges(object.getString(String.valueOf("PickUp_DeliveryCharges")));
            invoiceData.setSurgingAmount(object.getString(String.valueOf("SurgingAmount")));
            invoiceData.setPackageAdjustmentAmt(object.getString(String.valueOf("PackageAdjustmentAmt")));

            invoiceData.setLaundryBagAmount(object.getString(String.valueOf("LaundryBagAmount")));
            invoiceData.setCashBackAmount(object.getString(String.valueOf("CashBackAmount")));
            invoiceData.setServiceTaxAmt(object.getString(String.valueOf("ServiceTaxAmt")));

            invoiceData.setSwachhBharatCess(object.getString(String.valueOf("SwachhBharatCess")));
            invoiceData.setKrishiKalyanCessAmt(object.getString(String.valueOf("KrishiKalyanCessAmt")));
            invoiceData.setTotalAmount(object.getString(String.valueOf("TotalAmount")));

            invoiceData.setRoundOffAmount(object.getString(String.valueOf("RoundOffAmount")));
            invoiceData.setWalletAdjustment(object.getString(String.valueOf("WalletAdjustment")));
            invoiceData.setNetBalance(object.getString(String.valueOf("NetBalance")));

            return invoiceData;
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.getLogger(EmpInfo.class.getName()).log(Level.SEVERE, null, e);

        }
        return null;
    }
}
