package com.zeusadssolutions.easyghaat_empapp.model;

import android.util.Log;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by HP on 28-11-2016.
 */
public class EmpInfo {

    String emp_id,asin_branchId,emp_name,password,per_phoneNo,Email,branchName,RoleName,RoleId;

    public EmpInfo() {
    }

    public String getRoleId() {
        return RoleId;
    }

    public void setRoleId(String roleId) {
        RoleId = roleId;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }

    public String getPassword() {
        return password;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getRoleName() {
        return RoleName;
    }

    public void setRoleName(String roleName) {
        RoleName = roleName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPer_phoneNo() {
        return per_phoneNo;
    }

    public void setPer_phoneNo(String per_phoneNo) {
        this.per_phoneNo = per_phoneNo;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getAsin_branchId() {
        return asin_branchId;
    }

    public void setAsin_branchId(String asin_branchId) {
        this.asin_branchId = asin_branchId;
    }

    public EmpInfo UserDetailsJsonTo(JSONObject object) {
        EmpInfo empInfo =new EmpInfo();
        Log.d(SplashScreen.TAG, "Inside UserDetailsJson method");
        try {

            // Check for error node in json


            Log.d(SplashScreen.TAG, String.valueOf(object.getString(String.valueOf("emp_name"))));
            // user successfully logged in
            empInfo.setEmp_id(object.getString(String.valueOf("emp_id")));
            empInfo.setEmp_name(object.getString(String.valueOf("emp_name")));
            empInfo.setPassword(object.getString(String.valueOf("password")));
            empInfo.setBranchName(object.getString(String.valueOf("Name")));
            empInfo.setPer_phoneNo(object.getString(String.valueOf("per_phoneNo")));
            empInfo.setEmail(object.getString(String.valueOf("EmailId")));
            empInfo.setRoleName(object.getString(String.valueOf("RoleName")));
            empInfo.setRoleId(object.getString(String.valueOf("RoleId")));
            empInfo.setAsin_branchId(object.getString(String.valueOf("asin_branchId")));
            return empInfo;
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.getLogger(EmpInfo.class.getName()).log(Level.SEVERE, null, e);

        }
        return null;
    }

}
