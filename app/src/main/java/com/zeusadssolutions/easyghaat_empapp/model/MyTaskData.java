package com.zeusadssolutions.easyghaat_empapp.model;

import android.util.Log;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by HP on 02-12-2016.
 */
public class MyTaskData {
    String  TASKID,ORDERID,
            PICKUPFROMTIME,PICKUPTOTIME,
            PICKUPDATE,DELVFROMTIME,
            DELVTOTIME,DELIVERYDATE,
            CUSTOMERID,VENDORID,
            VENDORAREA,CUSTPICKAREA,
            CUSTDELVAREA,TaskType,OrderType;

    public MyTaskData() {
    }

    public String getTASKID() {
        return TASKID;
    }

    public void setTASKID(String TASKID) {
        this.TASKID = TASKID;
    }

    public String getORDERID() {
        return ORDERID;
    }

    public String getOrderType() {
        return OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public void setORDERID(String ORDERID) {
        this.ORDERID = ORDERID;
    }

    public String getPICKUPFROMTIME() {
        return PICKUPFROMTIME;
    }

    public void setPICKUPFROMTIME(String PICKUPFROMTIME) {
        this.PICKUPFROMTIME = PICKUPFROMTIME;
    }

    public String getPICKUPTOTIME() {
        return PICKUPTOTIME;
    }

    public void setPICKUPTOTIME(String PICKUPTOTIME) {
        this.PICKUPTOTIME = PICKUPTOTIME;
    }

    public String getPICKUPDATE() {
        return PICKUPDATE;
    }

    public void setPICKUPDATE(String PICKUPDATE) {
        this.PICKUPDATE = PICKUPDATE;
    }

    public String getDELVFROMTIME() {
        return DELVFROMTIME;
    }

    public void setDELVFROMTIME(String DELVFROMTIME) {
        this.DELVFROMTIME = DELVFROMTIME;
    }

    public String getDELVTOTIME() {
        return DELVTOTIME;
    }

    public void setDELVTOTIME(String DELVTOTIME) {
        this.DELVTOTIME = DELVTOTIME;
    }

    public String getDELIVERYDATE() {
        return DELIVERYDATE;
    }

    public void setDELIVERYDATE(String DELIVERYDATE) {
        this.DELIVERYDATE = DELIVERYDATE;
    }

    public String getCUSTOMERID() {
        return CUSTOMERID;
    }

    public void setCUSTOMERID(String CUSTOMERID) {
        this.CUSTOMERID = CUSTOMERID;
    }

    public String getVENDORID() {
        return VENDORID;
    }

    public void setVENDORID(String VENDORID) {
        this.VENDORID = VENDORID;
    }

    public String getVENDORAREA() {
        return VENDORAREA;
    }

    public void setVENDORAREA(String VENDORAREA) {
        this.VENDORAREA = VENDORAREA;
    }

    public String getCUSTPICKAREA() {
        return CUSTPICKAREA;
    }

    public void setCUSTPICKAREA(String CUSTPICKAREA) {
        this.CUSTPICKAREA = CUSTPICKAREA;
    }

    public String getCUSTDELVAREA() {
        return CUSTDELVAREA;
    }

    public void setCUSTDELVAREA(String CUSTDELVAREA) {
        this.CUSTDELVAREA = CUSTDELVAREA;
    }

    public String getTaskType() {
        return TaskType;
    }

    public void setTaskType(String taskType) {
        TaskType = taskType;
    }



    public MyTaskData TaskDetailsJsonTo(JSONObject object) {
        MyTaskData taskData =new MyTaskData();
          Log.d(SplashScreen.TAG, "Inside TaskDetailsJsonTo method");

        try {
            Log.d(SplashScreen.TAG, "order id-----" + object.getString(String.valueOf("ORDERID")));
            // Check for error node in json
            taskData.setTASKID(object.getString(String.valueOf("TASKID")));

            taskData.setORDERID(object.getString(String.valueOf("ORDERID")));
            taskData.setOrderType(object.getString(String.valueOf("OrderType")));

            taskData.setPICKUPFROMTIME(object.getString(String.valueOf("PICKUPFROMTIME")));
            taskData.setPICKUPTOTIME(object.getString(String.valueOf("PICKUPTOTIME")));
            taskData.setPICKUPDATE(object.getString(String.valueOf("PICKUPDATE")));

            taskData.setDELVFROMTIME(object.getString(String.valueOf("DELVFROMTIME")));
            taskData.setDELVTOTIME(object.getString(String.valueOf("DELVTOTIME")));
            taskData.setDELIVERYDATE(object.getString(String.valueOf("DELIVERYDATE")));

            taskData.setCUSTOMERID(object.getString(String.valueOf("CUSTOMERID")));
            taskData.setCUSTPICKAREA(object.getString(String.valueOf("CUSTPICKAREA")));
            taskData.setCUSTDELVAREA(object.getString(String.valueOf("CUSTDELVAREA")));

            taskData.setVENDORID(object.getString(String.valueOf("VENDORID")));
            taskData.setVENDORAREA(object.getString(String.valueOf("VENDORAREA")));

            taskData.setTaskType(object.getString(String.valueOf("TaskType")));
            return taskData;
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.getLogger(MyTaskData.class.getName()).log(Level.SEVERE, null, e);

        }
        return taskData;
    }
}
