package com.zeusadssolutions.easyghaat_empapp.model;

import android.util.Log;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by HP on 06-12-2016.
 */
public class InstantOrderDetails {
    public InstantOrderDetails() {
    }

    String serviceName,serviceId;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public InstantOrderDetails InstantOrdersJsonTo(JSONObject object) {
        InstantOrderDetails managerData =new InstantOrderDetails();
        Log.d(SplashScreen.TAG, "Inside InstantOrdersJsonTo method");
        try {
            // Check for error node in json
            Log.d(SplashScreen.TAG, String.valueOf(object.getString(String.valueOf("Name"))));

            managerData.setServiceId(object.getString(String.valueOf("Id")));
            managerData.setServiceName(object.getString(String.valueOf("Name")));

            return managerData;
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.getLogger(EmpInfo.class.getName()).log(Level.SEVERE, null, e);

        }
        return null;
    }
}
