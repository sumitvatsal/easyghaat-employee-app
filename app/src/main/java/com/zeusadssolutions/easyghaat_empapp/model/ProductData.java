package com.zeusadssolutions.easyghaat_empapp.model;

import android.util.Log;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by HP on 07-Feb-17.
 */

public class ProductData {

    String PriceId,ProductId,Product_Name,Product_Desc,Product_Imgurl,CategoryId,BranchId,Price,U_Type,GenderId,ServiceId,Prc_IsActive,Prc_IsDeleted,UnitId;

    public String getPriceId() {
        return PriceId;
    }

    public void setPriceId(String priceId) {
        PriceId = priceId;
    }

    public String getUnitId() {
        return UnitId;
    }

    public void setUnitId(String unitId) {
        UnitId = unitId;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public String getProduct_Name() {
        return Product_Name;
    }

    public void setProduct_Name(String product_Name) {
        Product_Name = product_Name;
    }

    public String getProduct_Desc() {
        return Product_Desc;
    }

    public void setProduct_Desc(String product_Desc) {
        Product_Desc = product_Desc;
    }

    public String getProduct_Imgurl() {
        return Product_Imgurl;
    }

    public void setProduct_Imgurl(String product_Imgurl) {
        Product_Imgurl = product_Imgurl;
    }

    public String getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(String categoryId) {
        CategoryId = categoryId;
    }

    public String getBranchId() {
        return BranchId;
    }

    public void setBranchId(String branchId) {
        BranchId = branchId;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getU_Type() {
        return U_Type;
    }

    public void setU_Type(String u_Type) {
        U_Type = u_Type;
    }

    public String getGenderId() {
        return GenderId;
    }

    public void setGenderId(String genderId) {
        GenderId = genderId;
    }

    public String getServiceId() {
        return ServiceId;
    }

    public void setServiceId(String serviceId) {
        ServiceId = serviceId;
    }

    public String getPrc_IsActive() {
        return Prc_IsActive;
    }

    public void setPrc_IsActive(String prc_IsActive) {
        Prc_IsActive = prc_IsActive;
    }

    public String getPrc_IsDeleted() {
        return Prc_IsDeleted;
    }

    public void setPrc_IsDeleted(String prc_IsDeleted) {
        Prc_IsDeleted = prc_IsDeleted;
    }


    public ProductData ProductDataJsonTo(JSONObject object) {
        ProductData data =new ProductData();
        Log.d(SplashScreen.TAG, "Inside ProductDataJsonTo method");
        try {

            data.setPriceId(object.getString(String.valueOf("PriceId")));
            data.setProductId(object.getString(String.valueOf("ProductId")));


            data.setProduct_Name(object.getString(String.valueOf("Product_Name")));
            data.setProduct_Desc(object.getString(String.valueOf("Product_Desc")));


            data.setProduct_Imgurl(object.getString(String.valueOf("Product_Imgurl")));
            data.setCategoryId(object.getString(String.valueOf("CategoryId")));


            data.setBranchId(object.getString(String.valueOf("BranchId")));

            data.setPrice(object.getString(String.valueOf("Price")));
            data.setUnitId(object.getString(String.valueOf("UnitId")));

            data.setU_Type(object.getString(String.valueOf("U_Type")));
            data.setGenderId(object.getString(String.valueOf("GenderId")));

            data.setServiceId(object.getString(String.valueOf("ServiceId")));
            data.setPrc_IsActive(object.getString(String.valueOf("Prc_IsActive")));

            data.setPrc_IsDeleted(object.getString(String.valueOf("Prc_IsDeleted")));



            return data;
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.getLogger(EmpInfo.class.getName()).log(Level.SEVERE, null, e);

        }
        return null;
    }
}
