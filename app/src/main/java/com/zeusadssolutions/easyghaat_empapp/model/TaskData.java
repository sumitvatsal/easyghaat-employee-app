package com.zeusadssolutions.easyghaat_empapp.model;

import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by HP on 01-12-2016.
 */
public class TaskData {

    public TaskData() {
    }

    String TASKID,ORDID,VNDRID,VendorName,AREAID,ASGNMNTDT,Type,PIKUPFROMTIME,PICKUPTOTIME,PICKUPDATE,DELFROMTIME,DELTOTIME,DELIVERYDATE,CUSTOMERID,CUSTNAME,PICKUPAREA,DELAREA,DeliveryType;

    public String getTASKID() {
        return TASKID;
    }

    public void setTASKID(String TASKID) {
        this.TASKID = TASKID;
    }


    public String getAREAID() {
        return AREAID;
    }

    public void setAREAID(String AREAID) {
        this.AREAID = AREAID;
    }

    public String getORDID() {
        return ORDID;
    }

    public void setORDID(String ORDID) {
        this.ORDID = ORDID;
    }

    public String getVNDRID() {
        return VNDRID;
    }

    public void setVNDRID(String VNDRID) {
        this.VNDRID = VNDRID;
    }

    public String getVendorName() {
        return VendorName;
    }

    public void setVendorName(String vendorName) {
        VendorName = vendorName;
    }

    public String getASGNMNTDT() {
        return ASGNMNTDT;
    }

    public void setASGNMNTDT(String ASGNMNTDT) {
        this.ASGNMNTDT = ASGNMNTDT;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getPIKUPFROMTIME() {
        return PIKUPFROMTIME;
    }

    public void setPIKUPFROMTIME(String PIKUPFROMTIME) {
        this.PIKUPFROMTIME = PIKUPFROMTIME;
    }

    public String getPICKUPTOTIME() {
        return PICKUPTOTIME;
    }

    public void setPICKUPTOTIME(String PICKUPTOTIME) {
        this.PICKUPTOTIME = PICKUPTOTIME;
    }

    public String getPICKUPDATE() {
        return PICKUPDATE;
    }

    public void setPICKUPDATE(String PICKUPDATE) {
        this.PICKUPDATE = PICKUPDATE;
    }

    public String getDELFROMTIME() {
        return DELFROMTIME;
    }

    public void setDELFROMTIME(String DELFROMTIME) {
        this.DELFROMTIME = DELFROMTIME;
    }

    public String getDELTOTIME() {
        return DELTOTIME;
    }

    public void setDELTOTIME(String DELTOTIME) {
        this.DELTOTIME = DELTOTIME;
    }

    public String getDELIVERYDATE() {
        return DELIVERYDATE;
    }

    public void setDELIVERYDATE(String DELIVERYDATE) {
        this.DELIVERYDATE = DELIVERYDATE;
    }

    public String getCUSTOMERID() {
        return CUSTOMERID;
    }

    public void setCUSTOMERID(String CUSTOMERID) {
        this.CUSTOMERID = CUSTOMERID;
    }

    public String getCUSTNAME() {
        return CUSTNAME;
    }

    public void setCUSTNAME(String CUSTNAME) {
        this.CUSTNAME = CUSTNAME;
    }

    public String getPICKUPAREA() {
        return PICKUPAREA;
    }

    public void setPICKUPAREA(String PICKUPAREA) {
        this.PICKUPAREA = PICKUPAREA;
    }

    public String getDELAREA() {
        return DELAREA;
    }

    public void setDELAREA(String DELAREA) {
        this.DELAREA = DELAREA;
    }

    public String getDeliveryType() {
        return DeliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        DeliveryType = deliveryType;
    }


    public TaskData TaskDetailsJsonTo(JSONObject object) {
        TaskData taskData =new TaskData();
      //  Log.d(SplashScreen.TAG, "Inside UserDetailsJson method");
        try {

            // Check for error node in json
            taskData.setTASKID(object.getString(String.valueOf("TASKID")));
            taskData.setORDID(object.getString(String.valueOf("ORDID")));

            taskData.setVNDRID(object.getString(String.valueOf("VNDRID")));
            taskData.setVendorName(object.getString(String.valueOf("VendorName")));

            taskData.setTASKID(object.getString(String.valueOf("TASKID")));
            taskData.setORDID(object.getString(String.valueOf("ORDID")));

            taskData.setAREAID(object.getString(String.valueOf("AREAID")));
            taskData.setASGNMNTDT(object.getString(String.valueOf("ASGNMNTDT")));

            taskData.setTASKID(object.getString(String.valueOf("TASKID")));
            taskData.setType(object.getString(String.valueOf("Type")));

            taskData.setPIKUPFROMTIME(object.getString(String.valueOf("PIKUPFROMTIME")));
            taskData.setPICKUPTOTIME(object.getString(String.valueOf("PICKUPTOTIME")));

            taskData.setPICKUPDATE(object.getString(String.valueOf("PICKUPDATE")));
            taskData.setDELFROMTIME(object.getString(String.valueOf("DELFROMTIME")));

            taskData.setDELTOTIME(object.getString(String.valueOf("DELTOTIME")));
            taskData.setDELIVERYDATE(object.getString(String.valueOf("DELIVERYDATE")));

            taskData.setCUSTOMERID(object.getString(String.valueOf("CUSTOMERID")));
            taskData.setCUSTNAME(object.getString(String.valueOf("CUSTNAME")));

            taskData.setPICKUPAREA(object.getString(String.valueOf("PICKUPAREA")));
            taskData.setDELAREA(object.getString(String.valueOf("DELAREA")));
            taskData.setDeliveryType(object.getString(String.valueOf("DeliveryType")));
            return taskData;
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.getLogger(EmpInfo.class.getName()).log(Level.SEVERE, null, e);

        }
        return null;
    }
}
