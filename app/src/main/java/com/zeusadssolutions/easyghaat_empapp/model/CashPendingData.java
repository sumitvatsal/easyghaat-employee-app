package com.zeusadssolutions.easyghaat_empapp.model;

import android.util.Log;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by HP on 22-12-2016.
 */
public class CashPendingData {

    String TaskId,OrderId,CustomerId,CreateDate,TotalAmount;

    public CashPendingData() {
    }

    public String getTaskId() {
        return TaskId;
    }

    public void setTaskId(String taskId) {
        TaskId = taskId;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(String customerId) {
        CustomerId = customerId;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }

    public String getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        TotalAmount = totalAmount;
    }

    public CashPendingData cashDetailsJsonTo(JSONObject object) {
        CashPendingData cashPendingData =new CashPendingData();
        Log.d(SplashScreen.TAG, "Inside cashDetailsJsonTo method");
        try {

            // Check for error node in json

            // user successfully logged in
            cashPendingData.setTaskId(object.getString(String.valueOf("TaskId")));
            cashPendingData.setOrderId(object.getString(String.valueOf("OrderId")));
            cashPendingData.setCustomerId(object.getString(String.valueOf("CustomerId")));
            cashPendingData.setCreateDate(object.getString(String.valueOf("CreateDate")));
            cashPendingData.setTotalAmount(object.getString(String.valueOf("TotalAmount")));

            return cashPendingData;
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.getLogger(EmpInfo.class.getName()).log(Level.SEVERE, null, e);

        }
        return null;
    }
}
