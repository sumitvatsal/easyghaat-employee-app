package com.zeusadssolutions.easyghaat_empapp.model;

import android.util.Log;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by HP on 05-12-2016.
 */
public class TaskCancelReason {

    String Id,reason;

    public TaskCancelReason() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public TaskCancelReason TaskRasonJsonTo(JSONObject object) {
        TaskCancelReason taskCancelReason =new TaskCancelReason();
        Log.d(SplashScreen.TAG, "Inside TaskRasonJsonTo method");
        try {

            // Check for error node in json


            Log.d(SplashScreen.TAG, String.valueOf(object.getString(String.valueOf("Reason"))));
            // user successfully logged in
            taskCancelReason.setId(object.getString(String.valueOf("Id")));
            taskCancelReason.setReason(object.getString(String.valueOf("Reason")));

            //userDetails.setDescription(object.getString(String.valueOf("capabilities")));
            return taskCancelReason;
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.getLogger(EmpInfo.class.getName()).log(Level.SEVERE, null, e);

        }
        return null;
    }
}
