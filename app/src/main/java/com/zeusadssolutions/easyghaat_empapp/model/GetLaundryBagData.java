package com.zeusadssolutions.easyghaat_empapp.model;

import android.util.Log;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by HP on 30-12-2016.
 */
public class GetLaundryBagData {
    public GetLaundryBagData() {
    }

    String MaterialName,Count,SubAmount,OrderBagId;

    /*all getters*/

    public String getMaterialName() {
        return MaterialName;
    }

    public String getCount() {
        return Count;
    }

    public String getSubAmount() {
        return SubAmount;
    }

    public String getOrderBagId() {
        return OrderBagId;
    }

    /*all setters*/

    public void setMaterialName(String materialName) {
        MaterialName = materialName;
    }

    public void setCount(String count) {
        Count = count;
    }

    public void setSubAmount(String subAmount) {
        SubAmount = subAmount;
    }

    public void setOrderBagId(String orderBagId) {
        OrderBagId = orderBagId;
    }

    public GetLaundryBagData GetLaundryBagDataJsonTo(JSONObject object) {
        GetLaundryBagData laundryBagDetails =new GetLaundryBagData();
        Log.d(SplashScreen.TAG, "Inside GetLaundryBagDataJsonTo method");
        try {
            // Check for error node in json

            laundryBagDetails.setMaterialName(object.getString(String.valueOf("MaterialName")));
            laundryBagDetails.setCount(object.getString(String.valueOf("Count")));
            laundryBagDetails.setSubAmount(object.getString(String.valueOf("SubAmount")));
            laundryBagDetails.setOrderBagId(object.getString(String.valueOf("OrderBagId")));

            return laundryBagDetails;
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.getLogger(EmpInfo.class.getName()).log(Level.SEVERE, null, e);

        }
        return null;
    }
}
