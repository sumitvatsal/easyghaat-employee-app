package com.zeusadssolutions.easyghaat_empapp.model;

/**
 * Created by nitin_000 on 9/16/2016.
 */
public class CalData {

    String mMonth,mDate,mDay;

    public CalData(String mMonth, String mDate, String mDay) {
        this.mMonth = mMonth;
        this.mDate = mDate;
        this.mDay=mDay;
    }

    public String getmDay() {
        return mDay;
    }

    public String getmMonth() {
        return mMonth;
    }

    public String getmDate() {
        return mDate;
    }
}
