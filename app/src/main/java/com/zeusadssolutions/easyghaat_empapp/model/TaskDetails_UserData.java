package com.zeusadssolutions.easyghaat_empapp.model;

import android.util.Log;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by HP on 06-12-2016.
 */
public class TaskDetails_UserData {

    String  OrderId,Type,PaymentType,AssignmentDateTime,ExpectedCount,DeliveryType,PickUpDate,OrderType,PickupFromtime,PickupTotime,
            CustomerId,CustomerName,CustomerEmailId,CustomerMobileNo,CUSTPICKHOUSENO,CUSTPICKSTREETNAME,CUSTPICKAREAID,CUSTPICKLANDMARK,CUSTPICKCITY,
            VENDORID,VENDORNAME,VENDORPHNNO,VNDRHOUSSHOPNO,VNDRSTREETNAME,VNDRAREAID,VNDRLNDMARK,VNDRCITYNAME;

    public TaskDetails_UserData() {
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }


    public String getPickupFromtime() {
        return PickupFromtime;
    }

    public void setPickupFromtime(String pickupFromtime) {
        PickupFromtime = pickupFromtime;
    }

    public String getPickupTotime() {
        return PickupTotime;
    }

    public void setPickupTotime(String pickupTotime) {
        PickupTotime = pickupTotime;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getAssignmentDateTime() {
        return AssignmentDateTime;
    }

    public void setAssignmentDateTime(String assignmentDateTime) {
        AssignmentDateTime = assignmentDateTime;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(String paymentType) {
        PaymentType = paymentType;
    }

    public String getExpectedCount() {
        return ExpectedCount;
    }

    public void setExpectedCount(String expectedCount) {
        ExpectedCount = expectedCount;
    }

    public String getDeliveryType() {
        return DeliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        DeliveryType = deliveryType;
    }

    public String getPickUpDate() {
        return PickUpDate;
    }

    public void setPickUpDate(String pickUpDate) {
        PickUpDate = pickUpDate;
    }

    public String getOrderType() {
        return OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public String getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(String customerId) {
        CustomerId = customerId;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerEmailId() {
        return CustomerEmailId;
    }

    public void setCustomerEmailId(String customerEmailId) {
        CustomerEmailId = customerEmailId;
    }

    public String getCustomerMobileNo() {
        return CustomerMobileNo;
    }

    public void setCustomerMobileNo(String customerMobileNo) {
        CustomerMobileNo = customerMobileNo;
    }

    public String getCUSTPICKHOUSENO() {
        return CUSTPICKHOUSENO;
    }

    public void setCUSTPICKHOUSENO(String CUSTPICKHOUSENO) {
        this.CUSTPICKHOUSENO = CUSTPICKHOUSENO;
    }

    public String getCUSTPICKSTREETNAME() {
        return CUSTPICKSTREETNAME;
    }

    public void setCUSTPICKSTREETNAME(String CUSTPICKSTREETNAME) {
        this.CUSTPICKSTREETNAME = CUSTPICKSTREETNAME;
    }

    public String getCUSTPICKAREAID() {
        return CUSTPICKAREAID;
    }

    public void setCUSTPICKAREAID(String CUSTPICKAREAID) {
        this.CUSTPICKAREAID = CUSTPICKAREAID;
    }

    public String getCUSTPICKLANDMARK() {
        return CUSTPICKLANDMARK;
    }

    public void setCUSTPICKLANDMARK(String CUSTPICKLANDMARK) {
        this.CUSTPICKLANDMARK = CUSTPICKLANDMARK;
    }

    public String getCUSTPICKCITY() {
        return CUSTPICKCITY;
    }

    public void setCUSTPICKCITY(String CUSTPICKCITY) {
        this.CUSTPICKCITY = CUSTPICKCITY;
    }

    public String getVENDORID() {
        return VENDORID;
    }

    public void setVENDORID(String VENDORID) {
        this.VENDORID = VENDORID;
    }

    public String getVENDORNAME() {
        return VENDORNAME;
    }

    public void setVENDORNAME(String VENDORNAME) {
        this.VENDORNAME = VENDORNAME;
    }

    public String getVENDORPHNNO() {
        return VENDORPHNNO;
    }

    public void setVENDORPHNNO(String VENDORPHNNO) {
        this.VENDORPHNNO = VENDORPHNNO;
    }

    public String getVNDRHOUSSHOPNO() {
        return VNDRHOUSSHOPNO;
    }

    public void setVNDRHOUSSHOPNO(String VNDRHOUSSHOPNO) {
        this.VNDRHOUSSHOPNO = VNDRHOUSSHOPNO;
    }

    public String getVNDRSTREETNAME() {
        return VNDRSTREETNAME;
    }

    public void setVNDRSTREETNAME(String VNDRSTREETNAME) {
        this.VNDRSTREETNAME = VNDRSTREETNAME;
    }

    public String getVNDRAREAID() {
        return VNDRAREAID;
    }

    public void setVNDRAREAID(String VNDRAREAID) {
        this.VNDRAREAID = VNDRAREAID;
    }

    public String getVNDRLNDMARK() {
        return VNDRLNDMARK;
    }

    public void setVNDRLNDMARK(String VNDRLNDMARK) {
        this.VNDRLNDMARK = VNDRLNDMARK;
    }

    public String getVNDRCITYNAME() {
        return VNDRCITYNAME;
    }

    public void setVNDRCITYNAME(String VNDRCITYNAME) {
        this.VNDRCITYNAME = VNDRCITYNAME;
    }

    public TaskDetails_UserData TaskDetailsJsonTo(JSONObject object) {
        TaskDetails_UserData taskData =new TaskDetails_UserData();
        Log.d(SplashScreen.TAG, "Inside TaskDetailsJsonTo method");

        try {
            Log.d(SplashScreen.TAG, "Task id-----" + object.getString(String.valueOf("OrderId")));
            // Check for error node in json
            taskData.setOrderId(object.getString(String.valueOf("OrderId")));

            taskData.setType(object.getString(String.valueOf("Type")));
            taskData.setOrderType(object.getString(String.valueOf("OrderType")));

            taskData.setAssignmentDateTime(object.getString(String.valueOf("AssignmentDateTime")));
            taskData.setExpectedCount(object.getString(String.valueOf("ExpectedCount")));
            taskData.setDeliveryType(object.getString(String.valueOf("DeliveryType")));
            taskData.setPaymentType(object.getString(String.valueOf("PaymentType")));
            taskData.setPickUpDate(object.getString(String.valueOf("PickUpDate")));
            taskData.setPickupFromtime(object.getString(String.valueOf("PickupFromtime")));
            taskData.setPickupTotime(object.getString(String.valueOf("PickupTotime")));


            taskData.setCustomerId(object.getString(String.valueOf("CustomerId")));
            taskData.setCustomerName(object.getString(String.valueOf("CustomerName")));
            taskData.setCustomerEmailId(object.getString(String.valueOf("CustomerEmailId")));

            taskData.setCustomerMobileNo(object.getString(String.valueOf("CustomerMobileNo")));
            taskData.setCUSTPICKHOUSENO(object.getString(String.valueOf("CUSTPICKHOUSENO")));
            taskData.setCUSTPICKSTREETNAME(object.getString(String.valueOf("CUSTPICKSTREETNAME")));


            taskData.setCUSTPICKAREAID(object.getString(String.valueOf("CUSTPICKAREAID")));
            taskData.setCUSTPICKLANDMARK(object.getString(String.valueOf("CUSTPICKLANDMARK")));
            taskData.setCUSTPICKCITY(object.getString(String.valueOf("CUSTPICKCITY")));

            taskData.setVENDORID(object.getString(String.valueOf("VENDORID")));
            taskData.setVENDORNAME(object.getString(String.valueOf("VENDORNAME")));

            taskData.setVENDORPHNNO(object.getString(String.valueOf("VENDORPHNNO")));
            taskData.setVNDRHOUSSHOPNO(object.getString(String.valueOf("VNDRHOUSSHOPNO")));

            taskData.setVNDRSTREETNAME(object.getString(String.valueOf("VNDRSTREETNAME")));
            taskData.setVNDRAREAID(object.getString(String.valueOf("VNDRAREAID")));

            taskData.setVNDRLNDMARK(object.getString(String.valueOf("VNDRLNDMARK")));
            taskData.setVNDRCITYNAME(object.getString(String.valueOf("VNDRCITYNAME")));

            return taskData;
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.getLogger(EmpInfo.class.getName()).log(Level.SEVERE, null, e);

        }
        return taskData;
    }
}
