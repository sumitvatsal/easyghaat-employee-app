package com.zeusadssolutions.easyghaat_empapp.model;

import android.util.Log;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by HP on 29-12-2016.
 */
public class EarningData {

    public EarningData() {
    }

    String OnDate,TotalIncentiveAmount,TotalDistanceAmount;

    /*all getters*/

    public String getOnDate() {
        return OnDate;
    }

    public String getTotalIncentiveAmount() {
        return TotalIncentiveAmount;
    }

    public String getTotalDistanceAmount() {
        return TotalDistanceAmount;
    }

    /*all setters*/

    public void setOnDate(String onDate) {
        OnDate = onDate;
    }

    public void setTotalIncentiveAmount(String totalIncentiveAmount) {
        TotalIncentiveAmount = totalIncentiveAmount;
    }

    public void setTotalDistanceAmount(String totalDistanceAmount) {
        TotalDistanceAmount = totalDistanceAmount;
    }



    public EarningData EarningDataJsonTo(JSONObject object) {
        EarningData earningData =new EarningData();
        Log.d(SplashScreen.TAG, "Inside UserDetailsJson method");
        try {

            // Check for error node in json



            // user successfully logged in
            earningData.setOnDate(object.getString(String.valueOf("OnDate")));
            earningData.setTotalIncentiveAmount(object.getString(String.valueOf("TotalIncentiveAmount")));
            earningData.setTotalDistanceAmount(object.getString(String.valueOf("TotalDistanceAmount")));

            return earningData;
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.getLogger(EmpInfo.class.getName()).log(Level.SEVERE, null, e);

        }
        return null;
    }
}
