package com.zeusadssolutions.easyghaat_empapp.model;

import android.util.Log;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by HP on 06-12-2016.
 */
public class MakeOrderDetails {
    public MakeOrderDetails() {
    }

    String OrderDetailId,ProductName,CategoryName,Service,Length,Breadth,Qunatity,Price,SubAmount;

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getOrderDetailId() {
        return OrderDetailId;
    }

    public void setOrderDetailId(String orderDetailId) {
        OrderDetailId = orderDetailId;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getService() {
        return Service;
    }

    public void setService(String service) {
        Service = service;
    }

    public String getLength() {
        return Length;
    }

    public void setLength(String length) {
        Length = length;
    }

    public String getBreadth() {
        return Breadth;
    }

    public void setBreadth(String breadth) {
        Breadth = breadth;
    }

    public String getQunatity() {
        return Qunatity;
    }

    public void setQunatity(String qunatity) {
        Qunatity = qunatity;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getSubAmount() {
        return SubAmount;
    }

    public void setSubAmount(String subAmount) {
        SubAmount = subAmount;
    }

    public MakeOrderDetails MakeOrdersJsonTo(JSONObject object) {
        MakeOrderDetails managerData =new MakeOrderDetails();
        Log.d(SplashScreen.TAG, "Inside MakeOrdersJsonTo method");
        try {
            // Check for error node in json
           Log.d(SplashScreen.TAG, String.valueOf(object.getString(String.valueOf("ProductName"))));

            managerData.setBreadth(object.getString(String.valueOf("Breadth")));
            managerData.setCategoryName(object.getString(String.valueOf("CategoryName")));
            managerData.setOrderDetailId(object.getString(String.valueOf("OrderDetailId")));
            managerData.setProductName(object.getString(String.valueOf("ProductName")));
            managerData.setService(object.getString(String.valueOf("Service")));
            managerData.setLength(object.getString(String.valueOf("Length")));
            managerData.setQunatity(object.getString(String.valueOf("Qunatity")));
            managerData.setPrice(object.getString(String.valueOf("Price")));
            managerData.setSubAmount(object.getString(String.valueOf("SubAmount")));

            return managerData;
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.getLogger(EmpInfo.class.getName()).log(Level.SEVERE, null, e);

        }
        return null;
    }
}
