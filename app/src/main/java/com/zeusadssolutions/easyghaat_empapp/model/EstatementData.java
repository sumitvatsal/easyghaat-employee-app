package com.zeusadssolutions.easyghaat_empapp.model;

import android.util.Log;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by HP on 29-12-2016.
 */
public class EstatementData {

    public EstatementData() {
    }

    String Reason,TransferedAmount,BankReferenceNo,Remark,TransferDate,BankName;

    /*all setters*/

    public void setReason(String reason) {
        Reason = reason;
    }

    public void setTransferedAmount(String transferedAmount) {
        TransferedAmount = transferedAmount;
    }

    public void setBankReferenceNo(String bankReferenceNo) {
        BankReferenceNo = bankReferenceNo;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public void setTransferDate(String transferDate) {
        TransferDate = transferDate;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    /*all getters*/

    public String getReason() {
        return Reason;
    }

    public String getTransferedAmount() {
        return TransferedAmount;
    }

    public String getBankReferenceNo() {
        return BankReferenceNo;
    }

    public String getRemark() {
        return Remark;
    }

    public String getTransferDate() {
        return TransferDate;
    }

    public String getBankName() {
        return BankName;
    }


    public EstatementData EstatementDataJsonTo(JSONObject object) {
        EstatementData estatementData =new EstatementData();
        Log.d(SplashScreen.TAG, "Inside EstatementDataJsonTo method");
        try {

            // user successfully logged in
            estatementData.setReason(object.getString(String.valueOf("Reason")));
            estatementData.setTransferedAmount(object.getString(String.valueOf("TransferedAmount")));
            estatementData.setBankReferenceNo(object.getString(String.valueOf("BankReferenceNo")));

            estatementData.setRemark(object.getString(String.valueOf("Remark")));
            estatementData.setTransferDate(object.getString(String.valueOf("TransferDate")));
            estatementData.setBankName(object.getString(String.valueOf("BankName")));

            return estatementData;
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.getLogger(EmpInfo.class.getName()).log(Level.SEVERE, null, e);

        }
        return null;
    }
}
