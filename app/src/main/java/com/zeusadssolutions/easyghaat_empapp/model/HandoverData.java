package com.zeusadssolutions.easyghaat_empapp.model;

import android.util.Log;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by HP on 21-12-2016.
 */
public class HandoverData {
    String PickUpId,TaskID,OrderId,VendorId,CustomerId,FromArea,ToArea,
            DeliveryType,VendorName,VendorPhone,VendorShopNo,VendorStreet,VendroLandmark,VendorArea,
            VendorCity,VendorPin;


    public String getPickUpId() {
        return PickUpId;
    }

    public void setPickUpId(String pickUpId) {
        PickUpId = pickUpId;
    }

    public String getTaskID() {
        return TaskID;
    }

    public void setTaskID(String taskID) {
        TaskID = taskID;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getVendorId() {
        return VendorId;
    }

    public void setVendorId(String vendorId) {
        VendorId = vendorId;
    }

    public String getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(String customerId) {
        CustomerId = customerId;
    }

    public String getFromArea() {
        return FromArea;
    }

    public void setFromArea(String fromArea) {
        FromArea = fromArea;
    }

    public String getToArea() {
        return ToArea;
    }

    public void setToArea(String toArea) {
        ToArea = toArea;
    }

    public String getDeliveryType() {
        return DeliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        DeliveryType = deliveryType;
    }

    public String getVendorName() {
        return VendorName;
    }

    public void setVendorName(String vendorName) {
        VendorName = vendorName;
    }

    public String getVendorPhone() {
        return VendorPhone;
    }

    public void setVendorPhone(String vendorPhone) {
        VendorPhone = vendorPhone;
    }

    public String getVendorShopNo() {
        return VendorShopNo;
    }

    public void setVendorShopNo(String vendorShopNo) {
        VendorShopNo = vendorShopNo;
    }

    public String getVendorStreet() {
        return VendorStreet;
    }

    public void setVendorStreet(String vendorStreet) {
        VendorStreet = vendorStreet;
    }

    public String getVendroLandmark() {
        return VendroLandmark;
    }

    public void setVendroLandmark(String vendroLandmark) {
        VendroLandmark = vendroLandmark;
    }

    public String getVendorArea() {
        return VendorArea;
    }

    public void setVendorArea(String vendorArea) {
        VendorArea = vendorArea;
    }

    public String getVendorCity() {
        return VendorCity;
    }

    public void setVendorCity(String vendorCity) {
        VendorCity = vendorCity;
    }

    public String getVendorPin() {
        return VendorPin;
    }

    public void setVendorPin(String vendorPin) {
        VendorPin = vendorPin;
    }

    public HandoverData TaskDetailsJsonTo(JSONObject object) {
        HandoverData taskData =new HandoverData();
        Log.d(SplashScreen.TAG, "Inside TaskDetailsJsonTo method");

        try {
            Log.d(SplashScreen.TAG, "Task id-----" + object.getString(String.valueOf("TaskID")));
            // Check for error node in json
            taskData.setTaskID(object.getString(String.valueOf("TaskID")));
            taskData.setPickUpId(object.getString(String.valueOf("PickUpId")));
            taskData.setOrderId(object.getString(String.valueOf("OrderId")));

            taskData.setCustomerId(object.getString(String.valueOf("CustomerId")));
            taskData.setFromArea(object.getString(String.valueOf("FromArea")));
            taskData.setToArea(object.getString(String.valueOf("ToArea")));

            taskData.setDeliveryType(object.getString(String.valueOf("DeliveryType")));

            taskData.setVendorId(object.getString(String.valueOf("VendorId")));
            taskData.setVendorName(object.getString(String.valueOf("VendorName")));
            taskData.setVendorPhone(object.getString(String.valueOf("VendorPhone")));

            taskData.setVendorShopNo(object.getString(String.valueOf("VendorShopNo")));
            taskData.setVendorStreet(object.getString(String.valueOf("VendorStreet")));
            taskData.setVendroLandmark(object.getString(String.valueOf("VendroLandmark")));
            taskData.setVendorShopNo(object.getString(String.valueOf("VendorShopNo")));
            taskData.setVendorArea(object.getString(String.valueOf("VendorArea")));

            taskData.setVendorCity(object.getString(String.valueOf("VendorCity")));
            taskData.setVendorPin(object.getString(String.valueOf("VendorPin")));

            return taskData;
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.getLogger(EmpInfo.class.getName()).log(Level.SEVERE, null, e);

        }
        return taskData;
    }
}
