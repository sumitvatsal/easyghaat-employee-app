package com.zeusadssolutions.easyghaat_empapp.model;

import android.util.Log;

import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by HP on 30-11-2016.
 */
public class ManagerData {

    String emp_name,per_phoneNo;

    public ManagerData() {
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }

    public String getPer_phoneNo() {
        return per_phoneNo;
    }

    public void setPer_phoneNo(String per_phoneNo) {
        this.per_phoneNo = per_phoneNo;
    }


    public ManagerData UserDetailsJsonTo(JSONObject object) {
        ManagerData managerData =new ManagerData();
        Log.d(SplashScreen.TAG, "Inside UserDetailsJson method");
        try {

            // Check for error node in json


            Log.d(SplashScreen.TAG, String.valueOf(object.getString(String.valueOf("emp_name"))));
            // user successfully logged in
            managerData.setPer_phoneNo(object.getString(String.valueOf("per_phoneNo")));
            managerData.setEmp_name(object.getString(String.valueOf("emp_name")));

            //userDetails.setDescription(object.getString(String.valueOf("capabilities")));
            return managerData;
        } catch (JSONException e) {
            e.printStackTrace();
            Logger.getLogger(EmpInfo.class.getName()).log(Level.SEVERE, null, e);

        }
        return null;
    }
}
