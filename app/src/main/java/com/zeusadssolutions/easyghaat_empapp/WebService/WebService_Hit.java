package com.zeusadssolutions.easyghaat_empapp.WebService;

import android.util.Log;


import com.zeusadssolutions.easyghaat_empapp.activity.SplashScreen;

import org.apache.http.NameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by HP on 12-10-2016.
 */
public class WebService_Hit {
 public static  int responseCode;
    public WebService_Hit() {
    }

    /*---------------------Get Method-------------*/
    public String getUrlContents(String theUrl) {
        StringBuilder content = new StringBuilder();
        Log.d(SplashScreen.TAG, theUrl);
        try {
            URL url = new URL(theUrl);
            HttpURLConnection myURLConnection = (HttpURLConnection) url.openConnection();
            myURLConnection.setReadTimeout(60000 /* milliseconds */);
            myURLConnection.setConnectTimeout(60000 /* milliseconds */);
            responseCode=myURLConnection.getResponseCode();
            if(responseCode==myURLConnection.HTTP_OK || responseCode==HttpURLConnection.HTTP_CREATED)
            {
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(myURLConnection.getInputStream()), 8);
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                Log.d(SplashScreen.TAG, content.toString());
                bufferedReader.close();

            }else {
                content.append(responseCode);
            }
        }catch (Exception e) {
            content.append(e);
            e.printStackTrace();
        }
        return content.toString();
    }

    /*---------------------Post with Param Method-------------*/
    public String postWithParamUrlContents( JSONObject postDataParams,final String url)
    {
        StringBuilder content = new StringBuilder();
        Log.d(SplashScreen.TAG,"Url received---"+ url);
        try {
           Log.d(SplashScreen.TAG,"postDataParams--received---"+ postDataParams.toString());

            URL myURL = new URL(url);



            HttpURLConnection myURLConnection = (HttpURLConnection) myURL.openConnection();
            myURLConnection.setReadTimeout(30000 /* milliseconds */);
            myURLConnection.setConnectTimeout(30000 /* milliseconds */);
            myURLConnection.setRequestMethod("POST");
            myURLConnection.setDoInput(true);
            myURLConnection.setDoOutput(true);
            myURLConnection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
            Log.d(SplashScreen.TAG, "send request ...............");

            OutputStream os = myURLConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();
            responseCode=myURLConnection.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK || responseCode==HttpURLConnection.HTTP_CREATED) {

                Log.d(SplashScreen.TAG, "response code---" + String.valueOf(responseCode));

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(
                                myURLConnection.getInputStream()));

                String line = "";

                while ((line = in.readLine()) != null) {

                    content.append(line);
                    break;
                }

                in.close();

            } else {

                content.append(responseCode);
            }

        }catch (Exception e) {
            Log.d(SplashScreen.TAG,"Inside catch..............."+e.toString());
            content.append(e);
            e.printStackTrace();
        }
        return content.toString();
    }

    /*---------------------PUT with Param Method--------------*/
    public String putWithParamUrlContents(JSONObject postDataParams,final String url)
    {
        StringBuilder content = new StringBuilder();
        Log.d(SplashScreen.TAG, url);
        try {
            Log.d(SplashScreen.TAG,"postDataParams--received---"+ postDataParams.toString());
            URL myURL = new URL(url);
            HttpURLConnection myURLConnection = (HttpURLConnection) myURL.openConnection();

            myURLConnection.setReadTimeout(30000 /* milliseconds */);
            myURLConnection.setConnectTimeout(30000 /* milliseconds */);
            myURLConnection.setRequestMethod("PUT");
            myURLConnection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
            Log.d(SplashScreen.TAG, "send request ...............");

            OutputStream os = myURLConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();
           // responseCode=myURLConnection.getResponseCode();

           // if (responseCode == HttpsURLConnection.HTTP_OK || responseCode==HttpURLConnection.HTTP_CREATED) {

                BufferedReader in=new BufferedReader(
                        new InputStreamReader(
                                myURLConnection.getInputStream()));
                String line="";

                while((line = in.readLine()) != null) {

                    content.append(line);
                    break;
                }

                in.close();

           // }
           // else {

             //   content.append(responseCode);
           // }



        }catch (Exception e) {
            content.append(e);
            e.printStackTrace();
        }
        return content.toString();
    }


    /*---------------------PUT Method-------------*/
    public String putUrlContents(final String url)
    {
        StringBuilder content = new StringBuilder();
        Log.d(SplashScreen.TAG, url);
        try {

            URL myURL = new URL(url);
            HttpURLConnection myURLConnection = (HttpURLConnection) myURL.openConnection();

            myURLConnection.setReadTimeout(30000 /* milliseconds */);
            myURLConnection.setConnectTimeout(30000 /* milliseconds */);
            myURLConnection.setRequestMethod("PUT");
            myURLConnection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
            Log.d(SplashScreen.TAG, "send request ...............");

            responseCode=myURLConnection.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK || responseCode==HttpURLConnection.HTTP_CREATED) {

                BufferedReader in=new BufferedReader(
                        new InputStreamReader(
                                myURLConnection.getInputStream()));
                String line="";

                while((line = in.readLine()) != null) {

                    content.append(line);
                    break;
                }

                in.close();

            }
            else {

                content.append(responseCode);
            }



        }catch (Exception e) {
            content.append(e);
            e.printStackTrace();
        }
        return content.toString();
    }

    /*---------------------DELETE Method-------------*/
    public String deleteUrlContents(final String url)
    {
        StringBuilder content = new StringBuilder();
        Log.d(SplashScreen.TAG, url);
        try {

            URL myURL = new URL(url);
            HttpURLConnection myURLConnection = (HttpURLConnection) myURL.openConnection();

            myURLConnection.setReadTimeout(30000 /* milliseconds */);
            myURLConnection.setConnectTimeout(30000 /* milliseconds */);
            myURLConnection.setRequestMethod("DELETE");
            myURLConnection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
            Log.d(SplashScreen.TAG, "send request ...............");

            responseCode=myURLConnection.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK || responseCode==HttpURLConnection.HTTP_CREATED) {
                BufferedReader in=new BufferedReader(
                        new InputStreamReader(
                                myURLConnection.getInputStream()));

                String line="";

                while((line = in.readLine()) != null) {

                    content.append(line);
                    break;
                }

                in.close();


            }
            else {

                content.append(responseCode);
            }



        }catch (Exception e) {
            content.append(e);
            e.printStackTrace();
        }
        return content.toString();
    }

    /*---------------------Post Method-------------*/
    public String postUrlContents( final String url)
    {
        StringBuilder content = new StringBuilder();
        Log.d(SplashScreen.TAG, url);
        try {
            //  Log.d(SplashScreen.TAG,"orderId-----"+ postDataParams.toString());
            URL myURL = new URL(url);
            HttpURLConnection myURLConnection = (HttpURLConnection) myURL.openConnection();
            myURLConnection.setReadTimeout(30000 /* milliseconds */);
            myURLConnection.setConnectTimeout(30000 /* milliseconds */);
            myURLConnection.setRequestMethod("POST");
            myURLConnection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
            Log.d(SplashScreen.TAG, "send request ...............");

            responseCode=myURLConnection.getResponseCode();

            Log.d(SplashScreen.TAG, "response code---" + String.valueOf(responseCode));
            if (responseCode == HttpsURLConnection.HTTP_OK || responseCode==HttpURLConnection.HTTP_CREATED) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(
                                myURLConnection.getInputStream()));

                String line = "";

                while ((line = in.readLine()) != null) {

                    content.append(line);
                    break;
                }

                in.close();
            } else {

                content.append(responseCode);
            }



        }catch (Exception e) {
            content.append(e);
            e.printStackTrace();
        }
        return content.toString();
    }

    /*---------------------Post with Param Method-------------*/
    public String postListParamUrlContents( List<NameValuePair> postDataParams,final String url)
    {
        StringBuilder content = new StringBuilder();
        Log.d(SplashScreen.TAG,"Url received---"+ url);
        try {
            Log.d(SplashScreen.TAG,"postDataParams--received---"+ postDataParams.toString());

            URL myURL = new URL(url);



            HttpURLConnection myURLConnection = (HttpURLConnection) myURL.openConnection();
            myURLConnection.setReadTimeout(30000 /* milliseconds */);
            myURLConnection.setConnectTimeout(30000 /* milliseconds */);
            myURLConnection.setRequestMethod("POST");
            myURLConnection.setDoInput(true);
            myURLConnection.setDoOutput(true);
            myURLConnection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
            Log.d(SplashScreen.TAG, "send request ...............");

            OutputStream os = myURLConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getQuery(postDataParams));

            writer.flush();
            writer.close();
            os.close();
            responseCode=myURLConnection.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK || responseCode==HttpURLConnection.HTTP_CREATED) {

                Log.d(SplashScreen.TAG, "response code---" + String.valueOf(responseCode));

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(
                                myURLConnection.getInputStream()));

                String line = "";

                while ((line = in.readLine()) != null) {

                    content.append(line);
                    break;
                }

                in.close();

            } else {

                content.append(responseCode);
            }

        }catch (Exception e) {
            Log.d(SplashScreen.TAG,"Inside catch..............."+e.toString());
            content.append(e);
            e.printStackTrace();
        }
        return content.toString();
    }

    public String getPostDataString(JSONObject params) throws Exception {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }
}
